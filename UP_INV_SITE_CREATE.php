<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVSITECREATE';


require_once 'app/init.php';
// Include app init file

$return_message = null;

// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

if(isset($_GET['message']))
{
  $return_message = $_GET['message'];
}


include 'header.php'; //includes the navigation header

?>



<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Site</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_SITE_CREATE_PROC.php' method='post'>

                    <div class="form-group">
                      <label for="site" class="control-label col-md-2">Site</label>
                        <div class="col-md-8">
                          <input type='text' name='site' id='site' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='sitevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='site_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'Success')
    {
      echo '<p class="bg-success" id="return">Site Successfully Created</p>';
    } elseif ($return_message == 'Error'){
      echo '<p class="bg-danger" id="return">Site creation ended in error.</p>';
    }



  ?>
</div>

<script>

var siteIsValid = null;

$(document).ready(function (){
    validate_filled();
    $('#site').change(validate_filled);
    $('#return_message').delay(5000).fadeOut();
});

$("#site").blur(function(){
  if (  $('#site').val().length   >   0  ){
    $('#site').val($(this).val().toUpperCase());
    var site = $('#site').val();
    var hasSpace = $('#site').val().indexOf(' ')>=0;
    if (!hasSpace){
    $.post('ajax/UP_INV_SITE_VALIDATE.php', {site: site}, function(data){
      if (data == 0){
              siteIsValid = true;
              $('#sitevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#site_submit').focus();
      } else if (data > 0){
        siteIsValid = false;
              $('#sitevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Site exists already.  Please enter a unique site name.");
              $('#site').val('');
              $('#site').focus();
      } else {
        siteIsValid = false;
         $('#sitevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new site name.");
              $('#site').val('');
      }
    });
    } else {
      $('#sitevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Site names cannot contain spaces.  Please enter a different site name.");
      $('#site').val('');
      $('#site').focus();
    }
  }
});

function validate_filled(){
    if (siteIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#group_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


</script>

              </body>
</html>
