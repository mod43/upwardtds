<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENULINESEDIT';

require_once 'app/init.php';
// Include app init file
    
// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function."); 
        
    }

include 'header.php'; //includes the navigation header

$menu_line_id = $_GET['menu_line_id'];

if (!$menu_shortcode)
    {
      echo '<div class="col-md-12">';
      echo 'Menu Line Not Found!';
      echo '<br>';
      echo '<a href="menu.php"><button type="button" class="btn btn-primary">Back to Menu</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
    } 

$menu_info = $database->table('srm_menu_lines')->where('menu_line_id','=',$menu_line_id)->first();

?>

<div class="col-sm-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Menu Line Create/Edit</h1></center>
    </div>
    <div class="panel-body">
      <form class="form-horizontal" action='ajax/UP_SRM_LINE_EDIT_PROCESS.php' method='post'>
        <div class="form-group">
          <label for="order" class="control-label col-sm-2">Order</label>
            <div class="col-sm-8">
              <span name='order' id='order' class="form-control"><?php echo $wo_line_info->work_order_number; ?></span>
            </div>
         </div>  
        <div class="form-group">
          <label for="item" class="control-label col-sm-2">Item</label>
            <div class="col-sm-8">
              <input type='text' name='item' id='item' value='<?php echo $wo_line_info->item_name; ?>' class='form-control' tabindex='1'>
            </div>
            <div class="col-sm-1">
              <span id='itemvalid'></span>
            </div>
         </div>   
         <div class="form-group">
          <label for="quantity" class="control-label col-sm-2">Quantity</label>
            <div class="col-sm-8">
              <input type='text' name='quantity' id='quantity' value='<?php echo $wo_line_info->wo_line_quantity; ?>' class='form-control' tabindex='4'>
           </div>
           <div class="col-sm-1">
              <span id='quantityvalid'></span>
            </div>
         </div> 
         <div class="form-group">
          <label for="available_quantity" class="control-label col-sm-2">Available Quantity</label>
            <div class="col-sm-8">
              <span name='available_quantity' id='available_quantity' class="form-control"></span>
            </div>
         </div> 
         <div class="form-group">
                      <label for="line_status" class="control-label col-md-2">Line Status</label>
                        <div class="col-md-8">
                          <select class="form-control" name="line_status" id="line_status">
                            <option value="1" <?php if ($wo_line_info->wo_line_status == 1){echo 'selected';} ?>>Open</option>
                            <option value="2" <?php if ($wo_line_info->wo_line_status == 2){echo 'selected';} ?>>Pick Released</option>
                            <option value="3" <?php if ($wo_line_info->wo_line_status == 3){echo 'selected';} ?>>Picked</option>
                            <option value="4" <?php if ($wo_line_info->wo_line_status == 4){echo 'selected';} ?>>Backordered</option>
                            <option value="5" <?php if ($wo_line_info->wo_line_status == 5){echo 'selected';} ?>>Cancelled</option>
                          </select>
                        </div>
                     </div>
          <div class="form-group">
          <div class="row">
                  <div class="col-xs-1">
                    <input type="hidden" name="wo_line_id" value=<?php echo '"'.$wo_line_id.'"'; ?> >
                  </div>
          </div>
          <div class="row">
                  <div class="col-xs-1">
                    <input type="hidden" name="wo_header_number" value=<?php echo $wo_line_info->work_order_number; ?> >
                  </div>
          </div>
          <div class="col-sm-offset-3 col-sm-8">
              <span id='message'></span>
          </div>
          </div>   
         <div class="form-group">
            <div class="col-sm-offset-4 col-sm-4">
              <input type="submit" value="Submit" id='line_edit_submit' class="btn btn-primary btn-block" tabindex='5'>
            </div>
          </div>
        </form>
      </div>
  </div>      
</div>
<script type="text/javascript">

var itemIsValid = null;
var quantityIsValid = null;
var available_quantity = null;

$(document).ready(function (){
    validate_filled();
    var item = $('#item').val();
    $.post('ajax/UP_INV_QTYBYSITE_QUERY.php', {item: item}, function(data){
                  $('#available_quantity').html(data);
                  available_quantity = data;
                  });
    $('#item').focus();
    $('#item, #quantity').blur(validate_filled);

});

function validate_filled(){
    if (itemIsValid &&
        quantityIsValid) {
        $('#item_submit').attr("disabled", false);
        $('#item_submit').focus();
    }
    else {
        $('#item_submit').attr("disabled", true);
    }
};

// item autocomplete
$(function() {
  $("#item").autocomplete({
    source: "ajax/UP_INV_ITEM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });       
});

// item validation
$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data > 0){
        itemIsValid = true;
              $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              if ($('#item').val().length  >   0){
                  var item = $('#item').val();
                  $.post('ajax/UP_INV_QTYBYSITE_QUERY.php', {item: item}, function(data){
                  $('#available_quantity').html(data);
                  available_quantity = data;
                  });
              } 
              $('quantity').focus();
      } else {
        itemIsValid = false;
        $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#item').val('');
        $('#item').focus();
      }
    });
  }
});

$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_ent = parseInt($('#quantity').val()); //create integer
        var qty_avl = parseInt(available_quantity);  //create integer
          if (qty_ent > qty_avl){
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-warning'><span class='glyphicon glyphicon-warning-sign' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered is greater than available quantity. This may result in a backorder.");
            validate_filled();
          } else if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
          } else if (qty_ent <= qty_avl) {
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});





</script>


</body>
</html>