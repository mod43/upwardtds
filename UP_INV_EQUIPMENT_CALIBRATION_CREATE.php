<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQPCALCREATE';

$return_message = null;

require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Calibration and Serial Setup</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_EQUIPMENT_CALIBRATE_CREATE_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="equipment" class="control-label col-md-2">Equipment</label>
                        <div class="col-md-8">
                          <input type='text' name='equipment' id='equipment' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='equipmentvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="serial" class="control-label col-md-2">Serial</label>
                        <div class="col-md-8">
                          <input type='text' name='serial' id='serial' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='serialvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="equipment_desc" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                          <input type='text' name='equipment_desc' id='equipment_desc' value='' class='form-control' autocomplete="off" readonly>
                        </div>
                        <div class="col-md-2">
                          <span id='equipment_descvalid'></span>
                        </div>
                     </div>
                    
                    <div class="form-group">
                      <label for="frequency" class="control-label col-md-2">Frequency</label>
                        <div class="col-md-8">
                          <input name='frequency' id='frequency' value='' class='form-control' autocomplete="off" type='number'>
                        </div>
                        <div class="col-md-2">
                          <span id='frequencyvalid'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="initial_date" class="control-label col-md-2">Last Date</label>
                        <div class="col-md-8">
                          <input name='initial_date' id='initial_date' value='' class='form-control' tabindex='3' readonly='readonly' style='background:white;'>
                        </div>
                        <div class="col-md-2">
                          <span id='initialdatevalid'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="last_user" class="control-label col-md-2">Last User</label>
                        <div class="col-md-8">
                          <input type='text' name='last_user' id='last_user' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='uservalid'></span>
                        </div>
                     </div>

                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="col-xs-1">
                        <input type="hidden" id="equipment_id" name="equipment_id" value=''>
                      </div>
                     <div class="col-xs-1">
                        <input type="hidden" id="serial_id" name="serial_id" value=''>
                      </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='calibration_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Calibration setup created successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Calibration setup ended in error.</p>';
    }



  ?>
</div>

</div>


<script>
var equipmentIsValid = null;
var serialIsValid = null;
var frequencyIsValid = null;
var dateIsValid = null;
var lastUserIsValid = null;

$(document).ready(function(){

    validate_filled();
});



$('#initial_date').datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat: "yy-mm-dd"
});

$(function() {
  $("#equipment").autocomplete({
    source: "ajax/UP_INV_EQUIPMENT_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#equipment").blur(function(){
  if (  $('#equipment').val().length   >   0  ){
    $('#equipment').val($(this).val().toUpperCase()); //take the item to uppercase
    var equipment = $('#equipment').val();
    $.post('ajax/UP_INV_EQUIPMENT_VALIDATE.php', {equipment: equipment}, function(data){
      if (data > 0){
        equipmentIsValid = true;
        $('#equipmentvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
        $("#serial").focus();
        $.post('ajax/UP_INV_EQUIPMENT_GET_DESCRIPTION.php', {equipment: equipment}, function(data){
          $('#equipment_desc').attr('placeholder', data);
        });
        $.post('ajax/UP_INV_EQUIPMENT_GET_ID.php', {equipment: equipment}, function(data){
          $('#equipment_id').attr('value', data);
        });
        $('#message').html("");
      } else {
        itemIsValid = false;
        $('#equipmentvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#equipment').val('');
        $('#equipment').focus();
        $('#message').html("Invalid Equipment");
      }
    });
  } else {
    equipmentIsValid = false;
      $('#equipment').val('');
      validate_filled();
  }
});

$(function(){
    $("#serial").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_CALIBRATE_SERIAL_QUERY.php",
          data: {
            equipment: $("#equipment").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});




$('#serial').blur(function(){
  if (  $('#serial').val().length   >   0  ){
    $('#serial').val($(this).val().toUpperCase());
    var equipment = $('#equipment').val();
    var serial = $('#serial').val();
    $.post('ajax/UP_INV_EQUIPMENT_CALIBRATE_SERIAL_VALIDATE.php', {equipment: equipment, serial: serial}, function(data){
      if (data > 0){
        serialIsValid = true;
              $('#serialvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              $('#frequency').focus();
              validate_filled();
              var equipment_id = $('#equipment_id').val();
              $.post('ajax/UP_INV_EQUIPMENT_SERIAL_GET_ID.php', {equipment_id: equipment_id, serial: serial}, function(data){
                $('#serial_id').attr('value', data);
              });
      } else {
        serialIsValid = false;
              $('#serialvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Invalid Serial Number. Either Serial number does not exist or is already in use.");
              $('#serial').val('');
              $('#serial').focus();
              validate_filled();
      }
    });
  } else {
            serialIsValid = false;

              $('#serial').val('');
              validate_filled();
  }
});

$('#frequency').blur(function(){
 if($('#frequency').val().length>0){
  var frequency = $('#frequency').val();
  if ($.isNumeric(frequency)){
    if (frequency < 1000 && frequency > 0){
      frequencyIsValid = true;
      $('#frequencyvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
      $("#initial_date").focus();
    } else {
      $('#frequencyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Invalid Frequency.  Please enter a number between 1 and 999.");
      $('#frequency').val('');
      $('#frequency').focus();
      validate_filled();
    }
  } else {
    frequencyIsValid = false;
    $('#frequency').val('');
  }
 } else {
  frequencyIsValid = false;
  $('#frequency').val('');
  validate_filled();
 }
});

$("#last_user").blur(function(){
  if (  $('#last_user').val().length   >   0  ){
    lastUserIsValid = true;
    $('#uservalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    validate_filled();
    $('#calibration_submit').focus();
  } else {
    lastUserIsValid = false;
      $('#last_user').val('');
      validate_filled();
  }
});


function validate_date(){
  if($('#initial_date').val().length   >   0){
    dateIsValid = true;
  } else {
    dateIsValid = false;
  }
};



function validate_filled(){
    validate_date();
    if (equipmentIsValid && serialIsValid && frequencyIsValid && dateIsValid && lastUserIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};






</script>

    </body>
</html>
