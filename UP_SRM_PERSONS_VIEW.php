<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMPERSONSVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Upward Persons Listing</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>Street Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Edit Person</th>
         </tr>
          <?php

            $persons = $database->table('mod43fordpoc.dbo.srm_persons')->all()->get();
          
            $count = count($persons);
            $l = 0;

            while ($l < $count)
            {
              echo '<tr>';
              echo '<td>'.$persons[$l]['first_name'].'</td>';
              echo '<td>'.$persons[$l]['middle_name'].'</td>';
              echo '<td>'.$persons[$l]['last_name'].'</td>';
              echo '<td>'.$persons[$l]['email_address'].'</td>';
              echo '<td>'.$persons[$l]['telephone_number'].'</td>';
              echo '<td>'.$persons[$l]['street_address'].'</td>';
              echo '<td>'.$persons[$l]['city'].'</td>';
              echo '<td>'.$persons[$l]['state'].'</td>';
              echo '<td>'.$persons[$l]['postal_code'].'</td>';
              echo '<td><a href="UP_SRM_PERSONS_EDIT.php?person='.$persons[$l]['person_id'].'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
              echo '</tr>';
              $l++;
            }
            
          ?>
          </table>
          </div>

  </div>

</body>
</html>