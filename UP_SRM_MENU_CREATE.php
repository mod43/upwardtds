<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENUCREATE';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Function</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_FUNCTION_CREATE_PROCESS.php' method='post'>
                
                    <div class="form-group">
                      <label for="func" class="control-label col-md-2">Function Name</label>
                        <div class="col-md-8">
                          <input type='text' name='func' id='func' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='functionnamevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Function Shortcode</label>
                        <div class="col-md-8">
                          <input type='text' name='shortcode' id='shortcode' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='shortcodevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="description" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                          <input type='text' name='description' id='description' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='descriptionvalid'></span>
                        </div> 
                     </div>
                    <div class="form-group">
                      <label for="filename" class="control-label col-md-2">Filename</label>
                        <div class="col-md-8">
                          <input type='text' name='filename' id='filename' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='filenamevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="enabled_flag" class="control-label col-md-2">Enabled</label>
                        <div class="col-md-8">
                          <select class="form-control" name="enabled_flag" id="enabled_flag">
                            <option value="1" selected>Yes</option>
                            <option value="2">No</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>   
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='function_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>  
<script>
var functionNameIsValid = null;
var functionShortcodeIsValid = null;
var functionFilenameIsValid = null;

function validate_filled(){
    if (functionNameIsValid && functionShortcodeIsValid && functionFilenameIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$(document).ready(function(){
    validate_filled();
    $('#func').change(validate_filled);
    $('#shortcode').change(validate_filled);
    $('#description').change(validate_filled);
    $('#filename').change(validate_filled);
});

$("#func").blur(function(){
  if($('#func').val().length   >   0){
      var func = $('#func').val();
      $.post('ajax/UP_SRM_FUNCTION_NAME_VALID.php', {func: func}, function(data){
      if (data == 0){
              functionNameIsValid = true;
              $('#functionnamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#shortcode').focus();
                    } 
      else if (data > 0){
              functionNameIsValid = false;
              $('#functionnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Function name exists already.  Please enter a unique function name.");
              $('#func').val('');
              $('#func').focus();
                    } 
      else {
              functionNameIsValid = false;
              $('#functionnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new function name.");
              $('#func').val('');
                    }           
    });
  }
});

$("#shortcode").blur(function(){
  if($('#shortcode').val().length   >   0){
      $('#shortcode').val($(this).val().toUpperCase());
      var shortcode = $('#shortcode').val();
      $.post('ajax/UP_SRM_FUNCTION_SHORTCODE_VALID.php', {shortcode: shortcode}, function(data){
      if (data == 0){
              functionShortcodeIsValid = true;
              $('#shortcodevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#description').focus();
                    } 
      else if (data > 0){
              functionShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Function shortcode exists already.  Please enter a unique function shortcode.");
              $('#shortcode').val('');
              $('#shortcode').focus();
                    } 
      else {
              functionShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new function shortcode.");
              $('#shortcode').val('');
                    }           
    });
  }
});

$("#filename").blur(function(){
  if($('#filename').val().length   >   0){
      var filename = $('#filename').val();
      $.post('ajax/UP_SRM_FUNCTION_FILENAME_VALID.php', {filename: filename}, function(data){
      if (data == 0){
              functionFilenameIsValid = true;
              $('#filenamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#enabled_flag').focus();
                    } 
      else if (data > 0){
              functionFilenameIsValid = false;
              $('#filenamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Function filename exists already.  Please enter a unique function filename.");
              $('#filename').val('');
              $('#filename').focus();
                    } 
      else {
              functionFilenameIsValid = false;
              $('#filenamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new function filename.");
              $('#filename').val('');
                    }           
    });
  }
});



</script>

    </body>
</html>