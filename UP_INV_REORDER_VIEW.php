<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVROVIEW';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
$site_id = $_SESSION['site_id'];

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Re-order Planning View</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Item</th>
            <th>Minimum Quantity</th>
            <th>Maximum Quantity</th>
            <th>Order Multiple</th>
            <th>Current Onhand</th>
            <th>Action Recommended</th>
         </tr>
          <?php
            $all_items = $database->table('up_inv_reorder_quantities')->where('site_id', '=', $site_id)->get();

            $count = count($all_items);
            $l = 0;

            while ($l < $count)
            {

              echo '<tr>';
              echo '<td>'.$all_items[$l]->item_name.'</td>';
              echo '<td>'.$all_items[$l]->minimum_quantity.'</td>';
              echo '<td>'.$all_items[$l]->maximum_quantity.'</td>';
      			  echo '<td>'.$all_items[$l]->order_multiple_quantity.'</td>';
      			  echo '<td>'.$all_items[$l]->current_onhand_qty.'</td>';
              $current_qty = $all_items[$l]->current_onhand_qty;
              $min_qty = $all_items[$l]->minimum_quantity;
              $max_qty = $all_items[$l]->maximum_quantity;
              $order_qty = $all_items[$l]->order_multiple_quantity;

      			  if($current_qty > $max_qty){
                echo '<td>Item is stocked at or above required quantity. No action is needed.</td>';
              } elseif($current_qty <= $min_qty){
                $reorder_multiple_qty = floor(($max_qty - $current_qty)/$order_qty);
                echo '<td>Order '.$reorder_multiple_qty.' multiples of the item at the reorder multiple of '.$order_qty.' eaches. ('.$reorder_multiple_qty*$order_qty.' Total)</td>';
              } else {
                echo '<td>Item is stocked above minimum, but may require reorder soon.</td>';
              }
              echo '</tr>';
              $l++;

            }

          ?>
          </table>
          </div>

  </div>

</body>
</html>
