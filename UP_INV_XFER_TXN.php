
<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVXFER';
$return_message = null;

require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }


    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }


include 'header.php'; //includes the navigation header
?>

<!-- Set up the Form -->
<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Inventory Transfer</h1></center>

</div>
<div class="panel-body">



              <form class="form-horizontal" action='ajax/UP_INV_XFER_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="item" class="control-label col-md-3">Item</label>
                        <div class="col-md-7">
                          <input type='text' name='item' id='item' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-2">
                          <span id='itemvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="from_group" class="control-label col-md-3">From Group</label>
                        <div class="col-md-7">
                          <input type='text' name='from_group' id='from_group' value='' class='form-control' tabindex='2'>
                        </div>
                        <div class="col-md-2">
                          <span id='fromgroupvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="from_location" class="control-label col-md-3">From Location</label>
                        <div class="col-md-7">
                          <input type='text' name='from_location' id='from_location' value='' class='form-control' tabindex='3'>
                        </div>
                        <div class="col-md-2">
                          <span id='fromlocationvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="to_group" class="control-label col-md-3">To Group</label>
                        <div class="col-md-7">
                          <input type='text' name='to_group' id='to_group' value='' class='form-control' tabindex='4'>
                        </div>
                        <div class="col-md-2">
                          <span id='togroupvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="to_location" class="control-label col-md-3">To Location</label>
                        <div class="col-md-7">
                          <input type='text' name='to_location' id='to_location' value='' class='form-control' tabindex='5'>
                         </div>
                         <div class="col-md-2">
                          <span id='tolocationvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="quantity" class="control-label col-md-3">Quantity</label>
                        <div class="col-md-7">
                          <input type='text' name='quantity' id='quantity' value='' class='form-control' placeholder='Available Quantity: ' tabindex='6'>
                       </div>
                       <div class="col-md-2">
                          <span id='quantityvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                          <input type="submit" value="Submit" id='inv_xfer_submit' class="btn btn-primary btn-block" tabindex='7'>
                        </div>
                      </div>
              </form>


            </div>
          </div>

        <div id='return_message'>
          <?php

            if ($return_message == 'success')
            {
              echo '<p class="bg-success" id="return">Item Transfer Successful</p>';
            } elseif ($return_message == 'error'){
              echo '<p class="bg-danger" id="return">Item Transfer ended in error.</p>';
            }



          ?>
        </div>
</div>
<script type="text/javascript">

var itemIsValid = null;
var fromGroupIsValid = null;
var fromLocationIsValid = null;
var toGroupIsValid = null;
var toLocationIsValid = null;
var quantityIsValid = null;
var available_quantity = 0;

// item autocomplete
$(function() {
  $("#item").autocomplete({
    source: "ajax/UP_INV_ITEM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});


// item validation
$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data > 0){
        itemIsValid = true;
        $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
        $("#group").focus();
        $('#message').html("");
        validate_filled();
      } else {
        itemIsValid = false;
        $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#item').val('');
        $('#item').focus();
        $('#message').html("Invalid Item");
        validate_filled();
      }
    });
  } else {
    itemIsValid = false;
      $('#item').val('');
      validate_filled();
  }
});


$(function() {
  $("#from_group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});



$("#from_group").blur(function(){
  if (  $('#from_group').val().length   >   0  ){
    $('#from_group').val($(this).val().toUpperCase());
    var group = $('#from_group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        fromGroupIsValid = true;
              $('#fromgroupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#from_location').focus();
              $('#message').html("");
              validate_filled();
      } else {
        fromGroupIsValid = false;
              $('#fromgroupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#from_group').val('');
              $('#from_group').focus();
              $('#message').html("Invalid From Group");
              validate_filled();
      }
    });
  } else {
    fromGroupIsValid = false;
      $('#from_group').val('');
      validate_filled();
  }
});

$(function(){
    $("#from_location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#from_group").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#from_location").blur(function(){
  if (  $('#from_location').val().length   >   0  ){
    $('#from_location').val($(this).val().toUpperCase());
    var location = $('#from_location').val();
    var group = $("#from_group").val()
    $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
      if (data > 0){
        fromLocationIsValid = true;
        quantity_enabled();
              $('#fromlocationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#to_group').focus();
              $('#message').html("");
              validate_filled();
      } else {
        fromLocationIsValid = false;
              $('#fromlocationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#from_location').val('');
              $('#from_location').focus();
              $('#quantity').attr('placeholder', ("Available Quantity: "));
              $('#message').html("Invalid From Location");
              validate_filled();
      }
    }); // END POST COMMAND FOR LOCATION VALIDATION
        if ($('#from_group').val().length  >   0   &&
            $('#from_location').val().length    >   0 ){
              var fromloc = $('#from_location').val();
              var fromgroup = $('#from_group').val();
              var item = $('#item').val();
              $.post('ajax/UP_INV_QTYBYLOC_QUERY.php', {fromloc: fromloc, fromgroup: fromgroup, item: item}, function(data){
              $('#quantity').attr('placeholder', ("Available Quantity: " + data));
              available_quantity = data;
              });
        }
    } else {
            fromLocationIsValid = false;
            $('#quantity').attr('placeholder', ("Available Quantity: "));
            validate_filled();
    }
});



$(function() {
  $("#to_group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#to_group").blur(function(){
  if (  $('#to_group').val().length   >   0  ){
    $('#to_group').val($(this).val().toUpperCase());
    var group = $('#to_group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        toGroupIsValid = true;
              $('#togroupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
      } else {
        toGroupIsValid = false;
              $('#togroupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#to_group').val('');
              $('#message').html("Invalid To Group");
              validate_filled();
      }
    });
  } else {
    toGroupIsValid = false;
      $('#to_group').val('');
      validate_filled();
  }
});

$(function(){
    $("#to_location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#to_group").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#to_location").blur(function(){
  if (  $('#to_location').val().length   >   0  ){
    $('#to_location').val($(this).val().toUpperCase());
    var location = $('#to_location').val();
    var group = $("#to_group").val()
    $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
      if (data > 0){
        toLocationIsValid = true;
              $('#tolocationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#quantity').focus();
              $('#message').html("");
      } else {
        toLocationIsValid = false;
              $('#tolocationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#to_location').val('');
              $('#message').html("Invalid To Location");
      }
    });
  } else {
          toLocationIsValid = false;
          $('#to_location').val('');
          $('#quantity').attr('placeholder', ("Available Quantity: "));
          validate_filled();
  }
});



$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_norm = $('#quantity').val();
        var qty_ent = parseInt($('#quantity').val()); //create integer
        var qty_avl = parseInt(available_quantity);  //create integer
          if (qty_ent > qty_avl){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#site_id').html("Quantity entered is greater than available quantity.");
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
          } else if ((qty_norm % 1) != 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be in full integer quantities.  No decimals allowed.");
            $('#quantity').val('');
            $('#quantity').focus();
            validate_filled();
          } else if (qty_ent <= qty_avl) {
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});

$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    quantity_enabled();
    validate_filled();
    $('#item').focus();
    $('#item, #from_group, #from_location, #to_group, #to_location').blur(quantity_enabled);
    $('#item, #from_group, #from_location, #to_group, #to_location, #quantity').blur(validate_filled);
});



function validate_filled(){
    if (itemIsValid &&
        fromGroupIsValid &&
        fromLocationIsValid &&
        toGroupIsValid &&
        toLocationIsValid &&
        quantityIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("input[type=submit]").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


function quantity_enabled(){
    if (itemIsValid &&
        fromGroupIsValid &&
        fromLocationIsValid) {
       $('#quantity').attr("disabled", false);
    } else {
      $('#quantity').attr("disabled", true);
    }
};


</script>


</body>
</html>
