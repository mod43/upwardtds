<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMUSERSVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Upward User Listing</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Username</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Edit Access</th>
         </tr>
          <?php

            $users = $database->query('select su.user_name, sp.first_name, sp.last_name from mod43fordpoc.dbo.srm_users su LEFT JOIN srm_persons sp
                                        on su.person_id = sp.person_id')->get();

            $count = count($users);
            $l = 0;

            while ($l < $count)
            {
              echo '<tr>';
              echo '<td>'.$users[$l]['user_name'].'</td>';
              echo '<td>'.$users[$l]['first_name'].'</td>';
              echo '<td>'.$users[$l]['last_name'].'</td>';
              echo '<td><a href="UP_SRM_USER_RESP_CREATE.php?username='.$users[$l]['user_name'].'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
              echo '</tr>';
              $l++;
            }
            
          ?>
          </table>
          </div>

  </div>

</body>
</html>