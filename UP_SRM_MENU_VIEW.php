<?php
    require_once 'app/init.php';
    // Check if logged in
    if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
    	// die if not logged in
		die("Redirecting to index.php"); 
        header("Location: index.php");
    }
    unset($_SESSION['available_functions']); // remove any functions availble from previous resp choices
	$resp_details = $auth->getResponsibilityDetails($_SESSION['responsibility']); //grab resp details based on choice

	include 'header.php';
?>
<div class="col-sm-8">
<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">
	<center><h1 class="panel-title"><?php echo $resp_details[0]['responsibility_name']; ?></h1></center></div>
<div class="panel-body">
<?php

		
		$root_menu_lines = $auth->getMenuLinesView($resp_details[0]['responsibility_root_menu_id']);

		$count = count($root_menu_lines);



		$l = 0;

		var_dump($_SESSION);

		while($l < $count)
		{
				if($root_menu_lines[$l]['srm_menu_line_type_id'] == 1){


				$submenu_lines = $auth->getMenuLinesView($root_menu_lines[$l]['line_type_id']);
				
				$smcount = count($submenu_lines);
				
				$sm = 0;
				echo '<div class="panel-group" id="accordion">
        			<div class="panel panel-default">
           				 <div class="panel-heading">
                			<h4 class="panel-title">
                    			<a data-toggle="collapse" data-parent="#accordion" href="#'.$submenu_lines[$sm]['srm_menu_shortcode'].'"><span class="glyphicon glyphicon-plus"> </span>';
									
									echo "  ".$submenu_lines[$sm]['srm_menu_name']."<br>";
				echo ' 	        </a>
                			</h4>
            			</div>';
				echo '	<div id="'.$submenu_lines[$sm]['srm_menu_shortcode'].'" class="panel-collapse collapse">
                			<div class="panel-body">
                			    <table class="table table-striped">';
					while($sm < $smcount)
					{
						

						$_SESSION['available_functions'][] = $submenu_lines[$sm]['line_type_id'];

						echo '			<tr>
					  						<td><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>
					  						<a href="'.$submenu_lines[$sm]['line_filename'].'">'.$submenu_lines[$sm]['line_type_name'].'</td>
					  					</tr>';

						$sm++;
					}

				echo '			</table>
                			</div>
            			</div>
            		</div>';

			} else {
					//$function_details = $auth->getFunctionDetails($root_menu_lines[$l]->line_type_id);
					$_SESSION['available_functions'][] = $root_menu_lines[$l]['line_type_id'];

					echo '<div class="panel-heading">
                			<h4 class="panel-title">
                    			<a data-parent="#accordion" href="'.$root_menu_lines[$l]['line_filename'].'">  <span class="glyphicon glyphicon-transfer"> </span>';
                    echo '  '.$root_menu_lines[$l]['line_type_name'].'</a>
                    		</h4>
                    	</div>';

			}
			
			$l++;
		}
		
?>
</div>
</body>
</html>
