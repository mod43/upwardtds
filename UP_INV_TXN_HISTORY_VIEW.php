<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVTXNHISTORYVIEW';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
$site_id = $_SESSION['site_id'];

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Item Transaction History</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Transaction Type</th>
            <th>Item</th>
            <th>From Group</th>
            <th>From Location</th>
            <th>To Group</th>
            <th>To Location</th>
            <th>UOM</th>
            <th>Quantity</th>
            <th>Created User</th>
            <th>Update User</th>
            <th>Comments</th>
         </tr>
          <?php
            $all_history = $database->table('mod43fordpoc.dbo.up_inv_txn_history_view')->where('site_id', '=', $site_id)->get();

            $count = count($all_history);
            $l = 0;

            while ($l < $count)
            {

              echo '<tr>';
              echo '<td>'.$all_history[$l]['transaction_type'].'</td>';
              echo '<td>'.$all_history[$l]['item'].'</td>';
              echo '<td>'.$all_history[$l]['from_group'].'</td>';
      			  echo '<td>'.$all_history[$l]['from_location'].'</td>';
              echo '<td>'.$all_history[$l]['to_group'].'</td>';
              echo '<td>'.$all_history[$l]['to_location'].'</td>';
      			  echo '<td>'.$all_history[$l]['uom'].'</td>';
      			  echo '<td>'.$all_history[$l]['quantity'].'</td>';
              echo '<td>'.$all_history[$l]['created_by_user'].'</td>';
              echo '<td>'.$all_history[$l]['updated_by_user'].'</td>';
              echo '<td>'.$all_history[$l]['comment'].'</td>';
              echo '</tr>';
              $l++;

            }

          ?>
          </table>
          </div>

  </div>

</body>
</html>
