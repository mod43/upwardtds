<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMPEOPLE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }


include 'header.php'; //includes the navigation header

if(isset($_GET['person'])){
  $person_id = $_GET['person'];

  $person_info = $database->table('srm_persons')->where('person_id', '=', $person_id)->first();

}

?>



<div class="col-sm-8">

<div class="alert alert-warning" role="alert">Person records cannot be edited once created due to data integrity.  If you require changes, please contact support.</div>

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Person</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='#' method='post'>

                    <div class="form-group">
                      <label for="person" class="control-label col-md-2">Last Name</label>
                        <div class="col-md-8">
                          <input type='text' name='lastname' id='lastname' value='<?php echo $person_info->last_name; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='lastnamevalid'></span>
                        </div>
                     </div>

                    <div class="form-group">
                      <label for="person" class="control-label col-md-2">First Name</label>
                        <div class="col-md-8">
                          <input type='text' name='firstname' id='firstname' value='<?php echo $person_info->first_name; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='firstnamevalid'></span>
                        </div>
                      </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Middle Name</label>
                        <div class="col-md-8">
                          <input type='text' name='middlename' id='middlename' value='<?php echo $person_info->middle_name; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='middlenamevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Email</label>
                        <div class="col-md-8">
                          <input type='text' name='email' id='email' value='<?php echo $person_info->email_address; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='emailvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='emailmessage'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Phone</label>
                        <div class="col-md-8">
                          <input type='text' name='phone' id='phone' value='<?php echo $person_info->telephone_number; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='phonevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='phonemessage'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Address</label>
                        <div class="col-md-8">
                          <input type='text' name='address' id='address' value='<?php echo $person_info->street_address; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='addressvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">City</label>
                        <div class="col-md-8">
                          <input type='text' name='city' id='city' value='<?php echo $person_info->city; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='cityvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">State</label>
                        <div class="col-md-8">
                          <input type='text' name='state' id='state' value='<?php echo $person_info->state; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='statevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>

                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Postal Code</label>
                        <div class="col-md-8">
                          <input type='text' name='postal' id='postal' value='<?php echo $person_info->postal_code; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='postalvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>

                     <div class="form-group">
                         <div class="col-md-offset-2 col-md-8">
                          <a href="UP_SRM_PERSONS_VIEW.php"><input type="button" value="Return to Person List" id='group_submit' class="btn btn-primary btn-block"></a>
                        </div>
                      </div>

              </form>
  </div>
</div>

<script>

var lastNameIsValid = null;
var firstNameIsValid = null;
var middleNameIsValid = null;
var emailIsValid = null;
var phoneIsValid = null;
var addressIsValid = null;
var cityIsValid = null;
var stateIsValid = null;
var postalIsValid = null;

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};

function normalizePhone(phone) {
    //normalize string and remove all unnecessary characters
    phone = phone.replace(/[^\d]/g, "");

    //check if number length equals to 10
    if (phone.length == 10) {
        //reformat and return phone number
        return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    return null;
}


$(document).ready(function (){
    validate_filled();
    $('#lastname, #firstname, #middlename, #email, #phone, #address, #city, #state, #postal').change(validate_filled);
});


$('#lastname').blur(function(){
  if ($('#lastname').val().length > 0){
    $('#lastname').val($(this).val().toUpperCase());
    lastNameIsValid = true;
    $('#lastnamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#firstname').focus();
  }
});

$("#firstname").blur(function(){
  if (  $('#firstname').val().length   >   0  ){
    $('#firstname').val($(this).val().toUpperCase());
    var firstname = $('#firstname').val();
    var lastname = $('#lastname').val();
    $.post('ajax/UP_SRM_PERSON_VALIDATE.php', {firstname: firstname, lastname: lastname}, function(data){
      if (data == 0){
              firstNameIsValid = true;
              $('#firstnamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#middlename').focus();
      } else if (data > 0){
        firstNameIsValid = false;
              $('#firstnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Person exists already.  Please enter a unique first and last name combination.");
              $('#firstname').val('');
              $('#firstname').focus();
      } else {
        firstNameIsValid = false;
         $('#firstnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please enter new name.");
              $('#firstname').val('');
      }
    });
  }
});

$('#middlename').blur(function(){
  if ($('#middlename').val().length > 0){
    $('#middlename').val($(this).val().toUpperCase());
    middleNameIsValid = true;
    $('#middlenamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#email').focus();
  }
});

$('#email').blur(function(){
  if ($('#email').val().length > 0){
    $('#email').val($(this).val().toUpperCase());
    if(isValidEmailAddress($('#email').val())){
    emailIsValid = true;
    $('#emailvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#phone').focus();
  } else {
    $('#emailvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
    $('#emailmessage').html("Please use correct email format - name@domain.com for example");
    $('#email').val('');
    $('#email').focus();
  }
  }
});

$('#phone').blur(function(){
  if ($('#phone').val().length > 0){
    var phoneNumber = normalizePhone($('#phone').val());
    if (phoneNumber != null){
    phoneIsValid = true;
    $('#phone').val(phoneNumber);
    $('#phonevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#address').focus();
  } else {
    $('#phonevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
    $('#phonemessage').html("Please use correct email format - name@domain.com for example");
    $('#phone').val('');
    $('#phone').focus();
  }
  }
});

$('#address').blur(function(){
  if ($('#address').val().length > 0){
    $('#address').val($(this).val().toUpperCase());
    addressIsValid = true;
    $('#addressvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#city').focus();
  }
});

$('#city').blur(function(){
  if ($('#city').val().length > 0){
    $('#city').val($(this).val().toUpperCase());
    cityIsValid = true;
    $('#cityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#state').focus();
  }
});

$('#state').blur(function(){
  if ($('#state').val().length > 0){
    $('#state').val($(this).val().toUpperCase());
    stateIsValid = true;
    $('#statevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#postal').focus();
  }
});

$('#postal').blur(function(){
  if ($('#postal').val().length > 0){
    $('#postal').val($(this).val().toUpperCase());
    postalIsValid = true;
    $('#postalvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    validate_filled();
  }
});

$(document).ready(function (){
    validate_filled();

    $('#lastname, #firstname, #middlename, #email, #phone, #address, #city, #state, #postal').change(validate_filled);
});

function validate_filled(){
    if (lastNameIsValid &&
        firstNameIsValid &&
        middleNameIsValid &&
        emailIsValid &&
        phoneIsValid &&
        addressIsValid &&
        cityIsValid &&
        stateIsValid &&
        postalIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("input[type=submit]").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


</script>

              </body>
</html>
