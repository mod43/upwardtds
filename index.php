<?php

    require 'app/init.php';


        $submitted_username = '';


        if(!empty($_POST))
        {

            $username = $_POST['username'];
            $password = $_POST['password'];
            $signin_data = array('username' => $username, 'password1' => $password);
            //var_dump($signin_data);
            $signin = $auth->signin($signin_data);/*['username'] = $username;
                    array('password') = $password;*/



            if($signin)
            {

            //echo 'Signed In';

               header('Location: UP_SRM_RESPONSIBILITIES_VIEW.php'); //Location: test_session.php');
            }

        } else {

          $auth->signout();

        }

?>

<!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Upward Inventory Management</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="js/bootstrap.min.css">
        <link rel='shortcut icon' href='favicon.ico' type='image/x-icon'/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body>
        <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="padding: 3px 3px;"><img alt="Brand" width="43px" height="43px" src="img/nav_bar_logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="navbar-text">Upward Inventory Management</li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
        </ul>
      </div>
    </div>
</nav>
<div class="col-md-8 col-md-offset-2">
        <div class="jumbotron">
            <div class="container"><center>
                <h1><img class="img-responsive" src='img/main_logo.png'></h1>
                </center>
            </div>
        <div class="container"><center>
            <h3>Login</h3>
            </center>
        </div>
            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="index.php" method="post">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                                <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1" name="username" value="" autocomplete="off" />
                            </div>
                                </br>
                            <div class="input-group">
                                <span class="input-group-addon" id"basic-addon2"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon2" value="" />
                            </div>
                                </br>
                            <div class="form-group">
                                <center><input type="submit" class="btn btn-primary btn-md" value="Login" /></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>


</body>
</html>
