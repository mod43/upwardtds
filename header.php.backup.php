<?php

if (!isset($_SESSION['responsibility']))
{
echo '<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Upward Inventory Management</title>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="js/bootstrap.min.css">
      <link rel="stylesheet" href="js/jquery-ui.min.css" type="text/css" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
      <script src="js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui.min.js"></script>  ';

      echo '<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php" style="padding: 3px 3px;"><img alt="Brand" height="43px" width="43px" src="/img/nav_bar_logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="navbar-text">Upward Inventory</li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span><span> </span>'.$_SESSION['username'].'<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="responsibilities.php">Change Responsibility</a></li>
            <li><a href="#">Account Info</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>';
} else {

$current_responsibility = $_SESSION['responsibility'];

  echo '<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Upward Inventory Management</title>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
      <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>  ';

      echo '<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php" style="padding: 3px 3px;"><img alt="Brand" height="43px" width="43px" src="/img/nav_bar_logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="navbar-text">Upward Inventory</li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">';


        $header_resp_details = $auth->getResponsibilityDetails($_SESSION['responsibility']);
        echo $header_resp_details->responsibility_name;
        echo ' <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">';


        $header_root_menu = $header_resp_details->responsibility_root_menu_id;
        $menu_header = $auth->getMenuDetails($header_root_menu);
        $menu_lines = $auth->getMenuLines($header_root_menu);
        $count = count($menu_lines);
        $k = 0;

        while($k < $count)
        {
            if(!$k == 0)
            {
               echo '<li role="separator" class="divider"></li>';
            }

            if($menu_lines[$k]->menu_line_type_id == 1)
            {
              $submenu_header = $auth->getMenuDetails($menu_lines[$k]->menu_line_submenu_id);

              echo "<li class='dropdown-header'>".$submenu_header->menu_name."</li>";
              $submenu_lines = $auth->getMenuLines($menu_lines[$k]->menu_line_submenu_id);
              $smcount = count($submenu_lines);
              $sm = 0;
                while($sm < $smcount)
                {

                  $function_details = $auth->getFunctionDetails($submenu_lines[$sm]->menu_line_function_id);
                  $_SESSION['available_functions'][] = $function_details->function_id;

                  echo '<li><a href="'.$function_details->filename.'">'.$function_details->function_name.'</a></li>';

                  $sm++;

                }


            } else {

                $function_details = $auth->getFunctionDetails($menu_lines[$k]->menu_line_function_id);
                $_SESSION['available_functions'][] = $function_details->function_id;

                echo '<li><a href="'.$function_details->filename.'">'.$function_details->function_name.'</a></li>';
                    }


            $k++;


          }


        echo '</ul>
        </li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span><span> </span>'.$_SESSION['username'].'<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="responsibilities.php">Change Responsibility</a></li>
            <li><a href="#">Account Info</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>';
}

?>
