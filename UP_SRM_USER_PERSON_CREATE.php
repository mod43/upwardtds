<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMUSERPERSONCREATE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }
if(isset( $_GET['username'])){
$username = $_GET['username'];
} else {
  header("Location: UP_SRM_USERS_VIEW.php");
  die();
}

include 'header.php'; //includes the navigation header

var_dump($function_access);
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create User to Person Assignment</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_USER_PERSON_CREATE_PROCESS.php' method='post' id='user_resp'>

                    <div class="form-group">
                      <label for="resp" class="control-label col-md-2">Username</label>
                        <div class="col-md-8">
                          <input type='text' name='user' id='user' value='<?php echo $username; ?>' class='form-control' readonly>
                        </div>
                        <div class="col-md-2">
                          <span id='respnamevalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="person" class="control-label col-md-2">Person</label>
                        <div class="col-md-8">
                          <select class="form-control" name="person" id="person">
                            <option value="unselected">Select Person...</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="button" value="Submit" id='resp_submit' class="btn btn-primary btn-block" data-toggle="modal" data-target="#confirm-submit" >
                        </div>
                      </div>
              </form>
  </div>
</div>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Confirm Submit
            </div>
            <div class="modal-body">
                Are you sure you want to link the following user to the person listed?  This action can not be undone.

                <!-- We display the details entered by the user here -->
                <table class="table">
                    <tr>
                        <th>Username</th>
                        <td id="modal_username"></td>
                    </tr>
                    <tr>
                        <th>Person</th>
                        <td id="modal_person"></td>
                    </tr>
                </table>

            </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <a href="#" id="submit" class="btn btn-success success">Submit</a>
        </div>
    </div>
  </div>
</div>
<script>
var userNameIsValid = true;
var userPersonIsSelected = null;

$('#resp_submit').click(function() {
     /* when the button in the form, display the entered values in the modal */
     $('#modal_username').html($('#user').val());
     $('#modal_person').html($('#person option:selected').text());
});

$('#submit').click(function(){
     /* when the submit button in the modal is clicked, submit the form */
    $('#user_resp').submit();
});


function validate_filled(){
    if (userNameIsValid && userPersonIsSelected) {
        $("#resp_submit").attr("disabled", false);
        $("#resp_submit").focus();
    }
    else {
        $("#resp_submit").attr("disabled", true);
    }
};

$(document).ready(function(){
    $.getJSON('ajax/UP_SRM_GET_PERSONS_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#person').append($('<option></option>').val(key).html(value));
        });
    });
    validate_filled();
});

$('#person').change(function(){
  var personValue = $('#person').val();

  if (personValue != 'unselected'){
    userPersonIsSelected = true;
    validate_filled();
  }
});


</script>

    </body>
</html>
