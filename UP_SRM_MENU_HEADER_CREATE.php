<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENUCREATE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Menus</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_MENU_HEADER_CREATE_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="menu" class="control-label col-md-2">Menu Name</label>
                        <div class="col-md-8">
                          <input type='text' name='menu' id='menu' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='menunamevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Menu Shortcode</label>
                        <div class="col-md-8">
                          <input type='text' name='shortcode' id='shortcode' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='shortcodevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="description" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                          <input type='text' name='description' id='description' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='descriptionvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="menu_type" class="control-label col-md-2">Menu Type</label>
                        <div class="col-md-8">
                          <select class="form-control" name="menu_type" id="menu_type">
                            <option value="M" selected>Main Menu</option>
                            <option value="S">Submenu</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='menu_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>
<script>
var menuNameIsValid = null;
var menuShortcodeIsValid = null;
var descriptionIsValid = null;

function validate_filled(){
    if (menuNameIsValid && menuShortcodeIsValid && descriptionIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#menution_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$(document).ready(function(){
    validate_filled();
    $('#menu').change(validate_filled);
    $('#shortcode').change(validate_filled);
    $('#description').change(validate_filled);
});

$("#menu").blur(function(){
  if($('#menu').val().length   >   0){
      var menu = $('#menu').val();
      $.post('ajax/UP_SRM_MENU_NAME_VALIDATE.php', {menu: menu}, function(data){
      if (data == 0){
              menuNameIsValid = true;
              $('#menunamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#shortcode').focus();
              validate_filled();
                    }
      else if (data > 0){
              menuNameIsValid = false;
              $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Menu name exists already.  Please enter a unique menu name.");
              $('#menu').val('');
              $('#menu').focus();
              validate_filled();
                    }
      else {
              menuNameIsValid = false;
              $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new menu name.");
              $('#menu').val('');
              validate_filled();
                    }
    });
  } else {
    menuNameIsValid = false;
      $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Fields can not be blank. Please ensure all fields are filled in or selected.");
      $('#menu').val('');
      validate_filled();
  }
});

$("#shortcode").blur(function(){
  if($('#shortcode').val().length   >   0){
      $('#shortcode').val($(this).val().toUpperCase());
      $('#shortcode').val($(this).val().replace(/\s/g,''));
      var shortcode = $('#shortcode').val();
      $.post('ajax/UP_SRM_MENU_SHORTCODE_VALIDATE.php', {shortcode: shortcode}, function(data){
      if (data == 0){
              menuShortcodeIsValid = true;
              $('#shortcodevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#description').focus();
              validate_filled();
                    }
      else if (data > 0){
              menuShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Menu shortcode exists already.  Please enter a unique menu shortcode.");
              $('#shortcode').val('');
              $('#shortcode').focus();
              validate_filled();
                    }
      else {
              menuShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new menu shortcode.");
              $('#shortcode').val('');
              validate_filled();
                    }
    });
  } else {
    menuShortcodeIsValid = false;
      $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Fields can not be blank. Please ensure all fields are filled in or selected.");
      $('#shortcode').val('');
      validate_filled();
  }
});

$('#description').blur(function(){
  if($('#description').val().length   >   0){
    descriptionIsValid = true;
    $('#descriptionvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    $('#message').html("");
    $('#menu_type').focus();
    validate_filled();
  } else {
    descriptionIsValid = false;
      $('#descriptionvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Fields can not be blank. Please ensure all fields are filled in or selected.");
      $('#description').val('');
      validate_filled();
  }
});




</script>

    </body>
</html>
