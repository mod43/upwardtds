<?php

    require("config.php");
    if(empty($_SESSION['user'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

include 'pdo_creds.php';

include 'header.php';

	echo '<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Item Master Listing</h1></center>

</div>
<div class="panel-body">';

$dbh = new PDO($connection, $username, $password);

$sth = $dbh->prepare("select im.item_name, im.item_description, uom.uom_name, o.org_name, im.status
from item_master im, units_of_measure uom, organizations o
where im.org_id = o.org_id
and im.base_uom_id = uom.uom_id");
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);

/* Fetch all of the remaining rows in the result set */

echo '
				<div class="table-responsive">
				  <!-- Table -->
				  <table class="table">
				  <tr>
				  	<th>Item Name</th>
				  	<th>Description</th>
				  	<th>UOM</th>
				  	<th>Site</th>
				  	<th>Status</th>
				  	</tr>';



						foreach ($result as $row => $cur) {
							echo '<tr><td>'.$cur['item_name'].'</td><td>'.$cur['item_description'].'</td><td>'.$cur['uom_name'].'</td><td>'.$cur['org_name'].'</td><td>'.$cur['status'].'</td></tr>';
						}

						echo '</table>
						</div>

	</div>';

echo "</body>";
echo "</html>";
?>