<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVITEMRECEIPT';

$return_message = null;
require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
/*if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }

    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

*/
include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Inventory Receipt</h1></center>

  </div>
<div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->
              <form class="form-horizontal" id="UP_INV_ITEM_RCPT" action="ajax/UP_INV_ITEM_RCPT_PROCESS.php" method='post'>

                    <div class="form-group">
                      <label for="item" class="control-label col-sm-2">Item</label>
                        <div class="col-sm-8">
                          <input type='text' name='item' id='item' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-1">
                          <span id='itemvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="group" class="control-label col-sm-2">Group</label>
                        <div class="col-sm-8">
                          <input type='text' name='group' id='group' value='' class='form-control' tabindex='2'>
                        </div>
                        <div class="col-md-1">
                          <span id='groupvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="location" class="control-label col-sm-2">Location</label>
                        <div class="col-sm-8">
                          <input type='text' name='location' id='location' value='' class='form-control' tabindex='3'>
                        </div>
                        <div class="col-md-1">
                          <span id='locationvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="quantity" class="control-label col-sm-2">Quantity</label>
                        <div class="col-sm-8">
                          <input type='number' name='quantity' id='quantity' value='' class='form-control' placeholder='On-hand Quantity: ' tabindex='4'>
                       </div>
                       <div class="col-md-1">
                          <span id='quantityvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                          <input type="submit" value="Submit" id='inv_xfer_submit' class="btn btn-primary btn-block" tabindex='5'>
                        </div>
                     </div>
              </form>

</div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Item Receipt Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Item Receipt ended in error.</p>';
    }



  ?>
</div>
          <!--</div>
      </div>
</div>-->

<script type="text/javascript">

var itemIsValid = null;
var groupIsValid = null;
var locationIsValid = null;
var quantityIsValid = null;


// item autocomplete
$(function() {
  $("#item").autocomplete({
    source: "ajax/UP_INV_ITEM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});


// item validation
$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data > 0){
        itemIsValid = true;
        $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
        $("#group").focus();
        $('#message').html("");
      } else {
        itemIsValid = false;
        $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#item').val('');
        $('#item').focus();
        $('#message').html("Invalid Item");
      }
    });
  } else {
    itemIsValid = false;
      $('#item').val('');
      validate_filled();
  }
});


$(function() {
  $("#group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#group").blur(function(){
  if (  $('#group').val().length   >   0  ){
    $('#group').val($(this).val().toUpperCase());
    var group = $('#group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        groupIsValid = true;
              $('#groupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#location').focus();
              $('#message').html("");
      } else {
        groupIsValid = false;
              $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#group').val('');
              $('#group').focus();
              $('#message').html("Invalid Group");
      }
    });
  } else {
    groupIsValid = false;
      $('#group').val('');
      validate_filled();
  }
});

$(function(){
    $("#location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#group").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#location").blur(function(){
  if (  $('#location').val().length   >   0  ){
        $('#location').val($(this).val().toUpperCase());
        var location = $('#location').val();
        var group = $("#group").val()
        $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
        if (data > 0){
          locationIsValid = true;
          $('#locationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
          if (  itemIsValid && groupIsValid && locationIsValid ){
                var loc = $('#location').val();
                var group = $('#group').val();
                var item = $('#item').val();
                $.post('ajax/UP_INV_QTYBYLOC_QUERY.php', {fromloc: loc, fromgroup: group, item: item}, function(data){ //Note to standardize variables
                  available_quantity=data;
                  $('#quantity').attr('placeholder', ("Available Quantity: " + data));
                });
                }
              $('#quantity').focus();
              $('#message').html("");
      } else {
        locationIsValid = false;
              $('#quantity').attr('placeholder', ("Available Quantity: "));
                validate_filled();
      }
    });
  }
});




$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_norm = $('#quantity').val();
        var qty_ent = parseInt($('#quantity').val()); //create integer
          if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0.")
            $('#quantity').val('');
            validate_filled();
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
            validate_filled();
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
            validate_filled();
          } else if ((qty_norm%1) != 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be in full integer quantities.  No decimals allowed.");
            $('#quantity').val('');
            $('#quantity').focus();
            validate_filled();
          } else if (qty_ent > 0) {
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});


$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    validate_filled();
    document.title= 'Item Receipt';
    $('#item').focus();
    $('#item, #group, #location, #quantity').change(validate_filled);
});

function validate_filled(){
    if (itemIsValid &&
      groupIsValid &&
      locationIsValid &&
      quantityIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("input[type=submit]").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};




</script>


</body>
</html>
