<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVLOCATIONCREATE';

$return_message = null;
require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

include 'header.php'; //includes the navigation header

?>


<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Location</h1></center>


  </div>
  <div class="panel-body">
<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_LOC_CREATE_PROCESS.php' method='post'>
                  
                    <div class="form-group">
                      <label for="item_desc" class="control-label col-md-2">Group</label>
                        <div class="col-md-8">
                          <input type='text' name='group' id='group' value='' class='form-control'>
                        </div>
                         <div class="col-md-2">
                          <span id='groupvalid'></span>
                        </div> 
                     </div>
                    <div class="form-group">
                      <label for="location" class="control-label col-md-2">Location</label>
                        <div class="col-md-8">
                          <input type='text' name='location' id='location' value='' class='form-control'>
                        </div>
                         <div class="col-md-2">
                          <span id='locationvalid'></span>
                        </div> 
                     </div> 
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>  
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='location_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>
<div id='return_message'>
  <?php

    if ($return_message == 'Success')
    {
      echo '<p class="bg-success" id="return">Locator Created Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Locator Creation ended in error</p>';
    }



  ?>
</div>
</div>

<script>

var groupIsValid = null;
var locationIsValid = null;

$(document).ready(function (){
      $('#return_message').delay(5000).fadeOut();
    validate_filled();
    $('#group', '#location').change(validate_filled);
    $('#group').focus();
});

$(function() {
  $("#group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });       
});

$("#group").blur(function(){
  if (  $('#group').val().length   >   0  ){
    $('#group').val($(this).val().toUpperCase());
    var group = $('#group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        groupIsValid = true;
              $('#groupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#location').focus();
      } else {
        locationIsValid = false;
              $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#group').val('');
              $('#group').focus();
              $('#message').html("Group is invalid. Please enter valid group");
      }
    });
  }
});


$("#location").blur(function(){
  if (  $('#location').val().length   >   0  ){
    $('#location').val($(this).val().toUpperCase());
    var location = $('#location').val();
    var hasSpace = $('#location').val().indexOf(' ')>=0;
    var group = $('#group').val();
    if (!hasSpace){
    $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
      if (data == 0){
              locationIsValid = true;
              $('#locationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#location_submit').focus();
      } else if (data > 0){
        locationIsValid = false;
              $('#locationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Location exists already.  Please enter a unique location name.");
              $('#location').val('');
              $('#location').focus();
      } else {
        locationIsValid = false;
         $('#locationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new location name.");
              $('#location').val('');
      }
    });
    } else {
      $('#locationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Location names cannot contain spaces.  Please enter a different locaiton name.");
      $('#location').val('');
      $('#location').focus();
    }
  }
});

function validate_filled(){
    if (groupIsValid &&
        locationIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#location_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


</script>


</body>
</html>
