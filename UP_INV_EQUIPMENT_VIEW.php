<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQUIPMENTVIEW';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
$site_id = $_SESSION['site_id'];

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Equipment Master Listing</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Equipment</th>
            <th>Description</th>
            <th>Status</th>
            <th>Serial Enabled</th>
            <th>Calibrated</th>
         </tr>
          <?php
            $all_equipments = $database->table('mod43fordpoc.dbo.inv_equipment_master')->where('site_id', '=', $site_id)->get();

            $count = count($all_equipments);
            $l = 0;

            while ($l < $count)
            {

              echo '<tr>';
              echo '<td>'.$all_equipments[$l]['equipment_name'].'</td>';
              echo '<td>'.$all_equipments[$l]['equipment_description'].'</td>';
      			  echo '<td>'.$all_equipments[$l]['status'].'</td>';
      			  echo '<td>'.$all_equipments[$l]['serial_enabled'].'</td>';
      			  echo '<td>'.$all_equipments[$l]['calibration_required'].'</td>';
              echo '</tr>';
              $l++;

            }

          ?>
          </table>
          </div>

  </div>

</body>
</html>
