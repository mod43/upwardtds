<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVITEMCREATE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.

$return_message = null;

if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }

    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Create Item</h1></center>

</div>
<div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_ITEM_CREATE_PROC.php' method='post'>

                    <div class="form-group">
                      <label for="item" class="control-label col-md-3">Item</label>
                        <div class="col-md-8">
                          <input type='text' name='item' id='item' value='' class='form-control' autocomplete='off'>
                        </div>
                        <div class="col-md-1">
                          <span id='itemvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="item_desc" class="control-label col-md-3">Item Description</label>
                        <div class="col-md-8">
                          <input type='text' name='item_desc' id='item_desc' value='' class='form-control' autocomplete='off'>
                        </div>
                        <div class="col-md-1">
                          <span id='descvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="uom_code" class="control-label col-md-3">UOM</label>
                        <div class="col-md-8">
                          <input type='text' name='uom_code' id='uom_code' value='' class='form-control'>
                        </div>
                        <div class="col-md-1">
                          <span id='uomvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Status</label>
                        <div class="col-md-8">
                          <select class="form-control" name="status" id="status">
                              <option value="Active" selected>Active</option>
                              <option value="Inactive">Inactive</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='statusvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Serial Enabled</label>
                        <div class="col-md-8">
                          <select class="form-control" name="serial_enabled" id="serial_enabled">
                              <option value="unselected">Select Option...</option>
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='serialvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Consumable</label>
                        <div class="col-md-8">
                          <select class="form-control" name="consumable" id="consumable">
                              <option value="unselected">Select Option...</option>
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='consumablevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Reorder Planned</label>
                        <div class="col-md-8">
                          <select class="form-control" name="reorder_planned" id="reorder_planned">
                              <option value="unselected">Select Option...</option>
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='reordervalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-5 col-md-4">
                          <input type="submit" value="Submit" id='item_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
</div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Item Created Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Item Creation ended in error.</p>';
    }



  ?>
</div>

</div>


<script type="text/javascript">

var itemIsValid = null;
var descIsValid = null;
var uomIsValid = null;
var serialChosen = null;
var consumableChosen = null;
var reorderChosen = null;

$(document).ready(function (){
      $('#return_message').delay(5000).fadeOut();
    validate_filled();
    $('#item').focus();
    $('#item, #item_desc, #uom_code, #status').change(validate_filled);

});

function validate_filled(){
    if (itemIsValid &&
        descIsValid &&
        uomIsValid &&
        serialChosen &&
        consumableChosen &&
        reorderChosen) {
        $("input[type=submit]").attr("disabled", false);
        $("#item_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase());
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data == 0){
        itemIsValid = true;
              $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              $('#item_desc').focus();
              validate_filled();
      } else if (data > 0){
        itemIsValid = false;
              $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Item exists already.  Please enter a unique item name.");
              $('#item').val('');
              $('#item').focus();
              validate_filled();
      } else {
        itemIsValid = false;
         $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new item name.");
              $('#item').val('');
              validate_filled();
      }
    });
  } else {
            itemIsValid = false;

              $('#item').val('');
              validate_filled();
  }
});

$('#item_desc').blur(function(){
  if ($('#item_desc').val().length > 0){
      descIsValid = true;
      $('#descvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
      $('#uom_code').focus();
  } else {
              descIsValid = false;
              $('#item_desc').val('');
              validate_filled();
  }
});

$(function() {
  $("#uom_code").autocomplete({
    source: "ajax/UP_INV_UOM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#uom_code").blur(function(){
  if (  $('#uom_code').val().length   >   0  ){
    $('#uom_code').val($(this).val().toUpperCase());
    var uom = $('#uom_code').val();
    $.post('ajax/UP_INV_UOM_VALIDATE.php', {uom: uom}, function(data){
      if (data > 0){
        uomIsValid = true;
              $('#uomvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#status').focus();
              validate_filled();
      } else {
        uomIsValid = false;
              $('#uomvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("UOM Code is not allowed.  Please enter a correct UOM code or select from list.");
              $('#uom_code').val('');
              $('#uom_code').focus();
              validate_filled();
      }
    });
  } else {
        uomIsValid = false;
        $('#uom_code').val('');
        validate_filled();
  }
});


$('#serial_enabled').change(function (){

  var serialChoice = $('#serial_enabled option:selected').attr('value');
  if (serialChoice != 'unselected'){
    serialChosen = true;
    validate_filled();
    } else {
      $('#message').html("Please make a choice from the drop down");
      serialChosen = false;
      validate_filled();
    }

});

$('#reorder_planned').change(function (){

  var reorderChoice = $('#reorder_planned option:selected').attr('value');
  if (reorderChoice != 'unselected'){
    reorderChosen = true;
    validate_filled();
    } else {
      $('#message').html("Please make a choice from the drop down");
      reorderChosen = false;
      validate_filled();
    }

});

$('#consumable').change(function (){

  var consumableChoice = $('#consumable option:selected').attr('value');
  if (consumableChoice != 'unselected'){
    consumableChosen = true;
    validate_filled();
    } else {
      $('#message').html("Please make a choice from the drop down");
      consumableChosen = false;
      validate_filled();
    }

});






</script>


</body>
</html>
