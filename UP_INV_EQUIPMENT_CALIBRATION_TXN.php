<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQPCALTXN';

$return_message = null;

require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Equipment Calibration Transaction</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_EQUIPMENT_CALIBRATE_TXN_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="equipment" class="control-label col-md-2">Equipment</label>
                        <div class="col-md-8">
                          <input type='text' name='equipment' id='equipment' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='equipmentvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="serial" class="control-label col-md-2">Serial</label>
                        <div class="col-md-8">
                          <input type='text' name='serial' id='serial' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='serialvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="equipment_desc" class="control-label col-md-2">Notes</label>
                        <div class="col-md-8">
                          <textarea name='notes' id='notes' value='' class='form-control' rows='4' autocomplete="off" maxlength='250'></textarea>
                        </div>
                     </div>
                  

                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="col-xs-1">
                        <input type="hidden" id="equipment_id" name="equipment_id" value=''>
                      </div>
                     <div class="col-xs-1">
                        <input type="hidden" id="serial_id" name="serial_id" value=''>
                      </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='calibration_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Calibration recorded successfully.</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Calibration was not successful.</p>';
    }



  ?>
</div>

</div>


<script>
var equipmentIsValid = null;
var serialIsValid = null;


$(document).ready(function(){

    validate_filled();
    $('#return_message').delay(5000).fadeOut();
});




$(function() {
  $("#equipment").autocomplete({
    source: "ajax/UP_INV_EQUIPMENT_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#equipment").blur(function(){
  if (  $('#equipment').val().length   >   0  ){
    $('#equipment').val($(this).val().toUpperCase()); //take the item to uppercase
    var equipment = $('#equipment').val();
    $.post('ajax/UP_INV_EQUIPMENT_VALIDATE.php', {equipment: equipment}, function(data){
      if (data > 0){
        equipmentIsValid = true;
        $('#equipmentvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
        $("#serial").focus();
        $.post('ajax/UP_INV_EQUIPMENT_GET_DESCRIPTION.php', {equipment: equipment}, function(data){
          $('#equipment_desc').attr('placeholder', data);
        });
        $.post('ajax/UP_INV_EQUIPMENT_GET_ID.php', {equipment: equipment}, function(data){
          $('#equipment_id').attr('value', data);
        });
        $('#message').html("");
      } else {
        itemIsValid = false;
        $('#equipmentvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#equipment').val('');
        $('#equipment').focus();
        $('#message').html("Invalid Equipment");
      }
    });
  } else {
    equipmentIsValid = false;
      $('#equipment').val('');
      validate_filled();
  }
});


$(function(){
    $("#serial").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_CALIBRATE_SERIAL_QUERY.php",
          data: {
            equipment : $("#equipment").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    }); 
});


$('#serial').blur(function(){
  if (  $('#serial').val().length   >   0  ){
    $('#serial').val($(this).val().toUpperCase());
    var equipment = $('#equipment').val();
    var serial = $('#serial').val();
    $.post('ajax/UP_INV_EQUIPMENT_CALIBRATE_SERIAL_VALIDATE.php', {equipment: equipment, serial: serial}, function(data){
      if (data > 0){
        serialIsValid = true;
              $('#serialvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              $('#frequency').focus();
              validate_filled();
              var equipment_id = $('#equipment_id').val();
              $.post('ajax/UP_INV_EQUIPMENT_SERIAL_GET_ID.php', {equipment_id: equipment_id, serial: serial}, function(data){
                $('#serial_id').attr('value', data);
              });
      } else {
        serialIsValid = false;
              $('#serialvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Invalid Serial Number. Either Serial number does not exist or is already in use.");
              $('#serial').val('');
              $('#serial').focus();
              validate_filled();
      }
    });
  } else {
            serialIsValid = false;

              $('#serial').val('');
              validate_filled();
  }
});








function validate_filled(){
    if (equipmentIsValid && serialIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};






</script>

    </body>
</html>
