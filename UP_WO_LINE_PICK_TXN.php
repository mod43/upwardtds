<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOLINEPICKTXN';

require_once 'app/init.php';
// Include app init file
    
// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function."); 
        
    }

include 'header.php'; //includes the navigation header

$wo_line_id = $_GET['wo_line_id'];

if (!$wo_line_id)
    {
      echo '<div class="col-sm-12">';
      echo 'Work Order Line Not Found!';
      echo '<br>';
      echo '<a href="UP_WO_PICKABLE_VIEW.php"><button type="button" class="btn btn-primary">Work Order Pick List</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
    } 

$wo_line_info = $database->table('up_wo_lines_view')->where('wo_line_id','=',$wo_line_id)->first();

$best_location = $database->table('up_wo_best_location')->where('item_id', '=', $wo_line_info->wo_line_item_id)->first();



if (!$wo_line_info->wo_line_status == 2){
   echo '<div class="col-sm-12">';
      echo 'Work Order Line is not pickable.';
      echo '<br>';
      echo '<a href="UP_WO_PICKABLE_VIEW.php"><button type="button" class="btn btn-primary">Work Order Pick List</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
} 

?>



<div class="col-sm-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">WO Line Pick Transaction</h1></center>
    </div>
    <div class="panel-body">

<form class="form-horizontal" action='ajax/UP_WO_LINE_PICK_CONFIRM_PROCESS.php' method='post'>
        <div class="form-group">
          <label for="order" class="control-label col-sm-2">Order</label>
            <div class="col-sm-8">
              <span name='orderdisplay' id='orderdisplay' class="form-control"><?php echo $wo_line_info->work_order_number; ?></span>
            </div>
         </div>  
        <div class="form-group">
          <label for="pickitem" class="control-label col-sm-2">Item</label>
            <div class="col-sm-8">
              <span name='pickitem' id='pickitem' class="form-control"><?php echo $wo_line_info->item_name; ?></span>
            </div>
         </div>  
        <div class="form-group">
          <label for="item" class="control-label col-sm-2">Confirm</label>
            <div class="col-sm-8">
              <input type='text' name='item' id='item' value='' class='form-control' tabindex='1'>
            </div>
            <div class="col-sm-1">
              <span id='itemvalid'></span>
            </div>
         </div>
        <div class="form-group">
          <label for="bestloc" class="control-label col-sm-2">Best Location</label>
            <div class="col-sm-8">
              <span name='bestloc' id='bestloc' class="form-control"><?php echo $best_location->location_name." - ".$best_location->onhand_qty." onhand"; ?></span>
          </div>
        </div>     
        <div class="form-group">
          <label for="group" class="control-label col-sm-2">Group</label>
            <div class="col-sm-8">
              <input type='text' name='group' id='group' value='' class='form-control' tabindex='2'>
            </div>
            <div class="col-sm-1">
              <span id='groupvalid'></span>
            </div>
         </div>   
        <div class="form-group">
          <label for="location" class="control-label col-sm-2">Location</label>
            <div class="col-sm-8">
              <input type='text' name='location' id='location' value='' class='form-control' tabindex='3'>
            </div>
            <div class="col-sm-1">
              <span id='locationvalid'></span>
            </div>
         </div>
         <div class="form-group">
          <label for="requested_quantity" class="control-label col-sm-2">Order Qty</label>
            <div class="col-sm-8">
              <span name='requested_quantity' id='requested_quantity' class="form-control"><?php echo $wo_line_info->wo_line_quantity; ?></span>
            </div>
         </div> 
         <div class="form-group">
          <label for="requested_quantity" class="control-label col-sm-2">Remaining Qty</label>
            <div class="col-sm-8">
              <span name='remaining_quantity' id='remaining_quantity' class="form-control"><?php echo ($wo_line_info->wo_line_released_quantity-$wo_line_info->wo_line_picked_quantity); ?></span>
            </div>
         </div> 
         <div class="form-group">
          <label for="quantity" class="control-label col-sm-2">Quantity</label>
            <div class="col-sm-8">
              <input type='text' name='quantity' id='quantity' value='' class='form-control' placeholder='Available Quantity: ' tabindex='4'>
           </div>
           <div class="col-sm-1">
              <span id='quantityvalid'></span>
            </div>
         </div> 
         <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <span id='message'></span>
            </div>
            <div class="col-xs-1">
                    <input type="hidden" name="wo_line_id" value=<?php echo '"'.$wo_line_id.'"'; ?> >
            </div>
            <div class="col-xs-1">
                    <input type="hidden" name="order" value=<?php echo '"'.$wo_line_info->work_order_number.'"'; ?> >
            </div>
         </div>   
         <div class="form-group">
            <div class="col-sm-offset-4 col-sm-4">
              <input type="submit" value="Submit" id='wo_pick_confirm_submit' class="btn btn-primary btn-block" tabindex='5'>
            </div>
          </div>
        </form>
      </div>
  </div>      
</div>

<script type="text/javascript">

var available_quantity = 0;
var itemIsValid = null;
var groupIsValid = null;
var locationIsValid = null;
var quantityIsValid = null;
var partialPick = null;


// item validation
$("#item").blur(function(){
  if ($('#item').val().length > 0){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    var pickitem = $('#pickitem').text();
      if (item == pickitem){
        itemIsValid = true;
              $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
      } else {
        itemIsValid = false;
              $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      }
  }
});

$(function() {
  $("#group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });       
});

$("#group").blur(function(){
  if (  $('#group').val().length   >   0  ){
    $('#group').val($(this).val().toUpperCase());
    var group = $('#group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        groupIsValid = true;
              $('#groupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
      } else {
        groupIsValid = false;
              $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      }
    });
  }
});

$(function(){
    $("#location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#group").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    }); 
});

$("#location").blur(function(){
  if (  $('#location').val().length   >   0  ){
        $('#location').val($(this).val().toUpperCase());
        var location = $('#location').val();
        var group = $("#group").val()
        $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
        if (data > 0){
          locationIsValid = true;
          $('#locationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
          $('#message').html('');
          if (  itemIsValid && groupIsValid && locationIsValid ){
                var loc = $('#location').val();
                var group = $('#group').val();
                var item = $('#item').val();
                $.post('ajax/UP_INV_QTYBYLOC_QUERY.php', {fromloc: loc, fromgroup: group, item: item}, function(data){ //Note to standardize variables
                  available_quantity=data;
                  $('#quantity').attr('placeholder', ("Available Quantity: " + data));
                  if (data == 0){
                    $('#locationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                    $('#message').html("0 onhand in location entered.")
                    $('#location').val('');
                    $('#location').focus();
                  }
                });
                }
      } else {
        locationIsValid = false;
              $('#locationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      }
    });
  }
});


$(document).ready(function (){
    validate_filled();
    document.title= 'Item Issue';
    $('#item, #group, #location, #quantity').change(validate_filled);
});

function validate_filled(){
    if (itemIsValid &&
      groupIsValid &&
      locationIsValid &&
      quantityIsValid) {
        $("#wo_pick_confirm_submit").attr("disabled", false);
        $("#wo_pick_confirm_submit").focus();
    }
    else {
        $("#wo_pick_confirm_submit").attr("disabled", true);
    }
};






$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_ent = parseInt($('#quantity').val()); //create integer
        var qty_avl = parseInt(available_quantity);  //create integer
        var remaining_quantity = $('#remaining_quantity').text(); //Get requested quantity
        var qty_req = parseInt(remaining_quantity); //create integer
          if (qty_ent > qty_avl){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered is greater than available quantity.");
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0. Please backorder from the Pick List page if not picakble.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
          } else if (qty_ent <= qty_avl) {
                if(qty_ent > qty_req) {
                  quantityIsValid = false;
                  $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Quantity entered is greater than requested quantity.")
                  $('#quantity').val('');
                  $('#quantity').focus();
                } else if (qty_ent == qty_req){
                  quantityIsValid = true;
                  $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
                  $('#message').html("");
                  validate_filled();
                } else if (qty_ent < qty_req){
                  quantityIsValid = true;
                  $('#quantityvalid').html("<button type='button' class='btn btn-warning'><span class='glyphicon glyphicon-warning-sign' aria-hidden='true'></span></button>");
                  $('#message').html("Quantity entered is less than requested quantity. Line will remain open for picking remainder.")
                  validate_filled();
                }
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});





</script>


</body>
</html>
