<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOLINEBOTXN';

require_once 'app/init.php';
// Include app init file
    
// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function."); 
        
    }

include 'header.php'; //includes the navigation header

$wo_line_id = $_GET['wo_line_id'];

if (!$wo_line_id)
    {
      echo '<div class="col-sm-12">';
      echo 'Work Order Line Not Found!';
      echo '<br>';
      echo '<a href="UP_WO_PICKABLE_VIEW.php"><button type="button" class="btn btn-primary">Work Order Pick List</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
    } 

$wo_line_info = $database->table('up_wo_lines_view')->where('wo_line_id','=',$wo_line_id)->first();





if (!$wo_line_info->wo_line_status == 2){
   echo '<div class="col-sm-12">';
      echo 'Work Order Line is not in a backorderable state.';
      echo '<br>';
      echo '<a href="UP_WO_PICKABLE_VIEW.php"><button type="button" class="btn btn-primary">Work Order Pick List</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
} 

?>



<div class="col-sm-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">WO Line Backorder Transaction</h1></center>
    </div>
    <div class="panel-body">

<form class="form-horizontal" action='ajax/UP_WO_LINE_BACKORDER_PROCESS.php' method='post'>
        <div class="form-group">
          <label for="order" class="control-label col-sm-2">Order</label>
            <div class="col-sm-8">
              <span name='orderdisplay' id='orderdisplay' class="form-control"><?php echo $wo_line_info->work_order_number; ?></span>
            </div>
         </div>  
        <div class="form-group">
          <label for="pickitem" class="control-label col-sm-2">Item</label>
            <div class="col-sm-8">
              <span name='pickitem' id='pickitem' class="form-control"><?php echo $wo_line_info->item_name; ?></span>
            </div>
         </div>  
        
         <div class="form-group">
          <label for="requested_quantity" class="control-label col-sm-2">Order Qty</label>
            <div class="col-sm-8">
              <span name='requested_quantity' id='requested_quantity' class="form-control"><?php echo $wo_line_info->wo_line_quantity; ?></span>
            </div>
         </div> 
         <div class="form-group">
          <label for="requested_quantity" class="control-label col-sm-2">Remaining Qty</label>
            <div class="col-sm-8">
              <span name='remaining_quantity' id='remaining_quantity' class="form-control"><?php echo ($wo_line_info->wo_line_released_quantity-$wo_line_info->wo_line_picked_quantity); ?></span>
            </div>
         </div> 
         <div class="form-group">
            <div class="col-sm-offset-3 col-sm-8">
              <span id='message'></span>
            </div>
            <div class="col-xs-1">
                    <input type="hidden" name="wo_line_id" value=<?php echo '"'.$wo_line_id.'"'; ?> >
            </div>
            <div class="col-xs-1">
                    <input type="hidden" name="order" value=<?php echo '"'.$wo_line_info->work_order_number.'"'; ?> >
            </div>
         </div>   
         <div class="form-group">
            <div class="col-sm-offset-4 col-sm-4">
              <input type="submit" value="Backorder Line" id='wo_bo_confirm_submit' class="btn btn-primary btn-block" tabindex='5'>
            </div>
          </div>
        </form>
      </div>
  </div>      
</div>

<script type="text/javascript">


</script>


</body>
</html>
