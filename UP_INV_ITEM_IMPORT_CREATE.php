<?php
    require("config.php");
    if(empty($_SESSION['user'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

include 'header.php';
?>

<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Item Load</h1></center>

</div>
	<div class="panel-body">

		<form class="form-horizontal" enctype="multipart/form-data" action='test.php' method='post'>
   
      
        	<div class="controls">
              <input type="hidden" name="MAX_FILE_SIZE" value="9999999" />
              <input class="file" name="file" type="file" id="file" onchange="showCode()" onblur="showCode()" onclick="showCode" required="required"  />
              </div>
     		<div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                          <input type="submit" value="Submit" id='filename' class="btn btn-primary btn-block" tabindex='7'>
                        </div>
            </div>
 		</form>
	 </div>