
<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOHEADERCREATE';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php"); 
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function."); 
        header("Location: index.php");
    }


include 'header.php'; //includes the navigation header
?>

<!-- Set up the Form -->
<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Work Order Creation - Create Work Order</h1></center>

</div>
<div class="panel-body">



              <form class="form-horizontal" action='ajax/UP_WO_CREATE_HEADER_PROCESS.php' method='post'>
                
                    <div class="form-group">
                      <label for="name" class="control-label col-md-3">Deliver Name</label>
                        <div class="col-md-7">
                          <input type='text' name='name' id='name' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-2">
                          <span id='namevalid'></span>
                        </div>
                     </div>   
                    <div class="form-group">
                      <label for="location" class="control-label col-md-3">Deliver Location</label>
                        <div class="col-md-7">
                          <input type='text' name='location' id='location' value='' class='form-control' tabindex='2'>
                        </div>
                        <div class="col-md-2">
                          <span id='fromgroupvalid'></span>
                        </div>
                     </div>   
                    <div class="form-group">
                      <label for="scheduled_date" class="control-label col-md-3">Scheduled Date</label>
                        <div class="col-md-7">
                          <input type='text' name='scheduled_date' id='scheduled_date' value='' class='form-control' tabindex='3'>
                        </div>
                        <div class="col-md-2">
                          <span id='scheduleddatevalid'></span>
                        </div>
                     </div>   
                     <div class="form-group">
                      <label for="priority" class="control-label col-md-3">Priority</label>
                        <div class="col-md-7">
                                 <select name="priority" id="priority">
                                    <option value="1">High</option>
                                    <option value="2" selected>Normal</option>
                                    <option value="3">Low/Restock</option>
                                </select>
                        </div>
                        <div class="col-md-2">
                          <span id='priorityvalid'></span>
                        </div>
                     </div>  
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                          <input type="submit" value="Submit" id='wo_create_submit' class="btn btn-primary btn-block" tabindex='7'>
                        </div>
                      </div>
              </form>
          

            </div>
          </div>
        </div>


<script type="text/javascript">

$('#scheduled_date').datepicker({
  changeMonth: true,
  changeYear: true,
  dateFormat: "yy-mm-dd"
});


</script>


</body>
</html>