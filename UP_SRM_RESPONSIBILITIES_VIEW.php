<?php
    require_once 'app/init.php'; 
    // Check if logged in
    if (isset($_SESSION['user_id']))
    {
        
        if(!empty($_POST))
        {
            // following menu selection, set session variables and re-direct to menu page
            $_SESSION['responsibility'] = $_POST['responsibility'];
            $session_resp_name = $auth->getResponsibilityDetails($_SESSION['responsibility']);
            $_SESSION['responsibility_name'] = $session_resp_name->responsibility_name;
            $session_site_id = $auth->getResponsibilitySite($_SESSION['responsibility']);
            $_SESSION['site_id'] = $session_site_id[0]['site_id'];
            $session_site_name = $auth->getSiteName($_SESSION['site_id']);
            $_SESSION['site_name'] = $session_site_name[0]['site_name'];


            header('Location: UP_SRM_MENU_VIEW.php');
        }
            // Get user responsibility assignments
            $responsibilities = $auth->getUserResponsibilities($_SESSION['user_id']);
            $count = count($responsibilities);
            $i = 0;
    } else
     {
        
        //die if not logged in
        //header("Location: index.php");
        //die("Redirecting to index.php"); 
    }
include 'header.php'; //include base markup
?>
<div class="col-sm-8">
<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">
                <center><h1 class="panel-title">Choose Responsibility</h1></center></div>
<div class="panel-body">
<?php  

            
            //echo$_SESSION;
            var_dump($_SESSION);
                echo "<form action='UP_SRM_RESPONSIBILITIES_VIEW.php' method='post'>";  // build form for responsibility selection
                while($i < $count)
                {
                    echo "<button type='submit' class='btn btn-default btn-lg btn-block' name='responsibility' value='";
                    echo $responsibilities[$i]['responsibility_id'];
                    echo "'>"; // Add value for responsibility ID for submit
                    $responsibility_name = $auth->getResponsibilityDetails($responsibilities[$i]['responsibility_id']);
                    //echo $responsibility_name;
                    //var_dump($responsibility_name);
                    echo $responsibility_name[0]['responsibility_name'];
                    echo "</button>";
                    echo "<br>";
                    $i++;
                }       
?>
</div>
</body>
</html>
