<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SERIALSEARCHVIEW';

$return_message = null;
require_once 'app/init.php';
// Include app init file

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php"); 
        header("Location: index.php");
    }

        if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function."); 
        header("Location: index.php");
    }

include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Serial Search</h1></center>

  </div>
<div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->
              <form class="form-horizontal" id="UP_INV_SERIAL_VIEW" action="UP_INV_SERIAL_VIEW.php" method='post'>

                    <div class="form-group">
                      <label for="item" class="control-label col-sm-2">Item</label>
                        <div class="col-sm-8">
                          <input type='text' name='item' id='item' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-1">
                          <span id='itemvalid'></span>
                        </div>
                     </div>
                      <div class="form-group">
                        <div class="col-md-offset-3 col-md-8">
                          <span id='message'></span>
                        </div>
                      </div>
                     <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                          <input type="submit" value="Search" id='inv_serial_search_submit' class="btn btn-primary btn-block" tabindex='2'>
                        </div>
                     </div>
              </form>

</div>
</div>



<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Serial Query Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Serial Query ended in error.</p>';
    }



  ?>
</div>
          <!--</div>
      </div>
</div>-->

<script type="text/javascript">

var itemIsValid = null;


// item autocomplete
$(function() {
  $("#item").autocomplete({
    source: "ajax/UP_INV_SERIAL_ITEM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});


// item validation
$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data > 0){
        itemIsValid = true;
        $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
                $('#message').html("");
      } else {
        itemIsValid = false;
        $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#item').val('');
        $('#item').focus();
        $('#message').html("Invalid Item");
      }
    });
  } else {
    itemIsValid = false;
      $('#item').val('');
      validate_filled();
  }
});




$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    validate_filled();
    document.title= 'Serial Search';
    $('#item').focus();
    $('#item').change(validate_filled);
});

function validate_filled(){
    if (itemIsValid = true) {
        $("input[type=submit]").attr("disabled", false);
        $("input[type=submit]").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};




</script>


</body>
</html>
