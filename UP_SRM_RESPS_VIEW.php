<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMRESPSVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Upward Responsibility Listing</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Responsibility Name</th>
            <th>Site Assigment</th>
            <th>Delete</th>

         </tr>
          <?php

            $resps = $database->query('select sr.responsibility_name, sr.responsibility_shortcode, ins.site_name 
                                       from mod43fordpoc.dbo.srm_responsibility sr, mod43fordpoc.dbo.inv_sites ins, mod43fordpoc.dbo.srm_resp_site_assignments srsa
                                       where sr.responsibility_id = srsa.responsibility_id
                                       AND ins.site_id = srsa.site_id')->get();

            $count = count($resps);
            $l = 0;

            while ($l < $count)
            {
              echo '<tr>';
              echo '<td>'.$resps[$l]['responsibility_name'].'</td>';
              echo '<td>'.$resps[$l]['site_name'].'</td>';
              //echo '<td><a href="ajax/UP_SRM_RESPONSIBILITY_DELETE_PROC.php?shortcode='.$resps[$l]->responsibility_shortcode.'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>';
              echo '<td><a href="#" data-href="ajax/UP_SRM_RESPONSIBILITY_DELETE_PROC.php?shortcode='.$resps[$l]['responsibility_shortcode'].'" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>';
              echo '</tr>';
              $l++;
            }
            
          ?>
          </table>
          </div>

  </div>

  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>You are about to delete this responsibility for all users and sites, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

 <script>
   $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });


 </script> 

</body>
</html>