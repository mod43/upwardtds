<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOLINESCREATE';

require_once 'app/init.php';
// Include app init file
    
// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function."); 
        
    }

include 'header.php'; //includes the navigation header

$wo_header_number = $_GET['wo_header_number'];

if (!$wo_header_number)
    {
      echo '<div class="col-md-12">';
      echo 'Work Order Number Not Found!';
      echo '<br>';
      echo '<a href="UP_WO_HEADER_CREATE.php"><button type="button" class="btn btn-primary">Create Work Order</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
    } 

$wo_header_info = $database->table('wo_work_order_header')->where('work_order_number','=',$wo_header_number)->first();

$wo_header_id = $wo_header_info->work_order_header_id;

$order_lines = $database->table('up_wo_lines_view')->where('work_order_header_id','=', $wo_header_id)->get();

if ($wo_header_info->status == 1){
  $status = 'Open';
} elseif ($wo_header_info->status == 2) {
  $status = 'Pick Released';
} elseif ($wo_header_info->status == 3) {
  $status = 'Picked Full';
} elseif ($wo_header_info->status == 4) {
  $status = 'Cancelled';
} else {
  $status = 'Undefined';
}

if ($wo_header_info->priority == 1){
  $priority = 'High Priority';
} elseif ($wo_header_info->priority == 2) {
  $priority = 'Normal Priority';
} elseif ($wo_header_info->priority== 3) {
  $priority = 'Restock/Low Priority';
} else {
  $priority = 'Undefined';
}


$count = count($order_lines);
$ln = 0;

?>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Work Order Info - <?php echo $wo_header_number; ?></h1></center>
    </div>
    <div class="panel-body">
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Deliver To: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $wo_header_info->deliver_to_name; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Deliver Location: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $wo_header_info->deliver_to_location; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Schedule Date: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $wo_header_info->scheduled_date; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Priority: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $priority; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Order Status: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $status; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-offset-3 col-sm-4"><?php if($wo_header_info->status == 1){echo '<button class="btn btn-success" id="pick_release"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Confirm for picking</button>';} else {echo '';} ?></div>
        </div>
        <div class="row">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
          <div class="panel-heading">
            <center><h1 class="panel-title">Add Lines</h1></center>
          </div>
          <div class="panel-body">
            <form class="form-horizontal" action='ajax/UP_WO_ADD_LINE_PROC.php' method='post'>
              <div class="form-group">
                <div class="row">             
                  <div class="col-xs-offset-1 col-xs-2">
                    <label for="item" class="control-label">Item</label>
                  </div>
                  <div class="col-xs-7">
                    <input type='text' name='item' id='item' value='' class='form-control' tabindex='1'>
                  </div>
                  <div class="col-xs-2">
                    <span id='itemvalid'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-offset-1 col-xs-2">
                    <label for="quantity" class="control-label">Quantity</label>
                  </div>
                  <div class="col-xs-7">
                    <input type='text' name='quantity' id='quantity' value='' class='form-control' placeholder='Available Quantity: ' tabindex='2'>
                  </div>
                  <div class="col-xs-2">
                    <span id='quantityvalid'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-1">
                    <input type="hidden" name="wo_header_number" value=<?php echo '"'.$wo_header_number.'"'; ?> >
                  </div>
                  <div class="col-xs-offset-2 col-xs-10">
                    <span id='message'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-offset-4 col-xs-4">
                    <input type="submit" value="Add" id='item_submit' class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 1px 1px 1px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Order Lines</h1></center>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Item</th>
                <th>UOM</th>
                <th>Quantity</th>
                <th>Line Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php

                while($ln < $count){

                  echo '<tr>';
                  echo '<td>'.$order_lines[$ln]->item_name.'</td>';
                  echo '<td>'.$order_lines[$ln]->uom_name.'</td>';
                  echo '<td>'.$order_lines[$ln]->wo_line_quantity.'</td>';
                  if ($order_lines[$ln]->wo_line_status == 1){
                    $line_status = 'Open';
                  } elseif ($order_lines[$ln]->wo_line_status == 2) {
                    $line_status = 'Pick Released';
                  } elseif ($order_lines[$ln]->wo_line_status == 3) {
                    $line_status = 'Picked';
                  } elseif ($order_lines[$ln]->wo_line_status == 4) {
                    $line_status = 'Backordered';
                  } elseif ($order_lines[$ln]->wo_line_status == 5) {
                    $line_status = 'Cancelled';
                  } else {
                    $line_status = 'Unknown Status';
                  }
                  echo '<td>'.$line_status.'</td>';
                  echo '<td><a href="UP_WO_LINES_EDIT?wo_line_id='.$order_lines[$ln]->wo_line_id.'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
                  echo '</tr>';
                  $ln++;
                }
              ?>
            </tbody>
          </table>
      </div>
    </div>
  </div>        
</div>



<script type="text/javascript">

var itemIsValid = null;
var quantityIsValid = null;
var available_quantity = null;
var wo_order_number = <?php echo '"'.$wo_header_number.'"'; ?>;
var wo_order_number_int = parseInt(wo_order_number);

$("#pick_release").click(function(){
        $.ajax({
          url: 'ajax/UP_WO_RELEASE_ORDER_PROC.php',
          type: 'post',
          data: { wo_header_number: wo_order_number_int}
        }).done(function(){
          location.reload(true);
        });
    });    

$(document).ready(function (){
    validate_filled();
    $('#item, #quantity').blur(validate_filled);
});

function validate_filled(){
    if (itemIsValid &&
        quantityIsValid) {
        $('#item_submit').attr("disabled", false);
        $('#item_submit').focus();
    }
    else {
        $('#item_submit').attr("disabled", true);
    }
};

// item autocomplete
$(function() {
  $("#item").autocomplete({
    source: "ajax/UP_INV_ITEM_QUERY.php",
    autoFocus: true,
    minLength: 1
  });       
});

// item validation
$("#item").blur(function(){
  if (  $('#item').val().length   >   0  ){
    $('#item').val($(this).val().toUpperCase()); //take the item to uppercase
    var item = $('#item').val();
    $.post('ajax/UP_INV_ITEM_VALIDATE.php', {item: item}, function(data){
      if (data > 0){
        itemIsValid = true;
              $('#itemvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              if ($('#item').val().length  >   0){
                  var item = $('#item').val();
                  $.post('ajax/UP_INV_QTYBYSITE_QUERY.php', {item: item}, function(data){
                  $('#quantity').attr('placeholder', ("Available Quantity: " + data));
                  available_quantity = data;
                  });
              } 
              $('quantity').focus();
      } else {
        itemIsValid = false;
        $('#itemvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        $('#item').val('');
        $('#item').focus();
      }
    });
  }
});

$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_ent = parseInt($('#quantity').val()); //create integer
        var qty_avl = parseInt(available_quantity);  //create integer
          if (qty_ent > qty_avl){
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-warning'><span class='glyphicon glyphicon-warning-sign' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered is greater than available quantity. This may result in a backorder.");
            validate_filled();
          } else if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
            $('#quantity').focus();
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
          } else if (qty_ent <= qty_avl) {
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});





</script>


</body>
</html>