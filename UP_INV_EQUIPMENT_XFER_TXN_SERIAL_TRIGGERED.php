<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQPXFERSERIALTRIGGERED';
$return_message = null;

require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php"); 
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function."); 
        header("Location: index.php");
    }

if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Equipment Transfer - Serial Triggered</h1></center>

  </div>
<div class="panel-body">
 


<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->
              <form class="form-horizontal" id="UP_INV_EQUIPMENT_XFER" action="#" method='post'>
                
                    <div class="form-group">
                      <label for="serial" class="control-label col-md-3">Serial</label>
                        <div class="col-md-7">
                          <input type='text' name='serial' id='serial' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-2">
                          <span id='serialvalid'></span>
                        </div>
                     </div>
                     <div id="equipmentfieldarea">
                     </div>
                     <div id="equipmentlistarea">
                     </div>
                     <div id="currentlocationarea">
                     </div>
                     <div class="form-group">
                      <label for="to_group" class="control-label col-md-3">To Group</label>
                        <div class="col-md-7">
                          <input type='text' name='to_group' id='to_group' value='' class='form-control' tabindex='4'>
                        </div>
                        <div class="col-md-2">
                          <span id='togroupvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="to_location" class="control-label col-md-3">To Location</label>
                        <div class="col-md-7">
                          <input type='text' name='to_location' id='to_location' value='' class='form-control' tabindex='5'>
                         </div>
                         <div class="col-md-2">
                          <span id='tolocationvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-7">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="col-xs-1">
                      <input type="hidden" name="quantity" id="quantity" value="1">
                     </div>
                     <div class="col-xs-1">
                      <input type="hidden" name="equipment" id="equipment" value="">
                     </div>
                     <div class="col-xs-1">
                      <input type="hidden" name="from_group" id="from_group" value="">
                     </div>
                     <div class="col-xs-1">
                      <input type="hidden" name="from_location" id="from_location" value="">
                     </div>
   
                     <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                          <input type="submit" value="Submit" id='inv_xfer_submit' class="btn btn-primary btn-block" tabindex='25'>
                        </div>
                     </div>
              </form>

</div>
</div>
          <!--</div>
      </div>
</div>-->

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Equipment Transfer Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Equipment Transfer ended in error.</p>';
    }



  ?>
</div>

<script type="text/javascript">

var equipmentIsValid = null;
var toGroupIsValid = null;
var toLocationIsValid = null;
var fromGroupIsValid = null;
var fromLocationIsValid = null;
var equipmentIsSerialEnabled = true;
var serialIsValid = null;
var available_quantity = null;
var quantityEntered = null;


$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    validate_filled();
    document.title= 'Equipment Transfer';
    $('#equipment, #group, #location, #quantity').change(validate_filled);
});


$('#inv_xfer_submit').click( function(){

  if (equipmentIsSerialEnabled == true){
    $('#UP_INV_EQUIPMENT_XFER').attr('action', 'ajax/UP_INV_EQUIPMENT_XFER_SERIAL_TRIG_PROC.php');
  } else if (equipmentIsSerialEnabled == false){
    $('#UP_INV_EQUIPMENT_XFER').attr('action', 'ajax/UP_INV_EQUIPMENT_XFER_PROCESS.php');
  } 
});


//serial autocomplete



//serial validation
// on blur, query for quantity of this serial
// if 0 = blank the serial field and focus
// if 1 = create a form field in equipmentarea, display the equipment, make the field non editable
//  ---   Then AJAX the from_group and from_location and display in a concatenated field in the currentlocationarea, focus on to_group
// if 2 or more = create a selecter box with of all the possible equipments for this serial
// upon selection, AJAX the from_group and from_location and display in a concatenated field in the currentlocationarea, focus on to_group

$(function() {
  $("#serial").autocomplete({
    source: "ajax/UP_INV_EQUIPMENT_SERIAL_TRIGGER_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});



$("#serial").blur(function(){
  var serial_entered = $("#serial").val();
  if (  $('#serial').val().length   >   0 &&  $('#serial').val() != '%' ){
    $.post('ajax/UP_INV_EQUIPMENT_SERIAL_TRIGGER_INFO_QUERY.php', {serial: serial_entered}, function(data){
     
     var serial_qty = data.length;

     if (serial_qty == 1){
      $('#equipmentfieldarea').empty();
      $('#currentlocationarea').empty();
      $('#equipmentlistarea').empty();
      $('#equipmentfieldarea').addClass('form-group');
      $('#currentlocationarea').addClass('form-group');
      $('#equipmentfieldarea').append('<label for="equipmentfield" class="control-label col-md-3">Equipment</label>');
      $('#equipmentfieldarea').append('<div class="col-md-7"><input type="text" name="equipmentfield" id="equipmentfield" value="'+ data[0].equipment +'" class="form-control" tabindex="1"></div>');
      $('#equipmentfieldarea').append('<div class="col-md-2"><span id="equipmentvalid"></span></div>');
      $('#equipment').val(data[0].equipment);
      $('#currentlocationarea').append('<label for="equipment" class="control-label col-md-3">From</label>');
      $('#currentlocationarea').append('<div class="col-md-7"><input type="text" name="current_location" id="current_location" value="'+ data[0].from_group + ' - ' + data[0].from_location +'" class="form-control" tabindex="1"></div>');
      $('#currentlocationarea').append('<div class="col-md-2"><span id="currentlocationvalid"></span></div>');
      $('#from_group').val(data[0].from_group);
      $('#from_location').val(data[0].from_location);
      equipmentIsValid = true;
      fromGroupIsValid = true;
      fromLocationIsValid = true;
      serialIsValid = true;


     } else if(serial_qty > 1) {

          $('#equipmentfieldarea').empty();
          $('#currentlocationarea').empty();
          $('#equipmentlistarea').empty();
          $('#equipmentlistarea').addClass('form-group');
          $('#equipmentlistarea').append('<label for="equipment" class="control-label col-md-3">Equipment</label></div>');
          var select_html = '<div class="col-md-7"><select class="form-control" name="equipmentselect" id="equipmentselect">';
          select_html+='<option value="unselected">Select Equipment...</option>'

           var i = 0;
           while (i < serial_qty){

            select_html+='<option value="'+data[i].equipment+'" data-group="'+data[i].from_group+'" data-location="'+data[i].from_location+'">'+data[i].equipment+'</option>';
            i++

          }

          select_html+='</select></div>';
          $('#equipmentlistarea').append(select_html);

     } else {
      
          $('#message').html("Error with serial number.");
          serialIsValid = false;
     }
     
    }, "json");
  }
});


$(document).on('change', '#equipmentselect', function(){
  var current_equipment = $('#equipmentselect option:selected').val();
  var current_group = $('#equipmentselect option:selected').data('group');
  var current_location = $('#equipmentselect option:selected').data('location');

  $('#currentlocationarea').empty();
  $('#currentlocationarea').addClass('form-group');
  $('#currentlocationarea').append('<label for="equipment" class="control-label col-md-3">From</label>');
  $('#currentlocationarea').append('<div class="col-md-7"><input type="text" name="current_location" id="current_location" value="'+ current_group + ' - ' + current_location +'" class="form-control" tabindex="1"></div>');
  $('#currentlocationarea').append('<div class="col-md-2"><span id="currentlocationvalid"></span></div>');
  $('#equipment').val(current_equipment);
  $('#from_group').val(current_group);
  $('#from_location').val(current_location);
  $('#to_group').focus();
  equipmentIsValid = true;
  fromGroupIsValid = true;
  fromLocationIsValid = true;
  serialIsValid = true;

});



// to_group


$(function() {
  $("#to_group").autocomplete({
    source: "ajax/UP_INV_GROUP_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});

$("#to_group").blur(function(){
  if (  $('#to_group').val().length   >   0 &&  $('#to_group').val() != '%' ){
    $('#to_group').val($(this).val().toUpperCase());
    var group = $('#to_group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        toGroupIsValid = true;
              $('#togroupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
      } else {
        toGroupIsValid = false;
              $('#togroupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#to_group').val('');
              $('#message').html("Invalid To Group");
              validate_filled();
      }
    });
  } else {
    toGroupIsValid = false;
      $('#to_group').val('');
      validate_filled();
  }
});

// to_location

$(function(){
    $("#to_location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#to_group").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#to_location").blur(function(){
  if (  $('#to_location').val().length   >   0 &&  $('#to_location').val() != '%' ){
    $('#to_location').val($(this).val().toUpperCase());
    var location = $('#to_location').val();
    var group = $("#to_group").val()
    $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
      if (data > 0){
        toLocationIsValid = true;
              $('#tolocationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
      } else {
        toLocationIsValid = false;
              $('#tolocationvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#to_location').val('');
              $('#message').html("Invalid To Location");
              validate_filled();
      }
    });
  } else {
          toLocationIsValid = false;
          $('#to_location').val('');
          validate_filled();
  }
});









function validate_filled(){
    if (equipmentIsSerialEnabled == true){
      if (equipmentIsValid &&
        toGroupIsValid &&
        toLocationIsValid &&
        fromGroupIsValid &&
        fromLocationIsValid &&
        serialIsValid) {
          $("input[type=submit]").attr("disabled", false);
          $("input[type=submit]").focus();
      }
      else {
          $("input[type=submit]").attr("disabled", true);
      }
    } else if (equipmentIsSerialEnabled == false){
      if (equipmentIsValid &&
        toGroupIsValid &&
        toLocationIsValid &&
        fromGroupIsValid &&
        fromLocationIsValid) {
          $("input[type=submit]").attr("disabled", false);
          $("input[type=submit]").focus();
      }
      else {
          $("input[type=submit]").attr("disabled", true);
      }
    }
};






</script>


</body>
</html>