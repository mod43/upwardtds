<?php

class Database
{
	protected $host = '192.168.110.1';
	protected $dbname = 'mod43fordpoc';
	protected $username = 'sa';
	protected $password = 'password123';
	protected $pdo;
	public $debug = false;
	protected $table;
	protected $stmt;

	public function __construct()
	{
		try
		{
			$this->pdo = new PDO('odbc:mod43fordpoc', $this->username, $this->password);//PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->username, $this->password);
				if($this->debug)
				{
					$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
		}
		catch(PDOException $e)
		{
			die($this->debug ? $e->getMessage() : '');
		}
	}

	public function query($sql)
	{
		$this->stmt = $this->pdo->prepare($sql);
		$this->stmt->execute();
		return $this;
	}

	public function table($table)
	{
		$this->table = $table;
		return $this;
	}

	public function insert($data)
	{

		$keys = array_keys($data);
		$fields = implode(", ", $keys);
		$placeholders = ':'.implode(', :', $keys);
		$sql = "INSERT INTO {$this->table} ({$fields}) VALUES ({$placeholders})";

		$this->stmt = $this->pdo->prepare($sql);

		return $this->stmt->execute($data);
	}

	public function where($field, $operator, $value)
	{

		$this->stmt = $this->pdo->prepare("SELECT * from {$this->table} WHERE {$field} {$operator} :value");
		$this->stmt->bindParam(':value', $value);
        $this->stmt->execute();
		return $this;

	}

	public function count()
	{
		return $this->stmt->rowCount();
	}


	public function exists($data)
	{
		$field = array_keys($data);

		return $this->where($field, '=', $data[$field])->count ? true : false;

	}

	public function get()
	{
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function first()
	{
		return $this->get();
	}

	public function all()
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM {$this->table}");
		$this->stmt->execute();
		return $this;
	}

	public function deleteByID($id_column, $id)
	{
		$this->stmt = $this->pdo->prepare("DELETE FROM {$this->table} where {$id_column} = :id");
		$this->stmt->execute(array('id' => $id));
		return true;
	}

	public function updateByID($update_column, $update_data, $id_column, $id)
	{
		$this->stmt = $this->pdo->prepare("update {$this->table} set {$update_column} = :update_data where {$id_column} = :id");
		$this->stmt->bindParam(':update_data', $update_data);
		$this->stmt->bindParam(':id', $id);
		$this->stmt->execute();
		return true;
	}

	public function getUserRespDetails($responsibility_id, $user_id)
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM mod43fordpoc.dbo.srm_user_resp_assignments WHERE user_id = :user_id AND responsibility_id = :responsibility_id");
		$this->stmt->bindParam(':user_id', $user_id);
		$this->stmt->bindParam(':responsibility_id', $responsibility_id);
		$this->stmt->execute();
		return $this;
	}

	public function checkSubmenuOnMain($menu_id)
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM mod43fordpoc.dbo.srm_menu_lines WHERE menu_line_submenu_id = :menu_id");
		$this->stmt->bindParam(':menu_id', $menu_id);
		$this->stmt->execute();
		return $this;
	}

	public function checkMainOnResp($menu_id)
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM mod43fordpoc.dbo.srm_responsibility WHERE responsibility_root_menu_id = :menu_id");
		$this->stmt->bindParam(':menu_id', $menu_id);
		$this->stmt->execute();
		return $this;
	}



}


?>
