<?php

class Auth
{

	protected $database;
	protected $hash;
	protected $session_user_id = 'user_id';
	protected $session_username = 'username';
	protected $session_responsibility = 'responsibility';
	protected $table = 'mod43fordpoc.dbo.srm_users';


	public function __construct(Database $database, Hash $hash)
	{
		$this->database = $database;

		$this->hash = $hash;

	}



	public function createUser($data)
	{

		if(isset($data['password']))
		{
			$data['password'] = $this->hash->make($data['password']);
		}

		return $this->database->table('mod43fordpoc.dbo.srm_users')->insert($data);

	}

	public function signin($data)
	{
		$user = $this->database->table($this->table)->where('user_name', '=', $data['username']);

		if($user->count())
		{
			$user = $user->first();
			$_SESSION['test']=$user;
			$_SESSION['user_test']=$user[0]['password'];
			if($this->hash->verify($data['password1'], $user[0]['password']))
			  {

			  	
			  	 $this->setAuthSessionUserId($user[0]['user_id']);
			  	 $this->setAuthSessionUsername($user[0]['user_name']);

			  	 return true;
			  }

			  return false;

		}

		return false;
		//return true;
	}

	public function resetPassword($data)
	{
			if(isset($data['password']))
			{
				$data['password'] = $this->hash->make($data['password']);
				//print_r($data);
			}

			return $this->database->table($this->table)->updateByID('password', $data['password'], 'user_id', $data['user_id']);

	}

	protected function setAuthSessionUserId($id)
	{
		$_SESSION['user_id'] = $id; //$this->session_user_id
	}

	protected function setAuthSessionUsername($name)
	{
		$_SESSION['username'] = $name; //$this->session_username
	}

	public function check()
	{
		return isset($_SESSION[$this->session_user_id]);
	}

	public function signout()
	{
		if(isset($_SESSION[$this->session_user_id])){
			unset($_SESSION[$this->session_user_id]);
		}

		if(isset($_SESSION[$this->session_username])){
			unset($_SESSION[$this->session_username]);
		}

		if(isset($_SESSION[$this->session_responsibility])){
			unset($_SESSION[$this->session_responsibility]);
		}

		session_destroy();

	}

	public function getUserResponsibilities($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_user_resp_assignments')->where('user_id', '=', $id)->get();
	}

	public function getResponsibilityDetails($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_responsibility')->where('responsibility_id', '=', $id)->get();
	}

	public function getResponsibilitySite($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_resp_site_assignments')->where('responsibility_id', '=', $id)->get();
	}

	public function getSiteName($id)
	{
		return $this->database->table('mod43fordpoc.dbo.inv_sites')->where('site_id', '=', $id)->first();
	}

	public function getMenuDetails($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_menu_header')->where('menu_header_id', '=', $id)->first();
	}

	public function getMenuLines($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_menu_lines')->where('menu_header_id', '=', $id)->get();
	}

	public function getMenuLinesView($id)
	{
		return $this->database->table('mod43fordpoc.dbo.up_srm_menu_lines_view')->where('srm_menu_header_id', '=', $id)->get();
	}

	public function getFunctionDetails($id)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_functions')->where('function_id', '=', $id)->first();
	}

	public function getFunctionID($shortcode)
	{
		return $this->database->table('mod43fordpoc.dbo.srm_functions')->where('function_shortcode', '=', $shortcode)->first(); //->function_id
	}

	public function checkFunctionAccess($shortcode)
	{
		$function_id_get = $this->getFunctionID($shortcode);
		$function_id = $function_id_get[0]['function_id'];

		if(in_array($function_id, $_SESSION['available_functions'])){
			return true;
		} else {
			return false;
		}

	}

}


?>
