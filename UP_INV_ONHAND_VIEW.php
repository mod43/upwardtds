<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVONHANDVIEW';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
$site_id = $_SESSION['site_id'];

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Item On-hand Balances</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Item</th>
            <th>Description</th>
            <th>Group</th>
            <th>Location</th>
            <th>UOM</th>
            <th>Quantity</th>
         </tr>
          <?php

            $all_onhand = $database->table('mod43fordpoc.dbo.up_inv_onhand_balances_view')->where('site_id', '=', $site_id)->get();
            
            $count = count($all_onhand);
            $l = 0;

            while ($l < $count)
            {
              if ($all_onhand[$l]['onhand_qty'] > 0){
              echo '<tr>';
              echo '<td>'.$all_onhand[$l]['item_name'].'</td>';
              echo '<td>'.$all_onhand[$l]['item_description'].'</td>';
              echo '<td>'.$all_onhand[$l]['group_name'].'</td>';
      			  echo '<td>'.$all_onhand[$l]['location_name'].'</td>';
      			  echo '<td>'.$all_onhand[$l]['uom_name'].'</td>';
      			  echo '<td>'.$all_onhand[$l]['onhand_qty'].'</td>';
              echo '</tr>';
              $l++;
              }
              else{$l++;
              }
            }

          ?>
          </table>
          </div>

  </div>

</body>
</html>
