<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMRESPCREATE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Responsibility</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_RESPONSIBILITY_CREATE_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="resp" class="control-label col-md-2">Responsibility Name</label>
                        <div class="col-md-8">
                          <input type='text' name='resp' id='resp' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='respnamevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Responsibility Shortcode</label>
                        <div class="col-md-8">
                          <input type='text' name='shortcode' id='shortcode' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='shortcodevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="root_menu" class="control-label col-md-2">Root Menu</label>
                        <div class="col-md-8">
                          <select class="form-control" name="root_menu" id="root_menu">
                            <option value="unselected">Select Root Menu...</option>
                          </select>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="site" class="control-label col-md-2">Site</label>
                        <div class="col-md-8">
                          <select class="form-control" name="site" id="site">
                            <option value="unselected">Select Site...</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='resp_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>
<script>
var respNameIsValid = null;
var respShortcodeIsValid = null;
var respRootMenuIsSelected = null;
var respSiteIsSelected = null;


function validate_filled(){
    if (respNameIsValid && respShortcodeIsValid && respRootMenuIsSelected && respSiteIsSelected) {
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$(document).ready(function(){
    $.getJSON('ajax/UP_SRM_GET_ROOT_MENU_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#root_menu').append($('<option></option>').val(key).html(value));
        });
    });
    $.getJSON('ajax/UP_SRM_GET_SITES_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#site').append($('<option></option>').val(key).html(value));
        });
    });
    validate_filled();


});

$('#resp').blur(function(){
  if($('#resp').val().length   >   0){
      var resp = $('#resp').val();
      $.post('ajax/UP_SRM_RESPONSIBILITY_NAME_VALID.php', {resp: resp}, function(data){
      if (data == 0){
              respNameIsValid = true;
              $('#respnamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#shortcode').focus();
                    }
      else if (data > 0){
              respNameIsValid = false;
              $('#respnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Responsibility name exists already.  Please enter a unique responsibility name.");
              $('#resp').val('');
              $('#resp').focus();
                    }
      else {
              respNameIsValid = false;
              $('#respnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new function name.");
              $('#resp').val('');
                    }
    });
  }
});

$('#shortcode').blur(function(){
  if($('#shortcode').val().length   >   0){
      $('#shortcode').val($(this).val().toUpperCase());
      $('#shortcode').val($(this).val().replace(/\s/g,''));
      var shortcode = $('#shortcode').val();
      $.post('ajax/UP_SRM_RESPONSIBILITY_SHORTCODE_VALID.php', {shortcode: shortcode}, function(data){
      if (data == 0){
              respShortcodeIsValid = true;
              $('#shortcodevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#description').focus();
                    }
      else if (data > 0){
              respShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Responsibility shortcode exists already.  Please enter a unique responsibility shortcode.");
              $('#shortcode').val('');
              $('#shortcode').focus();
                    }
      else {
              respShortcodeIsValid = false;
              $('#shortcodevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new responsibility shortcode.");
              $('#shortcode').val('');
                    }
    });
  }
});

$('#root_menu').change(function(){
  var rootMenuValue = $('#root_menu').val();

  if (rootMenuValue != 'unselected'){
    respRootMenuIsSelected = true;
    validate_filled();
  }
});

$('#site').change(function(){
  var siteValue = $('#site').val();

  if (siteValue != 'unselected'){
    respSiteIsSelected = true;
    validate_filled();
  }
});


</script>

    </body>
</html>
