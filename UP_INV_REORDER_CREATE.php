<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVROCREATE';

$return_message = null;

require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }

include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Re-Order Information</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_REORDER_CREATE_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="resp" class="control-label col-md-2">Item</label>
                        <div class="col-md-8">
                          <select class="form-control" name="item" id="item">
                            <option value="unselected">Select Item...</option>
                          </select>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Minimum Quantity</label>
                        <div class="col-md-8">
                          <input type='text' name='minqty' id='minqty' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='minqtyvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Maximum Quantity</label>
                        <div class="col-md-8">
                          <input type='text' name='maxqty' id='maxqty' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='maxqtyvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="shortcode" class="control-label col-md-2">Order Multiple Quantity</label>
                        <div class="col-md-8">
                          <input type='text' name='ordermultiple' id='ordermultiple' value='' class='form-control' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='ordermultiplevalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='resp_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Re-order planning setup created successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Re-order planning setup ended in error.</p>';
    }



  ?>
</div>

</div>


<script>
var itemIsSelected = null;
var minQtyIsValid = null;
var maxQtyIsValid = null;
var orderMultipleIsValid = null;


function validate_filled(){
    if (itemIsSelected && minQtyIsValid && maxQtyIsValid && orderMultipleIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$(document).ready(function(){
  $('#return_message').delay(5000).fadeOut();
    $.getJSON('ajax/UP_INV_REORDER_ITEM_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#item').append($('<option></option>').val(key).html(value));
        });
    });
    validate_filled();
});

$('#item').change(function(){
  var itemValue = $('#item').val();

  if (itemValue != 'unselected'){
    itemIsSelected = true;
    validate_filled();
  }
});

$('#minqty').blur(function(){
  if ($.isNumeric($("#minqty").val())){  // Validate number
        var qty_norm = $('#minqty').val();
        var qty_ent = parseInt($('#minqty').val()); //create integer
          if (qty_ent < 0){
            minQtyIsValid = false;
            $('#minqtyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#minqty').val('');
            validate_filled();
          } else if (qty_ent == ''){
            minQtyIsValid = false;
            $('#minqty').val('');
            validate_filled();
          } else if ((qty_norm%1) != 0){
            minQtyIsValid = false;
            $('#minqtyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be in full integer quantities.  No decimals allowed.");
            $('#minqty').val('');
            validate_filled();
          } else if (qty_ent > 0) {
            minQtyIsValid = true;
            $('#minqtyvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#minqty').val('');
  }
});

$('#maxqty').blur(function(){
  if ($.isNumeric($("#maxqty").val())){  // Validate number
        var qty_norm = $('#maxqty').val();
        var qty_min = $('#minqty').val();
        var qty_ent = parseInt($('#maxqty').val()); //create integer
          if (qty_ent < 0){
            maxQtyIsValid = false;
            $('#maxqtyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#maxqty').val('');
            validate_filled();
          } else if (qty_ent == ''){
            maxQtyIsValid = false;
            $('#maxqty').val('');
            validate_filled();
          } else if ((qty_norm%1) != 0){
            maxQtyIsValid = false;
            $('#maxqtyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be in full integer quantities.  No decimals allowed.");
            $('#maxqty').val('');
            validate_filled();
          } else if (qty_norm < qty_min){
            maxQtyIsValid = false;
            $('#maxqtyvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be greater than minimum quantity.");
            $('#maxqty').val('');
            validate_filled();
          } else if (qty_ent > 0) {
            maxQtyIsValid = true;
            $('#maxqtyvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#maxqty').val('');
  }
});

$('#ordermultiple').blur(function(){
  if ($.isNumeric($("#ordermultiple").val())){  // Validate number
        var qty_norm  = $('#ordermultiple').val();
        var qty_min = $('#minqty').val();
        var qty_max = $('#maxqty').val();
        var qty_ent = parseInt($('#ordermultiple').val()); //create integer
          if (qty_ent < 0){
            orderMultipleIsValid = false;
            $('#ordermultiplevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#ordermultiple').val('');
            validate_filled();
          } else if (qty_ent == ''){
            orderMultipleIsValid = false;
            $('#ordermultiple').val('');
            validate_filled();
          } else if ((qty_norm%1) != 0){
            orderMultipleIsValid = false;
            $('#ordermultiplevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be in full integer quantities.  No decimals allowed.");
            $('#ordermultiple').val('');
            validate_filled();
          } else if (qty_norm > qty_max){
            orderMultipleIsValid = false;
            $('#ordermultiplevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered must be less than maximum quantity.");
            $('#ordermultiple').val('');
            validate_filled();
          } else if (qty_ent > 0) {
            orderMultipleIsValid = true;
            $('#ordermultiplevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            validate_filled();
          }
  } else { // Clear if not a number
    $('#ordermultiple').val('');
  }
});




</script>

    </body>
</html>
