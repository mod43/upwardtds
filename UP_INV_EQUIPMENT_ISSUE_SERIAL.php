<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQPISSUESERIAL';

$return_message = null;
require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }


include 'header.php'; //includes the navigation header
?>



<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">

              <center><h1 class="panel-title">Equipment Issue</h1></center>

    </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" id="UP_INV_EQUIPMENT_ISSUE" action="#" method='post'>

                    <div class="form-group">
                      <label for="equipment" class="control-label col-sm-2">Equipment</label>
                        <div class="col-sm-8">
                          <input type='text' name='equipment' id='equipment' value='' class='form-control' tabindex='1'>
                        </div>
                        <div class="col-md-1">
                          <span id='equipmentvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="group" class="control-label col-sm-2">Group</label>
                        <div class="col-sm-8">
                          <input type='text' name='group' id='group' value='' class='form-control' tabindex='2'>
                        </div>
                        <div class="col-md-1">
                          <span id='groupvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="location" class="control-label col-sm-2">Location</label>
                        <div class="col-sm-8">
                          <input type='text' name='location' id='location' value='' class='form-control' tabindex='3'>
                        </div>
                        <div class="col-md-1">
                          <span id='locationvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="quantity" class="control-label col-sm-2">Quantity</label>
                        <div class="col-sm-8">
                          <input type='number' name='quantity' id='quantity' value='' class='form-control' placeholder='Available Quantity: ' tabindex='4' autocomplete='off'>
                       </div>
                       <div class="col-md-1">
                          <span id='quantityvalid'></span>
                        </div>
                     </div>
                     <div class="form-group" id="serialarea">
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-3 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-4">
                          <input type="submit" value="Submit" id='inv_issue_submit' class="btn btn-primary btn-block" tabindex='5'>
                        </div>
                      </div>
              </form>

  </div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Equipment Issue Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Equipment Issue ended in error.</p>';
    }



  ?>
</div>

          <!--</div>
      </div>
</div>-->

<script type="text/javascript">

var available_quantity = 0;
var equipmentIsValid = null;
var groupIsValid = null;
var locationIsValid = null;
var quantityIsValid = null;
var equipmentIsSerialEnabled = false;
var quantityEntered = null;
var serialQuantityCreated = null;
var serialsValid = null;
var available_quantity = null;
var quantityEntered = null;



$('#inv_issue_submit').click( function(){

  if (equipmentIsSerialEnabled == true){
    $('#UP_INV_EQUIPMENT_ISSUE').attr('action', 'ajax/UP_INV_EQUIPMENT_ISSUE_SERIAL_PROCESS.php');
  } else if (equipmentIsSerialEnabled == false){
    $('#UP_INV_EQUIPMENT_ISSUE').attr('action', 'ajax/UP_INV_EQUIPMENT_ISSUE_PROCESS.php');
  } 
});



// equipment autocomplete
$(function() {
  $("#equipment").autocomplete({
    source: "ajax/UP_INV_EQUIPMENT_QUERY.php",
    autoFocus: true,
    minLength: 1
  });
});


// equipment validation
$("#equipment").blur(function(){
  if (  $('#equipment').val().length   >   0 && $('#equipment').val() != '%' ){
    $('#equipment').val($(this).val().toUpperCase()); //take the equipment to uppercase
    var equipment = $('#equipment').val();
    $.post('ajax/UP_INV_EQUIPMENT_VALIDATE.php', {equipment: equipment}, function(data){
      if (data > 0){
        equipmentIsValid = true;
              $('#equipmentvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $.post('ajax/UP_INV_EQUIPMENT_SERIAL_CHECK.php', {equipment: equipment}, function(dataSerial){
                  if (dataSerial == 'Y'){
                    equipmentIsSerialEnabled = true;
                  } else if (dataSerial == 'N'){
                    equipmentIsSerialEnabled = false;
                  }

              });
      } else {
        equipmentIsValid = false;
        $('#equipment').val('');
        $('#equipmentvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      }
    });
  } else {
    $('#equipment').val('');
    $('#equipment').focus();
  }
});

$(function(){
    $("#group").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_GROUP_QUERY.php",
          data: {
            equipment : $("#equipment").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#group").blur(function(){
  if (  $('#group').val().length   >   0 &&  $('#group').val() != '%' ){
    $('#group').val($(this).val().toUpperCase());
    var group = $('#group').val();
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data > 0){
        groupIsValid = true;
              $('#groupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#location').focus();
              $('#message').html("");
      } else {
        groupIsValid = false;
              $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#group').val('');
              $('#group').focus();
              $('#message').html("Invalid Group");
      }
    });
  } else {
    groupIsValid = false;
      $('#group').val('');
      validate_filled();
  }
});

$(function(){
    $("#location").autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_FROM_LOCATION_QUERY.php",
          data: {
            from_group : $("#group").val(),
            term : request.term,
            equipment: $("#equipment").val()
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});

$("#location").blur(function(){
  if (  $('#location').val().length   >   0 &&  $('#location').val() != '%' ){
        $('#location').val($(this).val().toUpperCase());
        var location = $('#location').val();
        var group = $("#group").val()
        $.post('ajax/UP_INV_LOC_VALIDATE.php', {location: location, group: group}, function(data){
        if (data > 0){
          locationIsValid = true;
          $('#locationvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
          if (  equipmentIsValid && groupIsValid && locationIsValid ){
                var loc = $('#location').val();
                var group = $('#group').val();
                var equipment = $('#equipment').val();
                $.post('ajax/UP_INV_QTYBYLOC_EQUIPMENT_QUERY.php', {fromloc: loc, fromgroup: group, equipment: equipment}, function(data){ //Note to standardize variables
                  available_quantity=data;
                  $('#quantity').attr('placeholder', ("Available Quantity: " + data));
                });
                }
              $('#quantity').focus();
              $('#message').html("");
      } else {
        locationIsValid = false;
        $('#quantity').attr('placeholder', ("Available Quantity: "));
        validate_filled();

      }
    });
  }
});


$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    validate_filled();
    document.title= 'equipment Issue';
    $('#equipment').focus();
    $('#equipment, #group, #location, #quantity').change(validate_filled);
});

function validate_filled(){
    if (equipmentIsSerialEnabled == true){
      if (equipmentIsValid &&
        groupIsValid &&
        locationIsValid &&
        quantityIsValid &&
        serialsValid) {
          $("input[type=submit]").attr("disabled", false);
          $("input[type=submit]").focus();
      }
      else {
          $("input[type=submit]").attr("disabled", true);
      }
    } else if (equipmentIsSerialEnabled == false){
      if (equipmentIsValid &&
        groupIsValid &&
        locationIsValid &&
        quantityIsValid) {
          $("input[type=submit]").attr("disabled", false);
          $("input[type=submit]").focus();
      }
      else {
          $("input[type=submit]").attr("disabled", true);
      }
    }
};





$('#quantity').blur(function(){
  if ($.isNumeric($("#quantity").val())){  // Validate number
        var qty_ent = parseInt($('#quantity').val()); //create integer
          if (qty_ent == 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannont be 0.")
            $('#quantity').val('');
          } else if (qty_ent < 0){
            quantityIsValid = false;
            $('#quantityvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Quantity entered cannot be negative.")
            $('#quantity').val('');
          } else if (qty_ent == ''){
            quantityIsValid = false;
            $('#quantity').val('');
          } else if (qty_ent > 0) {
            quantityIsValid = true;
            $('#quantityvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
            $('#message').html("");
            quantityEntered = qty_ent;
            if (equipmentIsSerialEnabled == true){
              create_serial_fields(quantityEntered);
              serialsValid = false;
              validate_filled();
            } else {
            validate_filled();
          }
          }
  } else { // Clear if not a number
    $('#quantity').val('');
  }
});

$('#serialarea').on("change", "[id^=serialfield]", function(){
    serialsValid = false;
    validate_filled();
    validate_serials_filled();
        $(this).autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_SERIAL_QUERY.php",
          data: {
            equipment: $("#equipment").val(),
            from_group: $("#group").val(),
            from_loc: $("#location").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
});


$('#serialarea').on("blur", "[id^=serialfield]", function(){
      var serial = $(this).val();
      var fieldNumber = $(this).attr('id');
      var equipment = $('#equipment').val();
      var serialLength = $(this).val().length
      if (serialLength != 0){
          $.post('ajax/UP_INV_EQUIPMENT_SERIAL_VALIDATE.php', {serial: serial, equipment: equipment}, function(data){
            if (data > 0){
              validate_serials_filled();  
            } else {
              $('#message').html("") 
              $('#'+fieldNumber).val("");
              serialsValid = false;
              validate_serials_filled();
              validate_filled(); 
            }
          });
      }
});

function validate_serials_filled(){
    var serial_count = 1;
    var serialsFilled = null;
    var currentSerial = null;
    var enteredSerials = [];
    while(serial_count <= quantityEntered  && serialsFilled != false){
      currentSerial = $.trim($('#serialfield'+serial_count).val());
      if ($.inArray(currentSerial, enteredSerials) == -1) {
        
        if (serial_count < quantityEntered){
          
              if(!currentSerial){
                serialsFilled = false;
                $('#serialfield'+serial_count).focus();

              } else {
                enteredSerials.push(currentSerial);
              }
          

        } else if (serial_count == quantityEntered){
              
              if(!currentSerial){
                serialsFilled = false;
              } else {
                serialsValid = true;
              }

        }
      } else {

        $('#serialfield'+serial_count).val('');
        serialsValid = false;
        validate_filled();

      }
      
      serial_count++
    
    }
    
    validate_filled();

};
function create_serial_fields(qty){
  if (qty != serialQuantityCreated){
    $('#serialarea').empty();
    var serial_quantity = qty;
    var serial_count = 1;
    while (serial_count <= serial_quantity){
      if (serial_count > 1 && serial_count < serial_quantity){
        $('#serialarea').append('<div class="form-group">');  
        $('#serialarea').append('<label for="serial_'+serial_count+'" class="control-label col-md-3">Serial '+serial_count+'</label>');
        $('#serialarea').append('<div class="col-md-7"><input type="text" name="serial[]" id="serialfield'+serial_count+'" value="" class="form-control" placeholder="Serial Number '+serial_count+' " tabindex="'+(5+serial_count)+'"></div>');
        $('#serialarea').append('<div class="col-md-2"><span id="serial_'+serial_count+'valid"></span></div></div>');
        $('#serialarea').find('input[type=text]:last').autocomplete({
        source: function(request, response) {
        $.ajax({
          url: "ajax/UP_INV_EQUIPMENT_SERIAL_QUERY.php",
          data: {
            equipment: $("#equipment").val(),
            from_group: $("#from_group").val(),
            from_loc: $("#from_location").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
      } else if (serial_count == serial_quantity){
        $('#serialarea').append('<div class="form-group">');  
        $('#serialarea').append('<label for="serial_'+serial_count+'" class="control-label col-md-3">Serial '+serial_count+'</label>');
        $('#serialarea').append('<div class="col-md-7"><input type="text" name="serial[]" id="serialfield'+serial_count+'" value="" class="form-control" placeholder="Serial Number '+serial_count+' " tabindex="'+(5+serial_count)+'"></div>');
        $('#serialarea').append('<div class="col-md-2"><span id="serial_'+serial_count+'valid"></span></div>');
        $('#serialarea').find('input[type=text]:last').autocomplete({
        source: function(request, response) {
        $.ajax({
           url: "ajax/UP_INV_EQUIPMENT_SERIAL_QUERY.php",
          data: {
            equipment: $("#equipment").val(),
            from_group: $("#from_group").val(),
            from_loc: $("#from_location").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
        });
    },
        autoFocus: true,
        minLength: 1
    });
      } else {
        $('#serialarea').append('<label for="serial_'+serial_count+'" class="control-label col-md-3">Serial '+serial_count+'</label>');
        $('#serialarea').append('<div class="col-md-7"><input type="text" name="serial[]" id="serialfield'+serial_count+'" value="" class="form-control" placeholder="Serial Number '+serial_count+' " tabindex="'+(5+serial_count)+'"></div>');
        $('#serialarea').append('<div class="col-md-2"><span id="serial_'+serial_count+'valid"></span></div></div>');
        $('#serialarea').find('input[type=text]:last').autocomplete({
        source: function(request, response) {
        $.ajax({
           url: "ajax/UP_INV_EQUIPMENT_SERIAL_QUERY.php",
          data: {
            equipment: $("#equipment").val(),
            from_group: $("#from_group").val(),
            from_loc: $("#from_location").val(),
            term : request.term
          },
          dataType: "json",
          success: function( data ) {
            response( data )
          }
          });
          },
            autoFocus: true,
            minLength: 1
      });
      }
      serial_count++;
      }
      serialQuantityCreated = qty;
  }
  $('#serialfield1').focus();
};





</script>


</body>
</html>
