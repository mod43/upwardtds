<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENUSVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Upward Menu Listing</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Menu Name</th>
            <th>Menu Description</th>
            <th>Menu Type</th>
            <th>Edit Access</th>
         </tr>
          <?php

            $menus = $database->query('select smh.menu_name, smh.menu_description, smh.menu_type, smh.menu_shortcode FROM mod43fordpoc.dbo.srm_menu_header smh')->get();

            $count = count($menus);
            $l = 0;

            while ($l < $count)
            {
              echo '<tr>';
              echo '<td>'.$menus[$l]['menu_name'].'</td>';
              echo '<td>'.$menus[$l]['menu_description'].'</td>';
              echo '<td>'.$menus[$l]['menu_type'].'</td>';
              echo '<td><a href="UP_SRM_MENU_LINES_CREATE.php?menu_shortcode='.$menus[$l]['menu_shortcode'].'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>';
              echo '</tr>';
              $l++;
            }
            
          ?>
          </table>
          </div>

  </div>

</body>
</html>