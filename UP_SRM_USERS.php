<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMUSERS';


require_once 'app/init.php';
// Include app init file


/*
// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }

*/

if(!empty($_POST))
    {

        echo $_POST['username'];
        echo $_POST['password'];

        // Check that all required fields are filled in
        if(empty($_POST['username']))
        { die("Please enter a username."); }
        if(empty($_POST['password']))
        { die("Please enter a password."); }

        echo 'X';

        // Store posted variable for use with creation
        $username = $_POST['username'];
        $password = $_POST['password'];
        //$timestamp = date("Y-m-d H:i:s");
        echo 'Y';
        $created =  $auth->createUser(array(
                'user_name' => $username,
                'password' => $password,
                'enabled_flag' => 1,
                'created_by' => 1,
                'created_date' => '20161220 10:34:09 AM',//$timestamp,
                'updated_by' => 1,
                'updated_date' => '20161220 10:34:09 AM'//$timestamp
                ));
                

      $test = array('user_name' => $username,
                'password' => $password,
                'enabled_flag' => 1,
                'created_by' => 1,
                'created_date' => '20161220 10:34:09 AM',//$timestamp,
                'updated_by' => 1,
                'updated_date' => '20161220 10:34:09 AM'//$timestamp
                );
        print_r($test);

        echo 'Z';
        echo $created;


        if($created)
        {
            header('Location: UP_SRM_USERS_VIEW.php');
        }

    }


include 'header.php'; //includes the navigation header
?>



<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Username</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='UP_SRM_USERS.php' method='post'>

                    <div class="form-group">
                      <label for="person" class="control-label col-md-2">Username</label>
                        <div class="col-md-8">
                          <input type='text' name='username' id='username' value='' class='form-control' tabindex='0' autocomplete="off">
                        </div>
                        <div class="col-md-2">
                          <span id='usernamevalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="person" class="control-label col-md-2">Password</label>
                        <div class="col-md-8">
                          <input type='password' name='password' id='password' value='' class='form-control' tabindex='0'>
                        </div>
                        <div class="col-md-2">
                          <span id='passwordvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="person" class="control-label col-md-2">Re-enter Password</label>
                        <div class="col-md-8">
                          <input type='password' name='passwordcheck' id='passwordcheck' value='' class='form-control' tabindex='0'>
                        </div>
                        <div class="col-md-2">
                          <span id='passwordcheckvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='username_submit' class="btn btn-primary btn-block" tabindex='0'>
                        </div>
                      </div>

              </form>
  </div>
</div>

<script>

var usernameIsValid = null;
var passwordIsValid = null;
var passwordCheckIsValid = null;
var username = null;





$(document).ready(function (){
    validate_filled();
    $('#username, #password, #passwordcheck').change(validate_filled);
});



$("#username").blur(function(){
  if (  $('#username').val().length   >   0  ){
    $('#username').val($(this).val().toUpperCase());
    $('#username').val($(this).val().replace(/\s+/g, ''));
    username = $('#username').val();
    $.post('ajax/UP_SRM_USERNAME_VALIDATE.php', {username: username}, function(data){
      if (data == 0){
              usernameIsValid = true;
              $('#usernamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#password').focus();
      } else if (data > 0){
        usernameIsValid = false;
              $('#usernamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Username exists already.  Please enter a unique first and last name combination.");
              $('#username').val('');
              $('#username').focus();
                validate_filled();
      } else {
        usernameIsValid = false;
         $('#usernamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please enter new username.");
              $('#username').val('');
              $('#username').focus();
                validate_filled();
      }
    });
  }
});

$('#password').blur(function(){
  if ($('#password').val().length > 0){
    var password = $('#password').val();
    if (password.length < 8){
      passwordIsValid = false;
      $('#message').html("Password must be at least 8 characters.");
      $('#password').val('');
      $('#password').focus();
      $('#passwordvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        validate_filled();
    } else if (password.length > 15) {
      passwordIsValid = false;
      $('#message').html("Password must be 15 characters or less.");
      $('#password').val('');
      $('#password').focus();
      $('#passwordvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
        validate_filled();
    } else {
        if ( password == username){
          passwordIsValid = false;
        $('#message').html("Password and username can not be the same.");
        $('#password').val('');
        $('#password').focus();
        $('#passwordvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
          validate_filled();
        } else {
        passwordIsValid = true;
        $('#passwordvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
        validate_filled();
        $('#passwordcheck').focus();
          validate_filled();
        }
    }
  } else {
    $('#message').html("Password can not be blank.");
    $('#password').val('');
    $('#password').focus();
    $('#passwordvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      validate_filled();

  }
});

$('#passwordcheck').blur(function () {

  var password1 = $('#password').val();
  var password2 = $('#passwordcheck').val();
  if (!(password1 == password2)){
    passwordCheckIsValid = false;
    $('#message').html("Passwords do not match.");
    $('#passwordcheck').val('');
    $('#password').val('');
    $('#passwordcheckvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      validate_filled();
      $('#password').focus();
  } else {
    passwordCheckIsValid = true;
    $('#message').html("");
    $('#passwordcheckvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
    validate_filled();
    $('#username_submit').focus();
  }

});


function validate_filled(){
    if (usernameIsValid &&
        passwordIsValid &&
        passwordCheckIsValid ) {
        $("input[type=submit]").attr("disabled", false);
        $("input[type=submit]").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


</script>

              </body>
</html>
