<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOPICKABLEVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header

?>

<div class="col-md-12">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Pickable Work Orders</h1></center>

</div>
<div class="panel-body">
        <div class="table-responsive">
          <table class="table">
          <tr>
            <th>WO Number</th>
            <th>Delivery Name</th>
            <th>Delivery Location</th>
            <th>Scheduled Date</th>
         </tr>
          <?php

            $work_orders = $database->query('SELECT wo.work_order_header_id, wo.work_order_number, 
                                              wo.deliver_to_name, wo.deliver_to_location, wo.scheduled_date
                                              FROM wo_work_order_header wo 
                                              WHERE wo.wo_site_id = 1 
                                              AND wo.status = 2
                                              ORDER BY wo.scheduled_Date, wo.work_order_number ASC')->get();

            $count = count($work_orders);
            $l = 0;

            while ($l < $count)
            {
              echo '<tr>';
              echo '<td><a href="UP_WO_PICK_ORDER_VIEW.php?wo_header_number='.$work_orders[$l]->work_order_number.'">'.$work_orders[$l]->work_order_number.'</a></td>';
              echo '<td>'.$work_orders[$l]->deliver_to_name.'</td>';
              echo '<td>'.$work_orders[$l]->deliver_to_location.'</td>';
              echo '<td>'.$work_orders[$l]->scheduled_date.'</td>';
              echo '</tr>';
              $l++;
            }
            
          ?>
          </table>
          </div>

  </div>

</body>
</html>