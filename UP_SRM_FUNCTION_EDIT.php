<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMFUNCTIONEDIT';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens function which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

$function_shortcode = $_GET['shortcode'];

if (!$function_shortcode)
   {
      // die if not logged in
      header("Location: UP_SRM_FUNCTION_VIEW.php");
      die("No function passed.");
    }

$function_info = $database->table('mod43fordpoc.dbo.srm_functions')->where('function_shortcode','=',$function_shortcode)->first();

$function_menu_lines = $database->table('mod43fordpoc.dbo.srm_menu_lines')->where('menu_line_function_id','=', $function_info['function_id'])->get();

$function_menu_count = count($function_menu_lines);



include 'header.php'; //includes the navigation header
//var_dump($function_info);
//var_dump($function_menu_lines);
//var_dump($function_menu_count);
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Edit Function - <?php echo $function_info[0]['function_name']; ?></h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_FUNCTION_EDIT_PROCESS.php' method='post'>
                
                    <div class="form-group">
                      <label for="function_name" class="control-label col-md-2">Function Name</label>
                        <div class="col-md-8">
                          <input type='text' name='function_name' id='function_name' value='<?php echo $function_info['function_name']; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='functionnamevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="description" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                          <input type='text' name='description' id='description' value='<?php echo $function_info['description']; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='descriptionvalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="filename" class="control-label col-md-2">Filename</label>
                        <div class="col-md-8">
                          <input type='text' name='filename' id='filename' value='<?php echo $function_info['filename']; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='filenamevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <div class="col-xs-1">
                        <input type="hidden" name="function_id" value=<?php echo '"'.$function_info['function_id'].'"'; ?> >
                      </div>
                                            <div class="col-xs-1">
                        <input type="hidden" name="function_shortcode" value=<?php echo '"'.$function_info['function_shortcode'].'"'; ?> >
                      </div>
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>   
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='function_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>

                      <?php

                        if ($function_menu_count == 0){
                            echo '<div class="form-group">';
                            echo '<div class="col-md-offset-2 col-md-8">';
                            echo '<button id= "delete_button" type="button"  class="btn btn-danger btn-block" href="#" data-toggle="modal" data-target="#confirm-delete" data-href="ajax/UP_SRM_FUNCTION_DELETE_PROC.php?shortcode='.$function_info->function_shortcode.'">Delete Function</button>';
                            echo '</div>';
                            echo '</div>';
                        } else {
                          echo '<div class="form-group">';
                            echo '<div class="col-md-offset-2 col-md-8">';
                            echo '<div class="alert alert-info">Function exists on menus.  Please remove from menus if you wish to delete.</div>';
                            echo '</div>';
                            echo '</div>';
                        }

                      ?>
              </form>
  </div>
</div>  

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>
            
                <div class="modal-body">
                    <p>You are about to delete this function for all users and sites, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="debug-url"></p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>


<script>

   $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });

var functionNameIsValid = null;
var descriptionIsValid = null;
var filenameIsValid = null;
    var function_current = $('#function_name').val();
    var description_current = $('#description').val();
    var filename_current = $('#filename').val();

$(document).ready(function(){
    validate_filled();
    $('#function_name').change(validate_filled);
    $('#description').change(validate_filled);
    $('#filename').change(validate_filled);
});


function validate_filled(){
    var description_check = $('#description').val();
    var function_check = $('#function_name').val();
    var filename_check =  $('#filename').val();

    if (functionNameIsValid && (description_current == description_check) && (filename_current == filename_check)){
        $('#descriptionvalid').html("");
        $("input[type=submit]").attr("disabled", false);
        $("#function_name_submit").focus();
    } else if(descriptionIsValid && (function_current == function_check) && (filename_current == filename_check)){
        $('#functionnamevalid').html("");
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    } else if(filenameIsValid && (function_current == function_check) && (description_current == description_check)){
        $('#functionnamevalid').html("");
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    } else if(functionNameIsValid && descriptionIsValid && filenameIsValid){
        $("input[type=submit]").attr("disabled", false);
        $("#function_submit").focus();
    } else {
        $("input[type=submit]").attr("disabled", true);
    }
};


$("#function_name").blur(function(){
  if($('#function_name').val().length   >   0){
      var function_name = $('#function_name').val();
      if (function_name == function_current){
            functionNameIsValid = false;
            $('#functionnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Function name has not changed.  Please enter a new name or hit back to exit.");
            validate_filled();
          }
        else {
          $.post('ajax/UP_SRM_FUNCTION_NAME_VALIDATE.php', {function_name: function_name}, function(data){
          if (data == 0){
                  functionNameIsValid = true;
                  $('#functionnamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
                  $('#message').html("");
                  validate_filled();
                        } 
          else if (data > 0){
                  functionNameIsValid = false;
                  $('#functionnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Function exists already.  Please enter a unique function name.");
                  $('#function_name').val('');
                  $('#function_name').focus();
                        } 
          else {
                  functionNameIsValid = false;
                  $('#functionnamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Unknown error.  Please re-enter new function name.");
                  $('#function_name').val('');
                        }           
        });
      }
  }
});


$("#description").blur(function(){
  if($('#description').val().length   >   0){
      var description = $('#description').val();
      if (description == description_current){
            descriptionNameIsValid = false;
            $('#descriptionvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Description has not changed.  Please enter a new description or hit back to exit.");
            validate_filled();
            $('#function_name').focus();
          }
        else {
              descriptionIsValid = true;
              $('#descriptionvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#function_submit').focus();
             }
  }
});

$("#filename").blur(function(){
  if($('#filename').val().length   >   0){
      var filename = $('#filename').val();
      if (filename == filename_current){
            functionNameIsValid = false;
            $('#filenamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("function name has not changed.  Please enter a new name or hit back to exit.");
            validate_filled();
            $('#description').focus();
          }
        else {
          $.post('ajax/UP_SRM_FILENAME_VALIDATE.php', {filename: filename}, function(data){
          if (data == 0){
                  filenameIsValid = true;
                  $('#filenamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
                  $('#message').html("");
                  validate_filled();
                        } 
          else if (data > 0){
                  filenameIsValid = false;
                  $('#filenamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Function filename already exists.  Please enter a unique filename.");
                  $('#filename').val('');
                  $('#filename').focus();
                        } 
          else {
                  filenameIsValid = false;
                  $('#filenamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Unknown error.  Please re-enter new filename.");
                  $('#filename').val('');
                        }           
        });
      }
  }
});


</script>

    </body>
</html>