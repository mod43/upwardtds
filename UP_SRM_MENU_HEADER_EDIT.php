<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENUCREATE';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

$menu_shortcode = $_GET['menu_shortcode'];

if (!$menu_shortcode)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }

$menu_info = $database->table('mod43fordpoc.dbo.srm_menu_header')->where('menu_shortcode','=',$menu_shortcode)->first();



include 'header.php'; //includes the navigation header
?>

<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Edit Menu - <?php echo $menu_info['menu_name']; ?></h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_SRM_MENU_HEADER_EDIT_PROCESS.php' method='post'>
                
                    <div class="form-group">
                      <label for="menu" class="control-label col-md-2">Menu Name</label>
                        <div class="col-md-8">
                          <input type='text' name='menu' id='menu' value='<?php echo $menu_info->menu_name; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='menunamevalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <label for="description" class="control-label col-md-2">Description</label>
                        <div class="col-md-8">
                          <input type='text' name='description' id='description' value='<?php echo $menu_info->menu_description; ?>' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='descriptionvalid'></span>
                        </div> 
                     </div>
                     <div class="form-group">
                      <div class="col-xs-1">
                        <input type="hidden" name="menu_id" value=<?php echo '"'.$menu_info['menu_header_id'].'"'; ?> >
                      </div>
                                            <div class="col-xs-1">
                        <input type="hidden" name="menu_shortcode" value=<?php echo '"'.$menu_info['menu_shortcode'].'"'; ?> >
                      </div>
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>   
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='menu_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>  
<script>
var menuNameIsValid = null;
var descriptionIsValid = null;
var menu_current = $('#menu').val();
var description_current = $('#description').val();

function validate_filled(){
    var description_check = $('#description').val();
    var menu_check = $('#menu').val();

    if (menuNameIsValid && (description_current == description_check)) {
        $('#descriptionvalid').html("");
        $("input[type=submit]").attr("disabled", false);
        $("#menution_submit").focus();
    } else if(descriptionIsValid && (menu_current == menu_check)){
        $('#menunamevalid').html("");
        $("input[type=submit]").attr("disabled", false);
        $("#menution_submit").focus();
    } else if(menuNameIsValid && descriptionIsValid){
              $("input[type=submit]").attr("disabled", false);
        $("#menution_submit").focus();
    } else {
        $("input[type=submit]").attr("disabled", true);
    }
};

$(document).ready(function(){
    validate_filled();
    $('#menu').change(validate_filled);
    $('#description').change(validate_filled);
});

$("#menu").blur(function(){
  if($('#menu').val().length   >   0){
      var menu = $('#menu').val();
      if (menu == menu_current){
            menuNameIsValid = false;
            $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Menu name has not changed.  Please enter a new name or hit back to exit.");
            validate_filled();
            $('#description').focus();
          }
        else {
          $.post('ajax/UP_SRM_MENU_NAME_VALIDATE.php', {menu: menu}, function(data){
          if (data == 0){
                  menuNameIsValid = true;
                  $('#menunamevalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
                  $('#message').html("");
                  validate_filled();
                        } 
          else if (data > 0){
                  menuNameIsValid = false;
                  $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Menu name exists already.  Please enter a unique menu name.");
                  $('#menu').val('');
                  $('#menu').focus();
                        } 
          else {
                  menuNameIsValid = false;
                  $('#menunamevalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
                  $('#message').html("Unknown error.  Please re-enter new menu name.");
                  $('#menu').val('');
                        }           
        });
      }
  }
});


$("#description").blur(function(){
  if($('#description').val().length   >   0){
      var description = $('#description').val();
      if (description == description_current){
            descriptionNameIsValid = false;
            $('#descriptionvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
            $('#message').html("Description has not changed.  Please enter a new description or hit back to exit.");
            validate_filled();
            $('#menu').focus();
          }
        else {
              descriptionIsValid = true;
              $('#descriptionvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#menu_submit').focus();
             }
  }
});



</script>

    </body>
</html>