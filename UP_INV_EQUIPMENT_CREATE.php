<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVEQUIPMENTCREATE';


require_once 'app/init.php';
// Include app init file


// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
    die("Redirecting to index.php");
        header("Location: index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.

$return_message = null;

if (!$function_access)
   {
      // die if not logged in
    die("You do not have access to this function.");
        header("Location: index.php");
    }

    if(isset($_GET['message']))
    {
      $return_message = $_GET['message'];
    }


include 'header.php'; //includes the navigation header
?>

<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Create Equipment</h1></center>

</div>
<div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_EQUIPMENT_CREATE_PROC.php' method='post'>

                    <div class="form-group">
                      <label for="equipment" class="control-label col-md-3">Equipment</label>
                        <div class="col-md-8">
                          <input type='text' name='equipment' id='equipment' value='' class='form-control' autocomplete='off'>
                        </div>
                        <div class="col-md-1">
                          <span id='equipvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                      <label for="equip_desc" class="control-label col-md-3">Equipment Description</label>
                        <div class="col-md-8">
                          <input type='text' name='equip_desc' id='equip_desc' value='' class='form-control' autocomplete='off'>
                        </div>
                        <div class="col-md-1">
                          <span id='descvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Status</label>
                        <div class="col-md-8">
                          <select class="form-control" name="status" id="status">
                              <option value="Active" selected>Active</option>
                              <option value="Inactive">Inactive</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='statusvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Serial Enabled</label>
                        <div class="col-md-8">
                          <select class="form-control" name="serial_enabled" id="serial_enabled">
                              <option value="unselected">Select Option...</option>
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='serialvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                      <label for="status" class="control-label col-md-3">Calibration Tracked</label>
                        <div class="col-md-8">
                          <select class="form-control" name="calibration" id="calibration">
                              <option value="unselected">Select Option...</option>
                              <option value="1">YES</option>
                              <option value="0">NO</option>
                          </select>
                        </div>
                        <div class="col-md-1">
                          <span id='calibrationvalid'></span>
                        </div>
                     </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-5 col-md-4">
                          <input type="submit" value="Submit" id='equipment_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
</div>
</div>

<div id='return_message'>
  <?php

    if ($return_message == 'success')
    {
      echo '<p class="bg-success" id="return">Equipment Created Successful</p>';
    } elseif ($return_message == 'error'){
      echo '<p class="bg-danger" id="return">Equipment Creation ended in error.</p>';
    }



  ?>
</div>

</div>


<script type="text/javascript">

var equipmentIsValid = null;
var descIsValid = null;
var serialChosen = null;
var calibrationChosen = null;
var calibrationSerialCheck = null;

$(document).ready(function (){
    $('#return_message').delay(5000).fadeOut();
    validate_filled();
    $('#equipment').focus();
    $('#equipment, #equip_desc, #status').change(validate_filled);

});

function validate_filled(){
    if (equipmentIsValid &&
        descIsValid &&
        serialChosen &&
        calibrationChosen &&
        calibrationSerialCheck) {
        $("input[type=submit]").attr("disabled", false);
        $("#equipment_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};

function check_serial(){
    var serialType = $('#serial_enabled option:selected').attr('value');
    var calibrationType = $('#calibration option:selected').attr('value');

    if (calibrationType == 1) {
      if (serialType == 1){
          calibrationSerialCheck = true;
      } else {
          calibrationSerialCheck = false;
          $('#message').html("Calibrated items must be serial controlled.  Please Enable Serial Control.");
      }
    } else {
      calibrationSerialCheck = true;
    }

};

$("#equipment").blur(function(){
  if (  $('#equipment').val().length   >   0  &&  $('#equipment').val() != '%' ){
    $('#equipment').val($(this).val().toUpperCase());
    var equipment = $('#equipment').val();
    $.post('ajax/UP_INV_EQUIPMENT_VALIDATE.php', {equipment: equipment}, function(data){
      if (data == 0){
        equipmentIsValid = true;
              $('#equipvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              $('#equip_desc').focus();
              validate_filled();
      } else if (data > 0){
        equipmentIsValid = false;
              $('#equipvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Equipment exists already.  Please enter a unique equipment name.");
              $('#equipment').val('');
              $('#equipment').focus();
              validate_filled();
      } else {
        equipmentIsValid = false;
         $('#equipvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new equipment name.");
              $('#equipment').val('');
              validate_filled();
      }
    });
  } else {
            equipmentIsValid = false;

              $('#equipment').val('');
              validate_filled();
  }
});

$('#equip_desc').blur(function(){
  if ($('#equip_desc').val().length > 0){
      descIsValid = true;
      $('#descvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
  } else {
              descIsValid = false;
              $('#equip_desc').val('');
              validate_filled();
  }
});


$('#serial_enabled').change(function (){

  var serialChoice = $('#serial_enabled option:selected').attr('value');
  if (serialChoice != 'unselected'){
    serialChosen = true;
    check_serial();
    validate_filled();
    } else {
      $('#message').html("Please make a choice from the drop down");
      serialChosen = false;
      validate_filled();
    }

});


$('#calibration').change(function (){

  var calibrationChoice = $('#calibration option:selected').attr('value');
  if (calibrationChoice != 'unselected'){
    calibrationChosen = true;
    check_serial();
    validate_filled();
    } else {
      $('#message').html("Please make a choice from the drop down");
      calibrationChosen = false;
      validate_filled();
    }

});






</script>


</body>
</html>
