<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'INVGROUPCREATE';


require_once 'app/init.php';
// Include app init file

$return_message = null;

// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }




include 'header.php'; //includes the navigation header

?>



<div class="col-sm-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
  <div class="panel-heading">

              <center><h1 class="panel-title">Create Group</h1></center>

  </div>
  <div class="panel-body">



<!-- <div class="container-fluid">
    <div class="row-fluid">
          <div class="span6"> -->

              <form class="form-horizontal" action='ajax/UP_INV_GROUP_CREATE_PROCESS.php' method='post'>

                    <div class="form-group">
                      <label for="group" class="control-label col-md-2">Group</label>
                        <div class="col-md-8">
                          <input type='text' name='group' id='group' value='' class='form-control'>
                        </div>
                        <div class="col-md-2">
                          <span id='groupvalid'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <span id='message'></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-md-offset-2 col-md-8">
                          <input type="submit" value="Submit" id='group_submit' class="btn btn-primary btn-block">
                        </div>
                      </div>
              </form>
  </div>
</div>

<div id='return_message'>

</div>

<script>

var groupIsValid = null;

$(document).ready(function (){
    validate_filled();
    $('#group').change(validate_filled);
    $('#return_message').delay(5000).fadeOut();
});

$("#group").blur(function(){
  if (  $('#group').val().length   >   0  ){
    $('#group').val($(this).val().toUpperCase());
    var group = $('#group').val();
    var hasSpace = $('#group').val().indexOf(' ')>=0;
    if (!hasSpace){
    $.post('ajax/UP_INV_GROUP_VALIDATE.php', {group: group}, function(data){
      if (data == 0){
              groupIsValid = true;
              $('#groupvalid').html("<button type='button' class='btn btn-success'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span></button>");
              $('#message').html("");
              validate_filled();
              $('#group_submit').focus();
      } else if (data > 0){
        groupIsValid = false;
              $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Group exists already.  Please enter a unique group name.");
              $('#group').val('');
              $('#group').focus();
      } else {
        groupIsValid = false;
         $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
              $('#message').html("Unknown error.  Please re-enter new group name.");
              $('#group').val('');
      }
    });
    } else {
      $('#groupvalid').html("<button type='button' class='btn btn-danger'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button>");
      $('#message').html("Group names cannot contain spaces.  Please enter a different group name.");
      $('#group').val('');
      $('#group').focus();
    }
  }
});

function validate_filled(){
    if (groupIsValid) {
        $("input[type=submit]").attr("disabled", false);
        $("#group_submit").focus();
    }
    else {
        $("input[type=submit]").attr("disabled", true);
    }
};


</script>

              </body>
</html>
