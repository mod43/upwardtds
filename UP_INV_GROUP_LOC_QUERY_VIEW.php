<?php
    
    require("config.php");
    if(empty($_SESSION['user_id'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

include 'pdo_creds.php';

include 'header.php';

	echo '<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Groups and Locations</h1></center>

</div>
<div class="panel-body">';

$dbh = new PDO($connection, $username, $password);

$stg = $dbh->prepare("select * from inventory_group");
$stg->execute();
$result = $stg->fetchAll(PDO::FETCH_ASSOC);

/* Fetch all of the remaining rows in the result set */

echo '

		<div class="col-xs-12">
			<div class="panel panel-default">
		  		<div class="panel-heading"><b>Inventory Groups</b></div>
				  <table class="table">
				  <tr>
				  	<th>Group Name</th>
				  </tr>';
				  foreach ($result as $row => $cur) {
							echo '<tr><td>'.$cur['group_name'].'</td></tr>';
						}
						echo '</table>
			</div>
		</div>';

$stl = $dbh->prepare("select il.location_name, ig.group_name from inventory_location il, inventory_group ig
where il.group_id = ig.group_id
order by location_name asc");
$stl->execute();
$result = $stl->fetchAll(PDO::FETCH_ASSOC);

echo '<div class="col-xs-12">
			<div class="panel panel-default">
		  	<!-- Default panel contents -->
		  		<div class="panel-heading"><b>Inventory Locations</b></div>

				  <!-- Table -->
				  <table class="table">
				  <tr>
				  	<th>Location Name</th>
				  	<th>Group Name</th>
				  </tr>';



						foreach ($result as $row => $cur) {
							echo '<tr><td>'.$cur['location_name'].'</td><td>'.$cur['group_name'].'</td></tr>';
						}

						echo '</table>
				</div>
			</div>
		</div>
	</div>';


echo "</body>";
echo "</html>";
?>