<?php
   
    require("config.php");
    if(empty($_SESSION['user'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

include 'pdo_creds.php';

include 'header.php';

	echo '<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Site Listing</h1></center>

</div>
<div class="panel-body">';

$dbh = new PDO($connection, $username, $password);

$sth = $dbh->prepare("SELECT * FROM organizations");
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);

/* Fetch all of the remaining rows in the result set */

echo '


				  <!-- Table -->
				  <table class="table">
				  <tr>
				  	<th>ID#</th>
				  	<th>Name</th>
				  </tr>';



						foreach ($result as $row => $cur) {
							echo '<tr><td>'.$cur['org_id'].'</td><td>'.$cur['org_name'].'</td></tr>';
						}

						echo '</table>
				</div>
	</div>';

echo "</body>";
echo "</html>";
?>