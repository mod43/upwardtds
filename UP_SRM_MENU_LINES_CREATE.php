<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMMENULINESCREATE';

require_once 'app/init.php';
// Include app init file

// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function.");

    }

include 'header.php'; //includes the navigation header
if(isset($_GET['menu_shortcode'])){
  $menu_shortcode= $_GET['menu_shortcode'];
} else {
//  header("Location: UP_SRM_MENUS_VIEW.php");
    }

$menu_header_info = $database->table('mod43fordpoc.dbo.srm_menu_header')->where('menu_shortcode','=',$menu_shortcode)->first();

$menu_header_id = $menu_header_info[0]['menu_header_id'];

$menu_lines = $database->table('mod43fordpoc.dbo.up_srm_menu_lines_view')->where('srm_menu_header_id','=', $menu_header_id)->get();

if ($menu_header_info[0]['menu_type'] == 'M'){
  $type = 'Main  Menu';
  $description = 'Main menu lines can be made up of both functions and submenus.';
  $on_resp = $database->checkMainOnResp($menu_header_id)->get();
  $resp_count = count($on_resp);
    if ($resp_count > 0){
      $removable = false;
    } else {
      $removable = true;
    }
} elseif ($menu_header_info[0]['menu_type'] == 'S') {
  $type = 'Submenu';
  $description = 'Submenu menu lines can only be functions';
  $on_main = $database->checkSubmenuOnMain($menu_header_id)->get();
  $main_count = count($on_main);
    if ($main_count > 0){
      $removable = false;
    } else {
      $removable = true;
    }
} else {
  $status = 'Undefined';
}






$count = count($menu_lines);
$ln = 0;

?>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Menu Setup - <?php echo $menu_header_info[0]['menu_name']; ?></h1></center>
    </div>
    <div class="panel-body">
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Menu Name: </strong></h5></div>
          <div class="col-sm-8 text-left"><h5><?php echo $menu_header_info[0]['menu_name']; ?></h5></div>
          <div class="col-sm-1 text-left"><a href=<?php echo '"UP_SRM_MENU_HEADER_EDIT.php?menu_shortcode='.$menu_shortcode.'"'; ?>><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Menu Type: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $type; ?></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Notes: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $description; ?></h5></div>
        </div>

      <?php

        if ($removable){
            echo '<div class="row">';
            echo '<div class="col-md-offset-2 col-md-8">';
            echo '<button id= "delete_button" type="button"  class="btn btn-danger btn-block" href="#" data-toggle="modal" data-target="#confirm-delete" data-href="ajax/UP_SRM_MENU_DELETE_PROC.php?id='.$menu_header_id.'">Delete Menu</button>';
            echo '</div>';
            echo '</div>';
        } elseif (($type === 'Main  Menu') && !$removable) {
            echo '<div class="row">';
            echo '<div class="col-md-offset-2 col-md-8">';
            echo '<div class="alert alert-info">Main Menu is assigned to a responsibility.  Please first delete responsibility if you wish to delete this menu.</div>';
            echo '</div>';
            echo '</div>';
        } elseif (($type === 'Submenu') && !$removable) {
            echo '<div class="row">';
            echo '<div class="col-md-offset-2 col-md-8">';
            echo '<div class="alert alert-info">Submenu is part of a main menu.  Please remove from main menus if you wish to delete this menu.</div>';
            echo '</div>';
            echo '</div>';
        }

      ?>
    </div>
      <div class="col-sm-6">
        <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
          <div class="panel-heading">
            <center><h1 class="panel-title">Add Menu Lines</h1></center>
          </div>
          <div class="panel-body">
            <form class="form-horizontal" action='ajax/UP_SRM_MENU_ADD_LINE_PROC.php' method='post'>
              <div class="form-group">

                <div class="row">
                  <div class="col-xs-offset-1 col-xs-2">
                    <label for="item" class="control-label">Line Type</label>
                  </div>
                  <div class="col-xs-7">
                    <select class="form-control" name="object_type" id="object_type">
                            <?php if ($menu_header_info[0]['menu_type'] == 'M'){
                                echo '<option value="unselected">Select Object Type...</option>';
                                echo '<option value="1">Submenu</option>';
                                echo '<option value="2">Function</option>';
                            } else {
                                echo '<option value="2">Function</option>';
                            }
                            ?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-offset-1 col-xs-2">
                    <label for="object" class="control-label">Object:</label>
                  </div>
                  <div class="col-xs-7">
                    <select class="form-control" name="object" id="object">
                    </select>
                  <div class="col-xs-2">
                    <span id='quantityvalid'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-1">
                    <input type="hidden" name="menu_shortcode" value=<?php echo '"'.$menu_shortcode.'"'; ?> >
                  </div>
                  <div class="col-xs-1">
                    <input type="hidden" name="menu_header_id" value=<?php echo '"'.$menu_header_id.'"'; ?> >
                  </div>
                  <div class="col-xs-offset-2 col-xs-10">
                    <span id='message'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-offset-4 col-xs-4">
                    <input type="submit" value="Add" id='item_submit' class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 1px 1px 1px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Menu Lines</h1></center>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Sequence</th>
                <th>Type</th>
                <th>Object</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php

                while($ln < $count){

                  echo '<tr>';
                  echo '<td>'.$menu_lines[$ln]['srm_menu_line_sequence'].'</td>';
                  echo '<td>'.$menu_lines[$ln]['line_type'].'</td>';
                  echo '<td>'.$menu_lines[$ln]['line_type_name'].'</td>';
                  echo '<td><a href="ajax/UP_SRM_MENU_DELETE_LINE_PROC.php?menu_shortcode='.$menu_shortcode.'&line_id='.$menu_lines[$ln]['srm_menu_line_id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>';
                  echo '</tr>';
                  $ln++;
                }
              ?>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
            </div>

            <div class="modal-body">
                <p>You are about to delete this menu for all users and sites? this procedure is irreversible.</p>
                <p>Do you want to proceed?</p>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

$('#confirm-delete').on('show.bs.modal', function(e) {
         $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
     });

$(document).ready(function (){
var loadObjectType = $('#object_type option:selected').attr('value');
if (loadObjectType == 2 ){
  $.getJSON('ajax/UP_SRM_GET_FUNCTIONS_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#object').append($('<option></option>').val(key).html(value));
        });
      });
}

});

$('#object_type').change(function (){

  $('#object').find('option').remove().end();
  var objectTypeSelected = $('#object_type option:selected').attr('value');

    if (objectTypeSelected == 2){
      $.getJSON('ajax/UP_SRM_GET_FUNCTIONS_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#object').append($('<option></option>').val(key).html(value));
        });
      });
    } else if (objectTypeSelected == 1){
      $.getJSON('ajax/UP_SRM_GET_SUBMENUS_QUERY.php', function(data){
        $.each( data, function( key, value ){
          //console.log( key + ": " + value );
          $('#object').append($('<option></option>').val(key).html(value));
        });
      });
    }
});






</script>


</body>
</html>
