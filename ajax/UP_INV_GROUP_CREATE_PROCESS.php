<?php

include 'search_creds.php';
include '../app/init.php';

$group = $_POST['group'];
$site_id = $_SESSION['site_id'];


if (isset($group)){


  try {
      $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      $stmt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_group_create :group, :site_id'); //, @group_create_status
      $stmt->bindParam(':group', $group);
      $stmt->bindParam(':site_id', $site_id);          
      $stmt->execute();
      

      $stmt2 = $conn->prepare("SELECT COUNT(*) from mod43fordpoc.dbo.inv_groups where group_name = :group");
      $stmt2->bindParam(':group', $group);
      $stmt2->execute();
      $row = $stmt2->fetchcolumn();
        
      if ($row >= 1){
        header("Location: ../UP_INV_GROUP_CREATE.php?message=Success");
      }
      else{header("Location: ../UP_INV_GROUP_CREATE.php?message=Error");
      }
            


  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>
