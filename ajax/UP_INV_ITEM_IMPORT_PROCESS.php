<?php
    require("config.php");
    if(empty($_SESSION['user'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }
include 'header.php';

//Upload File
   if (is_uploaded_file($_FILES['file']['tmp_name'])) {
        echo "<h1>" . "File ". $_FILES['file']['name'] ." uploaded successfully." . "</h1>";
    }
    //Import uploaded file to Database
    $handle = fopen($_FILES['file']['tmp_name'], "r");
    $dbh = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
       $import = $dbh->prepare("CALL proc_create_item(?, ?, ?, ?, ?)");
       $import->bindParam(1, $data[0], PDO::PARAM_STR);
       $import->bindParam(2, $data[1], PDO::PARAM_STR);
       $import->bindParam(3, $data[2], PDO::PARAM_STR);
       $import->bindParam(4, $data[3], PDO::PARAM_STR);
       $import->bindParam(5, $data[4], PDO::PARAM_INT);
       $import->execute();
     }
    fclose($handle);
    print "Import done";

?>