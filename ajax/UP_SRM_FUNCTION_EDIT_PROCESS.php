<?php

include 'search_creds.php';
include '../app/init.php';

$function = $_POST['function_name'];
$description = $_POST['description'];
$filename = $_POST['filename'];
$function_id = $_POST['function_id'];
$function_shortcode = $_POST['function_shortcode'];


// TIME TO WRITE QUERY HERE.

$current_function_info = $database->table('srm_functions')->where('function_id', '=', $function_id)->first();

$current_function_name = $current_function_info->function_name;
$current_function_description = $current_function_info->description;
$current_filename = $current_function_info->filename;



if ($function != $current_function_name){

		$update_function = $database->table('srm_functions')->updateByID('function_name', $function, 'function_id', $function_id);

}

if ($description != $current_function_description){

		$update_description = $database->table('srm_functions')->updateByID('description', $description, 'function_id', $function_id);
}

if ($filename != $current_filename){

		$update_filename = $database->table('srm_functions')->updateByID('filename', $filename, 'function_id', $function_id);
}

if ($update_menu || $update_description || $update_filename){
	header('Location: ../UP_SRM_FUNCTION_EDIT.php?shortcode='.$function_shortcode);
} else {
	header("Location: ../UP_SRM_MENU_VIEW.php");
}


?>