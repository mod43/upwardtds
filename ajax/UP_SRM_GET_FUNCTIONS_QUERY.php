<?php

include 'search_creds.php';


// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT function_name, function_id FROM mod43fordpoc.dbo.srm_functions WHERE enabled_flag = 1');
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[$row['function_id']] =  $row['function_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>