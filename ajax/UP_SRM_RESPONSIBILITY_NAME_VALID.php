<?php

include 'search_creds.php';

$resp = $_POST['resp'];

// TIME TO WRITE QUERY HERE.

if (isset($resp)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as resp_count FROM mod43fordpoc.dbo.srm_responsibility WHERE responsibility_name LIKE :resp');
	    $stmt->bindParam(':resp', $resp);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['resp_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>