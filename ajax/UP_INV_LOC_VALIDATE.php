<?php

include 'search_creds.php';
include '../app/init.php';

$location = $_POST['location'];
$group = $_POST['group'];
$site_id = $_SESSION['site_id'];


// TIME TO WRITE QUERY HERE.

if (isset($location)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare("SELECT count(il.location_name) as location_count FROM mod43fordpoc.dbo.inv_locations il, mod43fordpoc.dbo.inv_groups ig 
	    	WHERE il.location_name LIKE :location and il.site_id = :site_id and il.group_id =ig.group_id and ig.group_name = :group");
	    $stmt->bindParam(':location', $location);
	    $stmt->bindParam(':group', $group);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['location_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}

    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>
