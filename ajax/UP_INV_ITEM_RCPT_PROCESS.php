<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$group = $_POST['group'];
$location = $_POST['location'];
$quantity = $_POST['quantity'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
$comment = $_POST['comment'];

// TIME TO WRITE QUERY HERE.





if (isset($item)){


	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_rcpt_txn :item, :group, :location, :quantity, :site_id, :user_id, :comment');
	    $stmt->bindParam(':item', $item);
			$stmt->bindParam(':group', $group);
			$stmt->bindParam(':location', $location);
			$stmt->bindParam(':quantity', $quantity);
			$stmt->bindParam(':site_id', $site_id);
			$stmt->bindParam(':user_id', $user_id);
			$stmt->bindParam(':comment', $comment);
			$stmt->execute();

			header('Location: ../UP_INV_ITEM_RCPT_SERIAL.php?message=success');

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}



?>
