<?php

include 'search_creds.php';
include '../app/init.php';

$equipment = $_POST['equipment'];
$serial = $_POST['serial'];
$site_id = $_SESSION['site_id'];

// TIME TO WRITE QUERY HERE.

if (isset($equipment)){
	$return_arr = array();


	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT count(*) as serial_count 
								from inv_equipment_serials ies 
								where ies.site_id = :site_id 
								and ies.equipment_id = (select equipment_id from mod43fordpoc.dbo.inv_equipment_master where equipment_name = :equipment and site_id = :site_id) 
								and ies.serial_number = :serial_number');
	    $stmt->bindParam(':equipment', $equipment);
	    $stmt->bindParam(':serial_number', $serial);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['serial_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>
