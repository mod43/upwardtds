<?php

include '../app/init.php';

$group = $_POST['group'];
$site_id = $_SESSION['site_id'];





if (isset($group)){


  try {
      $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $conn->prepare('CALL proc_inv_group_create(:group, :site_id, @group_create_status)');
      $stmt->bindParam(':group', $group);
      $stmt->bindParam(':site_id', $site_id);
      $stmt->execute();
      $stmt->closeCursor();

      $r = $conn->query('SELECT @group_create_status AS group_create_status')->fetch(PDO::FETCH_ASSOC);

      $create_status = $r['group_create_status'];

      if ($create_status == 'Success!'){
        header("Location: ../UP_INV_GROUP_CREATE.php?message=Success");
      } else {
        header("Location: ../UP_INV_GROUP_CREATE.php?message=Error");
      }




  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>
