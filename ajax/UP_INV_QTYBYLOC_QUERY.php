<?php

include 'search_creds.php';
include '../app/init.php';

$fromloc = $_POST['fromloc'];
$fromgroup = $_POST['fromgroup'];
$item = $_POST['item'];
$site_id = $_SESSION['site_id'];
$site_id2 = $_SESSION['site_id'];
$site_id3 = $_SESSION['site_id'];

// TIME TO WRITE QUERY HERE.

if (isset($item) && isset($fromloc) && isset($fromgroup)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare("SELECT isnull((select iob.onhand_qty FROM mod43fordpoc.dbo.inv_item_onhand_balances iob
	    	WHERE iob.item_id = (select item_id from mod43fordpoc.dbo.inv_item_master where item_name = :item and site_id = :site_id)
	    	AND iob.group_id = (select group_id from mod43fordpoc.dbo.inv_groups where group_name = :fromgroup and site_id = :site_id2)
	    	AND iob.location_id = (select location_id from mod43fordpoc.dbo.inv_locations where location_name = :fromloc and site_id = :site_id3 and group_id = iob.group_id)), 0) as onhand_qty
	    	");
	   	$stmt->bindParam(':fromloc', $fromloc);
	   	$stmt->bindParam(':fromgroup', $fromgroup);
	    $stmt->bindParam(':item', $item);
			$stmt->bindParam(':site_id', $site_id);
			$stmt->bindParam(':site_id2', $site_id2);
			$stmt->bindParam(':site_id3', $site_id3);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['onhand_qty'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>
