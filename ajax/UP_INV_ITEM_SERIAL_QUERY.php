<?php



include 'search_creds.php';
include '../app/init.php';

$item = $_GET['item'];
$from_group = $_GET['from_group'];
$from_loc = $_GET['from_loc'];




    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$site_id = $_SESSION['site_id'];




if (isset($_GET['term'])){

  $return_arr = array();
  $term = $_GET['term'].'%';




  try {
      $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $conn->prepare("SELECT serial_number FROM inv_item_serials
                              WHERE item_id = (SELECT item_id FROM inv_item_master WHERE item_name = :item AND site_id = :site_id) 
                              AND group_id = (SELECT group_id FROM inv_groups WHERE group_name = :from_group AND site_id = :site_id) 
                              AND location_id = (SELECT location_id FROM inv_locations WHERE location_name = :from_loc AND site_id = :site_id)
                              AND status = 'Active'
                              AND serial_number LIKE :term");
      $stmt->bindParam(':term', $term);
      $stmt->bindParam(':item', $item);
      $stmt->bindParam(':from_group', $from_group);
      $stmt->bindParam(':from_loc', $from_loc);
      $stmt->bindParam(':site_id', $site_id);

      //$stmt->execute(array('term' => $_GET['term'].'%'));
      $stmt->execute();

      while($row = $stmt->fetch()) {
          $return_arr[] =  $row['serial_number'];
      }

  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}







?>
