<?php

include 'search_creds.php';
include '../app/init.php';



$wo_line_id = $_POST['wo_line_id'];
$wo_header_number = $_POST['order'];

$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];

// TIME TO WRITE QUERY HERE.

//echo $wo_header_number." - ".$wo_line_id." - ".$from_group." - ".$from_location." - ".$item." - ".$qty." - ".$site_id;


if (isset($wo_line_id)){


	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('CALL proc_wo_line_backorder(:wo_line_id, :user_id, :site_id, @wo_line_bo_status)');
	    $stmt->bindParam(':wo_line_id', $wo_line_id);
	    $stmt->bindParam(':user_id', $user_id);
	    $stmt->bindParam(':site_id', $site_id);
		$stmt->execute();
		$stmt->closeCursor();

		$r = $conn->query('SELECT @wo_line_bo_status AS wo_line_bo_status')->fetch(PDO::FETCH_ASSOC);


	    
	    $edit_status = $r['wo_line_bo_status'];

	    //echo $edit_status;

	    if ($edit_status == 'Success!'){
	    	header("Location: ../UP_WO_PICK_ORDER_VIEW.php?wo_header_number=".$wo_header_number);
	    }

    	


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
 } 


?>