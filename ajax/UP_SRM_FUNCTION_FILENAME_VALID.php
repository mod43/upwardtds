<?php

include 'search_creds.php';

$filename = $_POST['filename'];

// TIME TO WRITE QUERY HERE.

if (isset($filename)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as filename_count FROM mod43fordpoc.dbo.srm_functions WHERE filename LIKE :filename');
	    $stmt->bindParam(':filename', $filename);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['filename_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>