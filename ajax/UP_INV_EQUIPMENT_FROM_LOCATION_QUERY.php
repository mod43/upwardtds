<?php

include 'search_creds.php';
include '../app/init.php';

$from_group = $_GET['from_group'];

    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$site_id = $_SESSION['site_id'];
$equipment = $_GET['equipment'];


if (isset($_GET['term'])){
	$return_arr = array();
  $term = $_GET['term'].'%';

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT location_name FROM mod43fordpoc.dbo.inv_locations WHERE location_name LIKE :term
                              and group_id = (select group_id from mod43fordpoc.dbo.inv_groups where group_name = :from_group and site_id = :site_id) 
                              and location_id in (select distinct location_id from mod43fordpoc.dbo.inv_equipment_onhand_balances 
                              where equipment_id = (select equipment_id from mod43fordpoc.dbo.inv_equipment_master where equipment_name = :equipment))');
      $stmt->bindParam(':term', $term);
      $stmt->bindParam(':from_group', $from_group);
      $stmt->bindParam(':site_id', $site_id);
      $stmt->bindParam(':equipment', $equipment);

      //$stmt->execute(array('term' => $_GET['term'].'%'));
      $stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['location_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}







?>
