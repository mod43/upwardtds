<?php

include 'search_creds.php';
include '../app/init.php';
/*
    require("config.php");
    if(empty($_SESSION['user_id'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }
*/
if (isset($_GET['term'])){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT uom_name FROM mod43fordpoc.dbo.inv_units_of_measure WHERE uom_name LIKE :term');
	    $stmt->execute(array('term' => $_GET['term'].'%'));
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['uom_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr); 

}


?>