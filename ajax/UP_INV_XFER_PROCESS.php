<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$from_group = $_POST['from_group'];
$from_location = $_POST['from_location'];
$to_group = $_POST['to_group'];
$to_location = $_POST['to_location'];
$quantity = $_POST['quantity'];
$comment = $_POST['comment'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
// TIME TO WRITE QUERY HERE.



if (isset($item)){


	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	  	$stmt = $conn->prepare('EXEC proc_inv_xfer_txn :item, :from_group, :from_location, :to_group, :to_location, :quantity, :comment, :site_id, :user_id'); //, @xfer_status
	  	$stmt->bindParam(':item', $item);
		$stmt->bindParam(':from_group', $from_group);
		$stmt->bindParam(':from_location', $from_location);
		$stmt->bindParam(':to_group', $to_group);
		$stmt->bindParam(':to_location', $to_location);
		$stmt->bindParam(':quantity', $quantity);
		$stmt->bindParam(':comment', $comment);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->bindParam(':user_id', $user_id);
		$stmt->execute();

		header('Location: ../UP_INV_XFER_TXN_SERIAL.php?message=success');


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}




?>
