<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$site_id = $_POST['site_id'];

// TIME TO WRITE QUERY HERE.

if (isset($item)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare("SELECT isnull((select sum(onhand_qty) FROM inv_item_onhand_balances 
	    	WHERE item_id = (select item_id from inv_item_master where item_name = :item and site_id = :site_id)), 0) 
	    	and site_id = :site_id as onhand_qty");
	    $stmt->bindParam(':item', $item);
	    $stmt->bindParam(':site_id', $site_id);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['onhand_qty'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>