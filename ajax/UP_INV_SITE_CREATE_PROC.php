<?php

include 'search_creds.php';
include '../app/init.php';

$site = $_POST['site'];
$user = $_SESSION['user_id'];



if (isset($site)){


  try {
      $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_site_create :site, :user');
      $stmt->bindParam(':site', $site);
      $stmt->bindParam(':user', $user);
      $stmt->execute();

      $stmt2 = $conn->prepare("SELECT COUNT(*) from mod43fordpoc.dbo.inv_sites where site_name = :site");
      $stmt2->bindParam(':site', $site);
      $stmt2->execute();
      $row = $stmt2->fetchcolumn();
        
      if ($row >= 1){
        header("Location: ../UP_INV_SITE_CREATE.php?message=Success");
      }
      else{header("Location: ../UP_SITE_SITE_CREATE.php?message=Error");
      }






  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>
