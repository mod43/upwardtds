<?php

include 'search_creds.php';
include '../app/init.php';

$from_group = $_GET['from_group'];

    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$site_id = $_SESSION['site_id'];
//$site_id2 = -999;//$_SESSION['site_id'];
//$term = 'P1-1-1';
//$group_name = 'PARTS';


if (isset($_GET['term'])){
	$return_arr = array();
  $term = $_GET['term'].'%';

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('select il.location_name from mod43fordpoc.dbo.inv_locations il, mod43fordpoc.dbo.inv_groups ig 
      where il.location_name LIKE :term and il.group_id = ig.group_id and ig.group_name = :from_group and il.site_id = ig.site_id and ig.site_id = :site_id');
      $stmt->bindParam(':term', $term);
      $stmt->bindParam(':from_group', $from_group);
      $stmt->bindParam(':site_id', $site_id);

      //$stmt->execute(array('term' => $_GET['term'].'%'));
      $stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['location_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}







?>
