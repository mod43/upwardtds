<?php

include 'search_creds.php';

include '../app/init.php';

$item = $_POST['item'];
$group = $_POST['group'];
$location = $_POST['location'];
$quantity = $_POST['quantity'];
$comment = $_POST['comment'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];

// TIME TO WRITE QUERY HERE.



if (isset($item)){


	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare("EXEC mod43fordpoc.dbo.proc_inv_issue_txn :item, :group, :location, :quantity, :comment, :site_id, :user_id");
	    $stmt->bindParam(':item', $item);
			$stmt->bindParam(':group', $group);
			$stmt->bindParam(':location', $location);
			$stmt->bindParam(':quantity', $quantity);
			$stmt->bindParam(':comment', $comment);
			$stmt->bindParam(':site_id', $site_id);
			$stmt->bindParam(':user_id', $user_id);
			$stmt->execute();

			header('Location: ../UP_INV_ITEM_ISSUE_SERIAL.php?message=success');


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}




?>
