<?php

include 'search_creds.php';
include '../app/init.php';

$item_id = $_POST['item'];
$minqty = $_POST['minqty'];
$maxqty = $_POST['maxqty'];
$ordermultiple = $_POST['ordermultiple'];
$site_id = $_SESSION['site_id'];


// TIME TO WRITE QUERY HERE.

if (isset($item_id)){


		$create_plan = $database->table('inv_reorder_planning')->insert([
										                'item_id' => $item_id,
										                'site_id' => $site_id,
										                'minimum_quantity' => $minqty,
										                'maximum_quantity' => $maxqty,
										                'order_multiple_quantity' => $ordermultiple
										                ]);



		if ($create_plan){
			header("Location: ../UP_INV_REORDER_CREATE.php?message=success");
	    } else {
	    	header("Location: ../UP_INV_REORDER_CREATE.php?message=error");
		}
}

?>
