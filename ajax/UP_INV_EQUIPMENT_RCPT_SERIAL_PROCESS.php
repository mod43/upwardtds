<?php

include 'search_creds.php';
include '../app/init.php';

$equipment = $_POST['equipment'];
$group = $_POST['group'];
$location = $_POST['location'];
$quantity = $_POST['quantity'];
$serials = $_POST['serial'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];

$serial_count = count($serials);


if ($serial_count == $quantity){

$loop_count = 0;

	while ($loop_count < $serial_count){

		$cur_serial = $serials[$loop_count];


			try {
				    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    
				    $stmt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_equipment_rcpt_serial_txn :equipment, :group, :location, :serial, :site_id, :user_id');
				    $stmt->bindParam(':equipment', $equipment);
					$stmt->bindParam(':group', $group);
					$stmt->bindParam(':location', $location);
					$stmt->bindParam(':serial', $cur_serial);
					$stmt->bindParam(':site_id', $site_id);
					$stmt->bindParam(':user_id', $user_id);
					$stmt->execute();


				} catch(PDOException $e) {
				    echo 'ERROR: ' . $e->getMessage();
				}

		$loop_count++;		
	}

	 header("Location: ../UP_INV_EQUIPMENT_RCPT_SERIAL.php?message=success");

} else {
	echo 'Serial quantity does not match given quantity';
}


?>