<?php

include 'search_creds.php';

$func = $_POST['func'];

// TIME TO WRITE QUERY HERE.

if (isset($func)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as func_count FROM mod43fordpoc.dbo.srm_functions WHERE function_name LIKE :func');
	    $stmt->bindParam(':func', $func);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['func_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>