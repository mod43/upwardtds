<?php

include 'search_creds.php';


// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare("SELECT menu_name, menu_header_id FROM mod43fordpoc.dbo.srm_menu_header WHERE menu_type = 'M' ");
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[$row['menu_header_id']] =  $row['menu_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>