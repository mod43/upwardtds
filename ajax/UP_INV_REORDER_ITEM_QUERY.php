<?php

include 'search_creds.php';
include '../app/init.php';

$site_id = $_SESSION['site_id'];

// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare("SELECT item_name, item_id FROM inv_item_master WHERE planning_enabled = 'Y' AND status = 'Active' AND site_id = {$site_id} AND item_id NOT IN (select item_id from inv_reorder_planning where site_id = {$site_id}) ");
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[$row['item_id']] =  $row['item_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>
