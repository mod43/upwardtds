<?php


include 'search_creds.php';
include '../app/init.php';


    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$site_id = $_SESSION['site_id'];


if (isset($_GET['term'])){
	$return_arr = array();
  $term = $_GET['term'].'%';

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT group_name FROM mod43fordpoc.dbo.inv_groups WHERE group_name LIKE :term and site_id = :site_id');
      $stmt->bindParam(':term', $term);
      $stmt->bindParam(':site_id', $site_id);
      //$stmt->execute(array('term' => $_GET['term'].'%'));
      $stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['group_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}


?>
