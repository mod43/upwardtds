<?php


include 'search_creds.php';
include '../app/init.php';


    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$serial = '12345678';//$_POST['serial'];
$equipment_id = 24;//$_POST['equipment_id'];
$site_id = 9;//$_SESSION['site_id'];


if (isset($equipment_id)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT serial_id FROM mod43fordpoc.dbo.inv_equipment_serials WHERE equipment_id = :equipment_id and site_id = :site_id and serial_number = :serial_number');
      	$stmt->bindParam(':equipment_id', $equipment_id);
      	$stmt->bindParam(':serial_number', $serial);
      	$stmt->bindParam(':site_id', $site_id);
      	$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['serial_id'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo $return_arr[0];
}


?>
