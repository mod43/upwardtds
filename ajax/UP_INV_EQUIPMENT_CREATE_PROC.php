<?php

/*    require("config.php");
    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }
*/
include 'search_creds.php';
include '../app/init.php';

$equipment = $_POST['equipment'];
$equip_desc = $_POST['equip_desc'];
$status = $_POST['status'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
$serial = $_POST['serial_enabled'];
$calibration = $_POST['calibration'];



if ($serial == 1){
  $serial_char = 'Y';
} else {
  $serial_char = 'N';
}

if ($calibration == 1){
  $calibration_char = 'Y';
} else {
  $calibration_char = 'N';
}



$conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$stmtcrt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_equipment_create :equipment_name, :equipment_description, :status, :serial, :calibration_required, :user_id, :site_id');
$stmtcrt->bindParam(':equipment_name', $equipment);
$stmtcrt->bindParam(':equipment_description', $equip_desc);
$stmtcrt->bindParam(':status', $status);
$stmtcrt->bindParam(':serial', $serial_char);
$stmtcrt->bindParam(':calibration_required', $calibration_char);
$stmtcrt->bindParam(':user_id', $user_id);
$stmtcrt->bindParam(':site_id', $site_id);
$stmtcrt->execute();


header("Location: ../UP_INV_EQUIPMENT_CREATE.php?message=success");





?>
