<?php

include 'search_creds.php';

$site = $_POST['site'];

// TIME TO WRITE QUERY HERE.

if (isset($site)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT count(*) as site_count FROM mod43fordpoc.dbo.inv_sites WHERE site_name LIKE :site');
	    $stmt->bindParam(':site', $site);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['site_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>
