<?php

include 'search_creds.php';
include '../app/init.php';

$equipment = $_GET['equipment'];
$site_id = $_SESSION['site_id'];
$site_id2 = $_SESSION['site_id'];



// TIME TO WRITE QUERY HERE.

if (isset($_GET['term'])){
	$return_arr = array();
  $term = $_GET['term'].'%';


	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT ies.serial_number
								from mod43fordpoc.dbo.inv_equipment_serials ies 
								where ies.site_id = :site_id 
								and ies.equipment_id = (select equipment_id from mod43fordpoc.dbo.inv_equipment_master where equipment_name = :equipment and site_id = :site_id2) 
								and ies.serial_number like :term
								and ies.serial_id not in (select iec.serial_id from mod43fordpoc.dbo.inv_equipment_calibration iec where iec.equipment_id = ies.equipment_id and iec.site_id = ies.site_id)');
	    
	    $stmt->bindParam(':equipment', $equipment);
	    $stmt->bindParam(':term', $term);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->bindParam(':site_id2', $site_id2);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['serial_number'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo json_encode($return_arr);
}



?>
