<?php

include 'search_creds.php';


// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare("select sp.person_id, CONCAT(sp.last_name, ', ', sp.first_name) as name from mod43fordpoc.dbo.srm_persons sp
								where sp.person_id not in (select distinct su.person_id from mod43fordpoc.dbo.srm_users su where person_id is not null)
								");
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[$row['person_id']] =  $row['name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>