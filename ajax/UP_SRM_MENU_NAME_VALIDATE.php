<?php

include 'search_creds.php';

$menu = $_POST['menu'];

// TIME TO WRITE QUERY HERE.

if (isset($menu)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD); //".ODBC_NAME." 'odbc:mod43fordpoc'
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as menu_count FROM mod43fordpoc.dbo.srm_menu_header WHERE menu_name LIKE :menu');
	    $stmt->bindParam(':menu', $menu);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['menu_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
    
}



?>