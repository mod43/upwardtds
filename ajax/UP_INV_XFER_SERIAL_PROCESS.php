<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$from_group = $_POST['from_group'];
$from_location = $_POST['from_location'];
$to_group = $_POST['to_group'];
$to_location = $_POST['to_location'];
$quantity = $_POST['quantity'];
$serials = $_POST['serial'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
$comment = $_POST['comment'];

$serial_count = count($serials);

/*echo "Inventory XFER Serial Debug<br>";
echo "-----------------------------------<br>";
echo "Item: ".$item."<br>";
echo "From Group: ".$from_group."<br>";
echo "From Location: ".$from_location."<br>";
echo "To Group: ".$to_group."<br>";
echo "To Location: ".$to_location."<br>";
echo "Quantity: ".$quantity."<br>";
echo "Site_id: ".$site_id."<br>";
echo "User_id: ".$user_id."<br>";

echo "Entering Serial Loop:<br>";
echo "-----------------------------------<br>";*/


if ($serial_count == $quantity){

$loop_count = 0;

	while ($loop_count < $serial_count){

		$cur_serial = $serials[$loop_count];
		$cur_quantity = 1;
		//echo $loop_count." - ".$cur_serial."<br>";

			try {
				    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    
				    $stmt = $conn->prepare('EXEC proc_inv_xfer_serial_txn :item, :from_group, :from_location, :to_group, :to_location, :quantity, :comment, :site_id, :user_id, :serial');
				    $stmt->bindParam(':item', $item);
					$stmt->bindParam(':from_group', $from_group);
					$stmt->bindParam(':from_location', $from_location);
					$stmt->bindParam(':to_group', $to_group);
					$stmt->bindParam(':to_location', $to_location);
					$stmt->bindParam(':quantity', $cur_quantity);
					$stmt->bindParam(':serial', $cur_serial);
					$stmt->bindParam(':site_id', $site_id);
					$stmt->bindParam(':user_id', $user_id);
					$stmt->bindParam(':comment', $comment);
					$stmt->execute();


				} catch(PDOException $e) {
				    echo 'ERROR: ' . $e->getMessage();
				}
				

		$loop_count++;		
	}

	 header("Location: ../UP_INV_XFER_TXN_SERIAL.php?message=success");

} else {
	echo 'Serial quantity does not match given quantity';
}


?>