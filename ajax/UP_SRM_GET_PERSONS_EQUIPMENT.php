<?php

include 'search_creds.php';


// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare("select sp.person_id, CONCAT(sp.last_name, ', ', sp.first_name) as name from srm_persons sp");
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[$row['person_id']] =  $row['name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>