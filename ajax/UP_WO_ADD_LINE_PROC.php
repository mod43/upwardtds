<?php

include 'search_creds.php';
include '../app/init.php';



$item = $_POST['item'];
$wo_header_number = $_POST['wo_header_number'];
$qty = $_POST['quantity'];
$site_id = 1;

// TIME TO WRITE QUERY HERE.


if (isset($item)){


	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('CALL proc_wo_line_create(:item, :wo_header_number, :qty, :site_id, @wo_add_status, @wo_header_number)');
	    $stmt->bindParam(':item', $item);
		$stmt->bindParam(':wo_header_number', $wo_header_number);
		$stmt->bindParam(':qty', $qty);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->execute();
		$stmt->closeCursor();

		$r = $conn->query('SELECT @wo_add_status AS wo_add_status, @wo_header_number as wo_header_number')->fetch(PDO::FETCH_ASSOC);
	    
	    $add_status = $r['wo_add_status'];
	    $header_number = $r['wo_header_number'];

	    if ($add_status == 'Success!'){
	    	header("Location: ../UP_WO_LINES_CREATE.php?wo_header_number=".$wo_header_number);
	    }

    	


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>