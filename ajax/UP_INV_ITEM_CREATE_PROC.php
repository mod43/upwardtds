<?php

/*require("config.php");
    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }
*/
include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$item_desc = $_POST['item_desc'];
$uom_code = $_POST['uom_code'];
$status = $_POST['status'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
$serial = $_POST['serial_enabled'];
$reorder = $_POST['reorder_planned'];
$consumable = $_POST['consumable'];



if ($serial == 1){
  $serial_char = 'Y';
} else {
  $serial_char = 'N';
}



if ($reorder == 1){
  $reorder_char = 'Y';
} else {
  $reorder_char= 'N';
}



if ($consumable == 1){
  $consumable_char = 'Y';
} else {
  $consumable_char = 'N';
}



$conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
$stmtcrt = $conn->prepare("EXEC mod43fordpoc.dbo.proc_inv_item_create :item_name, :item_description, :uom_code, :status, :serial, :reorder, :consumable, :user_id, :site_id");
$stmtcrt->bindParam(':item_name', $item);
$stmtcrt->bindParam(':item_description', $item_desc);
$stmtcrt->bindParam(':uom_code', $uom_code);
$stmtcrt->bindParam(':status', $status);
$stmtcrt->bindParam(':serial', $serial_char);
$stmtcrt->bindParam(':reorder', $reorder_char);
$stmtcrt->bindParam(':consumable', $consumable_char);
$stmtcrt->bindParam(':user_id', $user_id);
$stmtcrt->bindParam(':site_id', $site_id);
$stmtcrt->execute();


header("Location: ../UP_INV_ITEM_CREATE.php?message=success");





?>
