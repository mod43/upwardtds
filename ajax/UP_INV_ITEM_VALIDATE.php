<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$site_id = $_SESSION['site_id'];

// TIME TO WRITE QUERY HERE.

if (isset($item)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT count(*) as item_count FROM mod43fordpoc.dbo.inv_item_master WHERE item_name LIKE :item and site_id = :site_id');
	    $stmt->bindParam(':item', $item);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['item_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>
