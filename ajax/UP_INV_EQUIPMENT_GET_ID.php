<?php


include 'search_creds.php';
include '../app/init.php';


    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$equipment = $_POST['equipment'];
$site_id = $_SESSION['site_id'];


if (isset($equipment)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT equipment_id FROM mod43fordpoc.dbo.inv_equipment_master WHERE equipment_name = :equipment and site_id = :site_id');
      $stmt->bindParam(':equipment', $equipment);
      $stmt->bindParam(':site_id', $site_id);
      $stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['equipment_id'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo $return_arr[0];
}


?>
