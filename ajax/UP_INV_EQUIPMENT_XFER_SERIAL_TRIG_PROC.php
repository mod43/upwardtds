<?php

include 'search_creds.php';
include '../app/init.php';

$equipment = $_POST['equipment'];
$from_group = $_POST['from_group'];
$from_location = $_POST['from_location'];
$to_group = $_POST['to_group'];
$to_location = $_POST['to_location'];
$quantity = $_POST['quantity'];
$serial = $_POST['serial'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];



echo "Equipment XFER Serial Debug<br>";
echo "-----------------------------------<br>";
echo "equipment: ".$equipment."<br>";
echo "From Group: ".$from_group."<br>";
echo "From Location: ".$from_location."<br>";
echo "To Group: ".$to_group."<br>";
echo "To Location: ".$to_location."<br>";
echo "Quantity: ".$quantity."<br>";
echo "Site_id: ".$site_id."<br>";
echo "User_id: ".$user_id."<br>";

echo "Entering Serial Loop:<br>";
echo "-----------------------------------<br>";
echo "Serial: ".$serial."<br>";

if (1==1){

			try {
				    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    
				    $stmt = $conn->prepare('CALL proc_inv_equipment_xfer_serial_txn(:equipment, :from_group, :from_location, :to_group, :to_location, :quantity, :site_id, :user_id, :serial)');
				    $stmt->bindParam(':equipment', $equipment);
					$stmt->bindParam(':from_group', $from_group);
					$stmt->bindParam(':from_location', $from_location);
					$stmt->bindParam(':to_group', $to_group);
					$stmt->bindParam(':to_location', $to_location);
					$stmt->bindParam(':quantity', $quantity);
					$stmt->bindParam(':serial', $serial);
					$stmt->bindParam(':site_id', $site_id);
					$stmt->bindParam(':user_id', $user_id);
					$stmt->execute();


				} catch(PDOException $e) {
				    echo 'ERROR: ' . $e->getMessage();
				}
				

	


	 header("Location: ../UP_INV_EQUIPMENT_XFER_TXN_SERIAL_TRIGGERED.php?message=success");

} else {
	echo 'Serial quantity does not match given quantity';
}


?>