<?php


include 'search_creds.php';
include '../app/init.php';


    if(empty($_SESSION['user_id']))
    {
        header("Location: index.php");
        die("Redirecting to index.php");
    }

$serial = $_POST['serial'];
$site_id = $_SESSION['site_id'];


if (isset($serial)){
	$return_arr = array();

	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare("SELECT `IEM`.`EQUIPMENT_NAME` as equipment, `IG`.`GROUP_NAME` as from_group, `IL`.`LOCATION_NAME` as from_location
								FROM inv_equipment_serials IES,
								inv_equipment_master IEM,
								inv_groups IG,
								inv_locations IL
								WHERE `IES`.`SERIAL_NUMBER` = :serial_number
								AND `IES`.`GROUP_ID` = `IG`.`GROUP_ID`
								AND `IES`.`SITE_ID` = `IG`.`SITE_ID`
								AND `IES`.`LOCATION_ID` = `IL`.`LOCATION_ID`
								AND `IES`.`SITE_ID` = `IL`.`SITE_ID`
								AND `IES`.`EQUIPMENT_ID` = `IEM`.`EQUIPMENT_ID`
								AND `IES`.`SITE_ID` = `IEM`.`SITE_ID`
								AND `IES`.`SITE_ID` = :site_id
								AND `IES`.`STATUS` = 'Active'");
      	$stmt->bindParam(':serial_number', $serial);
      	$stmt->bindParam(':site_id', $site_id);
      	$stmt->execute();

	    $return_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}


?>
