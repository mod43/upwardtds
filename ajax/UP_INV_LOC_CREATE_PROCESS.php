<?php

include 'search_creds.php';
include '../app/init.php';

$location = $_POST['location'];
$group = $_POST['group'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];

if (isset($group)){


  try {
      $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $conn->prepare("EXEC mod43fordpoc.dbo.proc_inv_location_create @in_location=:location, @in_group_name=:group, @in_site_id=:site_id, @in_user_id=:user_id");  //, :user_id, @location_create_status
      $stmt->bindParam(':location', $location);
      $stmt->bindParam(':group', $group);
      $stmt->bindParam(':site_id', $site_id);
      $stmt->bindParam(':user_id', $user_id);
      $stmt->execute();
      $stmt->closeCursor();

      $stmt2 = $conn->prepare("SELECT COUNT(*) from mod43fordpoc.dbo.inv_locations where location_name = :location");
      $stmt2->bindParam(':location', $location);
      $stmt2->execute();
      $row = $stmt2->fetchcolumn();
        
      if ($row >= 1){
        header("Location: ../UP_INV_LOC_CREATE.php?message=Success");
      }
      else{header("Location: ../UP_INV_LOC_CREATE.php?message=Error");
      }




  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}
