<?php

include 'search_creds.php';
include '../app/init.php';

$item = $_POST['item'];
$group = $_POST['group'];
$location = $_POST['location'];
$quantity = $_POST['quantity'];
$comment = $_POST['comment'];
$serials = $_POST['serial'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];

$serial_count = count($serials);

/*echo "Inventory Issue Debug<br>";
echo "-----------------------------------<br>";
echo "Item: ".$item."<br>";
echo "Group: ".$group."<br>";
echo "Location: ".$location."<br>";
echo "Quantity: ".$quantity."<br>";
echo "Site_id: ".$site_id."<br>";
echo "User_id: ".$user_id."<br>";

echo "Entering Serial Loop:<br>";
echo "-----------------------------------<br>";*/


if ($serial_count == $quantity){

$loop_count = 0;

	while ($loop_count < $serial_count){



		$cur_serial = $serials[$loop_count];

		//echo $loop_count." - ".$cur_serial."<br>";

			
			try {
				    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    
				    $stmt = $conn->prepare('EXEC mod43fordpoc.dbo.proc_inv_issue_serial_txn :item, :group, :location, :comment, :serial, :site_id, :user_id');
				    $stmt->bindParam(':item', $item);
					$stmt->bindParam(':group', $group);
					$stmt->bindParam(':location', $location);
					$stmt->bindParam(':comment', $comment);
					$stmt->bindParam(':serial', $cur_serial);
					$stmt->bindParam(':site_id', $site_id);
					$stmt->bindParam(':user_id', $user_id);
					$stmt->execute();


				} catch(PDOException $e) {
				    echo 'ERROR: ' . $e->getMessage();
				}
				


		$loop_count++;		
	}

	 header("Location: ../UP_INV_ITEM_ISSUE_SERIAL.php?message=success");

} else {
	echo 'Serial quantity does not match given quantity';
}


?>