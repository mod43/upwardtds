<?php

include 'search_creds.php';

$shortcode = $_POST['shortcode'];

// TIME TO WRITE QUERY HERE.

if (isset($shortcode)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as shortcode_count FROM srm_menu_header WHERE menu_shortcode LIKE :shortcode');
	    $stmt->bindParam(':shortcode', $shortcode);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr[] =  $row['shortcode_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>