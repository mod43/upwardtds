<?php

include 'search_creds.php';
include '../app/init.php';

$equipment = $_POST['equipment'];
$serial = $_POST['serial'];
$notes = $_POST['notes'];
$equipment_id = $_POST['equipment_id'];
$serial_id = $_POST['serial_id'];
$site_id = $_SESSION['site_id'];
$user_id = $_SESSION['user_id'];
$date = date("Y-m-d H:i:s");


// TIME TO WRITE QUERY HERE.

if (isset($equipment_id)){


		$create_calibration = $database->table('mod43fordpoc.dbo.inv_equipment_calibration_history')->insert(array(
										                'equipment_id' => $equipment_id,
										                'site_id' => $site_id,
										                'serial_id' => $serial_id,
										                'calibration_user' => $user_id,
										                'calibration_date' => $date,
										                'calibration_notes' => $notes
										                ));


		
		if ($create_calibration){
			header("Location: ../UP_INV_EQUIPMENT_CALIBRATION_TXN.php?message=success");
	    } else {
	    	header("Location: ../UP_INV_EQUIPMENT_CALIBRATION_TXN.php?message=error");
		}
		
}

?>
