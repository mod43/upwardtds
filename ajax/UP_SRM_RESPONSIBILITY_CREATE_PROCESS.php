<?php

include 'search_creds.php';
include '../app/init.php';

date_default_timezone_set('America/New_York');

$resp = $_POST['resp'];
$shortcode = $_POST['shortcode'];
$root_menu = $_POST['root_menu'];
$site = $_POST['site'];


// TIME TO WRITE QUERY HERE.


if (isset($resp)){


		$created_responsibility = $database->table('srm_responsibility')->insert(array(
										                'responsibility_name' => $resp,
										                'responsibility_shortcode' => $shortcode,
										                'responsibility_root_menu_id' => $root_menu
										                ));

		if ($created_responsibility){



			$new_resp = $database->table('srm_responsibility')->where('responsibility_shortcode','=', $shortcode)->get();

			$responsibility_id = $new_resp[0]['responsibility_id'];

			$created_site_resp_assignment = $database->table('srm_resp_site_assignments')->insert(array(
										                'responsibility_id' => $responsibility_id,
										                'site_id' => $site,
										                'enabled_flag' => 1,
										                'created_by' => $_SESSION['user_id'],
										                'created_date' => date("Y-m-d H:i:s"),
										                'updated_by' => $_SESSION['user_id'],
										                'updated_date' => date("Y-m-d H:i:s"),
										                ));

		}

		if ($created_responsibility && $created_site_resp_assignment){
			header("Location: ../UP_SRM_RESPS_VIEW.php");
	    } else {
	    	header("Location: ../UP_SRM_RESPS_VIEW.php");
		}
}

?>
