<?php

include 'search_creds.php';

if(isset($_GET['user_id'])){
	$user_id = $_GET['user_id'];
}

// TIME TO WRITE QUERY HERE.


	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	    $stmt = $conn->prepare('SELECT responsibility_name, responsibility_id FROM mod43fordpoc.dbo.srm_responsibility WHERE responsibility_id NOT IN (SELECT distinct responsibility_id from mod43fordpoc.dbo.up_srm_user_resp_view where user_id = :user_id)');
			$stmt->bindParam(':user_id', $user_id);
			$stmt->execute();

	    while($row = $stmt->fetch()) {
	        $return_arr[$row['responsibility_id']] =  $row['responsibility_name'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);

    /* Send just the number */
    //echo $return_arr[0];




?>
