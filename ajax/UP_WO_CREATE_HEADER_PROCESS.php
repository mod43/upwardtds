<?php

include 'search_creds.php';
include '../app/init.php';

$name = $_POST['name'];
$location = $_POST['location'];
$scheduled_date = $_POST['scheduled_date'];
$priority= $_POST['priority'];
$site_id = 1;
$status = 1;

// TIME TO WRITE QUERY HERE.




if (isset($name)){


	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('CALL proc_wo_header_create(:name, :location, :scheduled_date, :priority, :status, :site_id, @wo_create_status, @wo_header_number)');
	    $stmt->bindParam(':name', $name);
		$stmt->bindParam(':location', $location);
		$stmt->bindParam(':scheduled_date', $scheduled_date);
		$stmt->bindParam(':priority', $priority);
		$stmt->bindParam(':site_id', $site_id);
		$stmt->bindParam(':status', $status);
		$stmt->execute();
		$stmt->closeCursor();

		$r = $conn->query('SELECT @wo_create_status AS wo_create_status, @wo_header_number as wo_header_number')->fetch(PDO::FETCH_ASSOC);
	    
	    $create_status = $r['wo_create_status'];
	    $header_number = $r['wo_header_number'];

	    if ($create_status == 'Success!'){
	    	header("Location: ../UP_WO_LINES_CREATE.php?wo_header_number=".$header_number);
	    }

    	


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>