<?php



include 'search_creds.php';
include '../app/init.php';

date_default_timezone_set('America/New_York');

$equipment = $_POST['equipment'];
$serial = $_POST['serial'];
$frequency = $_POST['frequency'];
$last_date = $_POST['initial_date'];
$last_user = $_POST['last_user'];
$equipment_id = $_POST['equipment_id'];
$serial_id = $_POST['serial_id'];
$site_id = $_SESSION['site_id'];

echo 'equipment: '.$equipment.'<br>';
echo 'serial: '.$serial.'<br>';
echo 'frequency: '.$frequency.'<br>';
echo 'last_date: '.$last_date.'<br>';
echo 'last_user: '.$last_user.'<br>';
echo 'equipment_id: '.$equipment_id.'<br>';
echo 'serial_id: '.$serial_id.'<br>';
echo 'site_id: '.$site_id.'<br>';


// TIME TO WRITE QUERY HERE.

if (isset($equipment_id)){


		$create_calibration = $database->table('inv_equipment_calibration')->insert([
										                'equipment_id' => $equipment_id,
										                'site_id' => $site_id,
										                'serial_id' => $serial_id,
										                'calibration_frequency' => $frequency,
										                'calibration_last_date' => $last_date,
										                'calibration_last_user' => $last_user
										                ]);



		if ($create_calibration){
			header("Location: ../UP_INV_EQUIPMENT_CALIBRATION_CREATE.php?message=success");
	    } else {
	    	header("Location: ../UP_INV_REORDER_CREATE.php?message=error");
		}
}

?>
