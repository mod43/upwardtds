<?php

include 'search_creds.php';
include '../app/init.php';


$wo_header_number = $_POST['wo_header_number'];
$wo_line_id = $_POST['wo_line_id'];
$item = $_POST['item'];
$qty = $_POST['quantity'];
$line_status = $_POST['line_status'];
$user_id = 3;

// TIME TO WRITE QUERY HERE.


if (isset($item)){


	try {
	    $conn = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('CALL proc_wo_line_edit(:wo_line_id, :item, :qty, :user_id, :line_status, @wo_line_edit_status)');
	    $stmt->bindParam(':wo_line_id', $wo_line_id);
	    $stmt->bindParam(':item', $item);
	    $stmt->bindParam(':qty', $qty);
	    $stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':line_status', $line_status);
		$stmt->execute();
		$stmt->closeCursor();

		$r = $conn->query('SELECT @wo_line_edit_status AS wo_line_edit_status')->fetch(PDO::FETCH_ASSOC);


	    
	    $edit_status = $r['wo_line_edit_status'];

	    if ($edit_status == 'Success!'){
	    	header("Location: ../UP_WO_LINES_CREATE.php?wo_header_number=".$wo_header_number);
	    }

    	


	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    //echo $r[0];
}


?>