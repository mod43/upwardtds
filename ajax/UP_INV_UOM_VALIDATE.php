<?php

include 'search_creds.php';

$uom = $_POST['uom'];

// TIME TO WRITE QUERY HERE.

if (isset($uom)){
	$return_arr = array();

	try {
	    $conn = new PDO(ODBC_NAME, DB_USER, DB_PASSWORD);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $stmt = $conn->prepare('SELECT count(*) as uom_count from mod43fordpoc.dbo.inv_units_of_measure WHERE uom_name = :uom');
	    $stmt->bindParam(':uom', $uom);
		$stmt->execute();
	    
	    while($row = $stmt->fetch()) {
	        $return_arr =  $row['uom_count'];
	    }

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}


    /* Toss back results as json encoded array. */
    //echo json_encode($return_arr);

    /* Send just the number */
    echo $return_arr[0];
}



?>