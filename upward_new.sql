-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               Microsoft SQL Server 2014 - 12.0.2269.0
-- Server OS:                    Windows NT 6.3 <X64> (Build 14393: )
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for mod43fordpoc
CREATE DATABASE IF NOT EXISTS "mod43fordpoc";
USE "mod43fordpoc";


-- Dumping structure for table mod43fordpoc.inv_equipment_calibration
CREATE TABLE IF NOT EXISTS "inv_equipment_calibration" (
	"calibration_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"equipment_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"serial_id" INT(10,0) NULL DEFAULT ((0)),
	"calibration_frequency" INT(10,0) NOT NULL DEFAULT ((-999)),
	"calibration_last_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"calibration_last_user" NVARCHAR(11) NOT NULL DEFAULT (N'UNKNOWN'),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999))
);

-- Dumping data for table mod43fordpoc.inv_equipment_calibration: 0 rows
DELETE FROM "inv_equipment_calibration";
/*!40000 ALTER TABLE "inv_equipment_calibration" DISABLE KEYS */;
/*!40000 ALTER TABLE "inv_equipment_calibration" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_equipment_calibration_history
CREATE TABLE IF NOT EXISTS "inv_equipment_calibration_history" (
	"calibration_history_id" INT(10,0) NOT NULL,
	"equipment_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"serial_id" INT(10,0) NULL DEFAULT ((0)),
	"calibration_notes" NVARCHAR(255) NOT NULL DEFAULT (N'None'),
	"calibration_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"calibration_user" INT(10,0) NOT NULL DEFAULT ((-999)),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("calibration_history_id")
);

-- Dumping data for table mod43fordpoc.inv_equipment_calibration_history: 0 rows
DELETE FROM "inv_equipment_calibration_history";
/*!40000 ALTER TABLE "inv_equipment_calibration_history" DISABLE KEYS */;
/*!40000 ALTER TABLE "inv_equipment_calibration_history" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_equipment_master
CREATE TABLE IF NOT EXISTS "inv_equipment_master" (
	"equipment_id" BIGINT(19,0) NOT NULL,
	"equipment_name" VARCHAR(30) NOT NULL DEFAULT (N'-999'),
	"equipment_description" VARCHAR(200) NOT NULL DEFAULT (N'-999'),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"status" VARCHAR(24) NULL DEFAULT (NULL),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"serial_enabled" NVARCHAR(1) NOT NULL DEFAULT (N'Y'),
	"calibration_required" NVARCHAR(1) NOT NULL DEFAULT (N'Y'),
	PRIMARY KEY ("equipment_id")
);

-- Dumping data for table mod43fordpoc.inv_equipment_master: 1 rows
DELETE FROM "inv_equipment_master";


-- Dumping structure for table mod43fordpoc.inv_equipment_onhand_balances
CREATE TABLE IF NOT EXISTS "inv_equipment_onhand_balances" (
	"onhand_id" BIGINT(19,0) NOT NULL,
	"equipment_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"group_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"location_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"onhand_qty" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("onhand_id")
);

-- Dumping data for table mod43fordpoc.inv_equipment_onhand_balances: 1 rows
DELETE FROM "inv_equipment_onhand_balances";



-- Dumping structure for table mod43fordpoc.inv_equipment_serials
CREATE TABLE IF NOT EXISTS "inv_equipment_serials" (
	"serial_id" INT(10,0) NOT NULL,
	"serial_number" NVARCHAR(128) NOT NULL,
	"equipment_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"group_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"location_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"status" NVARCHAR(128) NOT NULL DEFAULT (N'Active'),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	PRIMARY KEY ("serial_id")
);

-- Dumping data for table mod43fordpoc.inv_equipment_serials: 0 rows
DELETE FROM "inv_equipment_serials";
/*!40000 ALTER TABLE "inv_equipment_serials" DISABLE KEYS */;
/*!40000 ALTER TABLE "inv_equipment_serials" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_equipment_txn_history
CREATE TABLE IF NOT EXISTS "inv_equipment_txn_history" (
	"txn_id" BIGINT(19,0) NOT NULL,
	"txn_type_name" VARCHAR(64) NOT NULL DEFAULT (N''),
	"equipment_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"serial_number" NVARCHAR(128) NULL DEFAULT (NULL),
	"from_group_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"from_loc_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"to_group_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"to_loc_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"txn_qty" INT(10,0) NOT NULL,
	"site_id" BIGINT(19,0) NOT NULL,
	"created_by" BIGINT(19,0) NOT NULL,
	"created_date" DATETIME2(0) NOT NULL,
	"updated_by" BIGINT(19,0) NOT NULL,
	"updated_date" DATETIME2(0) NOT NULL,
	PRIMARY KEY ("txn_id")
);

-- Dumping data for table mod43fordpoc.inv_equipment_txn_history: 3 rows
DELETE FROM "inv_equipment_txn_history";



-- Dumping structure for table mod43fordpoc.inv_groups
CREATE TABLE IF NOT EXISTS "inv_groups" (
	"group_id" INT(10,0) NOT NULL,
	"group_name" NVARCHAR(50) NOT NULL,
	"site_id" INT(10,0) NOT NULL,
	"created_by" INT(10,0) NOT NULL,
	"created_date" DATETIME2(0) NOT NULL,
	"updated_by" INT(10,0) NOT NULL,
	"updated_date" DATETIME2(0) NOT NULL,
	PRIMARY KEY ("group_id")
);

-- Dumping data for table mod43fordpoc.inv_groups: 4 rows
DELETE FROM "inv_groups";



-- Dumping structure for table mod43fordpoc.inv_item_master
CREATE TABLE IF NOT EXISTS "inv_item_master" (
	"item_id" BIGINT(19,0) NOT NULL,
	"item_name" VARCHAR(20) NOT NULL DEFAULT (N'-999'),
	"item_description" NVARCHAR(150) NULL DEFAULT (NULL),
	"base_uom_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"item_picture" VARBINARY(max) NULL DEFAULT NULL,
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"status" VARCHAR(24) NULL DEFAULT (NULL),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"serial_enabled" NVARCHAR(1) NOT NULL DEFAULT (N'N'),
	"consumable" NVARCHAR(1) NOT NULL DEFAULT (N'N'),
	"planning_enabled" NVARCHAR(1) NOT NULL DEFAULT (N'N'),
	PRIMARY KEY ("item_id")
);

-- Dumping data for table mod43fordpoc.inv_item_master: 6 rows
DELETE FROM "inv_item_master";



-- Dumping structure for table mod43fordpoc.inv_item_onhand_balances
CREATE TABLE IF NOT EXISTS "inv_item_onhand_balances" (
	"onhand_id" BIGINT(19,0) NOT NULL,
	"item_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"group_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"location_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"onhand_qty" INT(10,0) NOT NULL DEFAULT ((-999)),
	"base_uom_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("onhand_id")
);

-- Dumping data for table mod43fordpoc.inv_item_onhand_balances: 4 rows
DELETE FROM "inv_item_onhand_balances";



-- Dumping structure for table mod43fordpoc.inv_item_serials
CREATE TABLE IF NOT EXISTS "inv_item_serials" (
	"serial_id" INT(10,0) NOT NULL,
	"serial_number" NVARCHAR(128) NOT NULL,
	"item_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"group_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"location_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"status" NVARCHAR(128) NOT NULL DEFAULT (N'Active'),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	PRIMARY KEY ("serial_id")
);

-- Dumping data for table mod43fordpoc.inv_item_serials: 0 rows
DELETE FROM "inv_item_serials";
/*!40000 ALTER TABLE "inv_item_serials" DISABLE KEYS */;
/*!40000 ALTER TABLE "inv_item_serials" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_locations
CREATE TABLE IF NOT EXISTS "inv_locations" (
	"location_id" BIGINT(19,0) NOT NULL,
	"location_name" VARCHAR(24) NOT NULL DEFAULT (N''),
	"group_id" BIGINT(19,0) NOT NULL,
	"site_id" BIGINT(19,0) NOT NULL,
	"created_by" BIGINT(19,0) NOT NULL,
	"created_date" DATETIME2(0) NOT NULL,
	"updated_by" BIGINT(19,0) NOT NULL,
	"updated_date" DATETIME2(0) NOT NULL,
	PRIMARY KEY ("location_id")
);

-- Dumping data for table mod43fordpoc.inv_locations: 3 rows
DELETE FROM "inv_locations";



-- Dumping structure for table mod43fordpoc.inv_reorder_planning
CREATE TABLE IF NOT EXISTS "inv_reorder_planning" (
	"plan_id" INT(10,0) NOT NULL,
	"item_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"minimum_quantity" INT(10,0) NOT NULL DEFAULT ((-999)),
	"maximum_quantity" INT(10,0) NOT NULL DEFAULT ((-999)),
	"order_multiple_quantity" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("plan_id")
);

-- Dumping data for table mod43fordpoc.inv_reorder_planning: 0 rows
DELETE FROM "inv_reorder_planning";
/*!40000 ALTER TABLE "inv_reorder_planning" DISABLE KEYS */;
/*!40000 ALTER TABLE "inv_reorder_planning" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_sites
CREATE TABLE IF NOT EXISTS "inv_sites" (
	"site_id" INT(10,0) NOT NULL,
	"site_name" NVARCHAR(50) NOT NULL,
	"enabled_flag" INT(10,0) NOT NULL,
	"created_by" INT(10,0) NOT NULL,
	"created_date" DATETIME2(0) NOT NULL,
	"updated_by" INT(10,0) NOT NULL,
	"updated_date" DATETIME2(0) NOT NULL,
	PRIMARY KEY ("site_id")
);

-- Dumping data for table mod43fordpoc.inv_sites: 1 rows
DELETE FROM "inv_sites";
/*!40000 ALTER TABLE "inv_sites" DISABLE KEYS */;
INSERT INTO "inv_sites" ("site_name", "enabled_flag", "created_by", "created_date", "updated_by", "updated_date") VALUES
	('MAIN', 1, 29, SYSDATETIME, 29, SYSDATETIME);
/*!40000 ALTER TABLE "inv_sites" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.inv_txn_history
CREATE TABLE IF NOT EXISTS "inv_txn_history" (
	"txn_id" BIGINT(19,0) NOT NULL,
	"txn_type_name" VARCHAR(64) NOT NULL DEFAULT (N''),
	"item_id" BIGINT(19,0) NOT NULL,
	"serial_number" NVARCHAR(128) NULL DEFAULT (NULL),
	"from_group_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"from_loc_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"to_group_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"to_loc_id" BIGINT(19,0) NULL DEFAULT (NULL),
	"source_wo_header_id" INT(10,0) NULL DEFAULT ((0)),
	"source_wo_header_number" NVARCHAR(128) NULL DEFAULT (NULL),
	"source_wo_line_id" INT(10,0) NULL DEFAULT ((0)),
	"txn_qty" INT(10,0) NOT NULL,
	"txn_uom_id" BIGINT(19,0) NOT NULL,
	"kiosk_id" NVARCHAR(128) NULL DEFAULT (N'-999'),
	"site_id" BIGINT(19,0) NOT NULL,
	"created_by" BIGINT(19,0) NOT NULL,
	"created_date" DATETIME2(0) NOT NULL,
	"updated_by" BIGINT(19,0) NOT NULL,
	"updated_date" DATETIME2(0) NOT NULL,
	"comment" NVARCHAR(100) NULL DEFAULT (NULL),
	PRIMARY KEY ("txn_id")
);

-- Dumping data for table mod43fordpoc.inv_txn_history: 8 rows
DELETE FROM "inv_txn_history";



-- Dumping structure for table mod43fordpoc.inv_units_of_measure
CREATE TABLE IF NOT EXISTS "inv_units_of_measure" (
	"uom_id" BIGINT(19,0) NOT NULL,
	"uom_name" VARCHAR(24) NOT NULL DEFAULT (N''),
	"uom_description" VARCHAR(64) NOT NULL DEFAULT (N''),
	"created_by" BIGINT(19,0) NOT NULL,
	"created_date" DATE(0) NOT NULL,
	"updated_by" BIGINT(19,0) NOT NULL,
	"updated_date" DATE(0) NOT NULL,
	PRIMARY KEY ("uom_id")
);

-- Dumping data for table mod43fordpoc.inv_units_of_measure: 1 rows
DELETE FROM "inv_units_of_measure";
/*!40000 ALTER TABLE "inv_units_of_measure" DISABLE KEYS */;
INSERT INTO "inv_units_of_measure" ("uom_name", "uom_description", "created_by", "created_date", "updated_by", "updated_date") VALUES
	('EA', 'Each', 1, SYSDATETIME, 1, SYSDATETIME);
/*!40000 ALTER TABLE "inv_units_of_measure" ENABLE KEYS */;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_equipment_create  
   @in_equipment nvarchar(30),
   @in_equipment_description nvarchar(200),
   @in_status nvarchar(20),
   @in_serial nvarchar(5),
   @in_calibration nvarchar(5),
   @in_user_id int,
   @in_site_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @crt_equipment_name nvarchar(20)

      DECLARE
         @crt_equipment_description nvarchar(50)

      DECLARE
         @crt_site_id int

      DECLARE
         @crt_created_date datetime2(0)

      DECLARE
         @crt_updated_date datetime2(0)

      DECLARE
         @crt_created_by int

      DECLARE
         @crt_updated_by int

      DECLARE
         @crt_status nvarchar(20)

      DECLARE
         @crt_serial nvarchar(5)

      DECLARE
         @crt_calibration nvarchar(5)

      SET @crt_equipment_name = @in_equipment

      SET @crt_equipment_description = @in_equipment_description

      SET @crt_status = @in_status

      SET @crt_site_id = @in_site_id

      SET @crt_created_date = getdate()

      SET @crt_updated_date = getdate()

      SET @crt_created_by = @in_user_id

      SET @crt_updated_by = @in_user_id

      SET @crt_serial = @in_serial

      SET @crt_calibration = @in_calibration

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.inv_equipment_master(
         dbo.inv_equipment_master.equipment_name, 
         dbo.inv_equipment_master.equipment_description, 
         dbo.inv_equipment_master.site_id, 
         dbo.inv_equipment_master.status, 
         dbo.inv_equipment_master.created_by, 
         dbo.inv_equipment_master.created_date, 
         dbo.inv_equipment_master.updated_by, 
         dbo.inv_equipment_master.updated_date, 
         dbo.inv_equipment_master.serial_enabled, 
         dbo.inv_equipment_master.calibration_required)
         VALUES (
            @crt_equipment_name, 
            @crt_equipment_description, 
            @crt_site_id, 
            @crt_status, 
            @crt_created_by, 
            isnull(@crt_created_date, getdate()), 
            @crt_updated_by, 
            isnull(@crt_updated_date, getdate()), 
            @in_serial, 
            @in_calibration)

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_issue_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_equipment_issue_txn  
   @in_equipment nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_quantity int,
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @iss_equipment_id int

      DECLARE
         @iss_group_id int

      DECLARE
         @iss_location_id int

      DECLARE
         @iss_quantity int

      DECLARE
         @iss_site_id int

      DECLARE
         @iss_created_date datetime2(0)

      DECLARE
         @iss_updated_date datetime2(0)

      DECLARE
         @iss_created_by int

      DECLARE
         @iss_updated_by int

      SET @iss_site_id = @in_site_id

      SET @iss_equipment_id = 
         (
            SELECT inv_equipment_master.equipment_id
            FROM dbo.inv_equipment_master
            WHERE inv_equipment_master.equipment_name = @in_equipment AND inv_equipment_master.site_id = @iss_site_id
         )

      SET @iss_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @iss_site_id
         )

      SET @iss_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @iss_group_id AND 
               inv_locations.site_id = @iss_site_id
         )

      SET @iss_quantity = @in_quantity

      SET @iss_created_date = getdate()

      SET @iss_updated_date = getdate()

      SET @iss_created_by = @in_user_id

      SET @iss_updated_by = @in_user_id

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_equipment_onhand_balances  AS iob1
            WHERE 
               iob1.equipment_id = @iss_equipment_id AND 
               iob1.group_id = @iss_group_id AND 
               iob1.location_id = @iss_location_id
         ) = 1
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_onhand_balances
               SET 
                  onhand_qty = (inv_equipment_onhand_balances.onhand_qty - @iss_quantity), 
                  updated_by = @iss_updated_by, 
                  updated_date = isnull(@iss_updated_date, getdate())
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @iss_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @iss_group_id AND 
               inv_equipment_onhand_balances.location_id = @iss_location_id AND 
               inv_equipment_onhand_balances.site_id = @iss_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.site_id, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date)
               VALUES (
                  N'EQUIPMENT_ISSUE', 
                  @iss_equipment_id, 
                  @iss_group_id, 
                  @iss_location_id, 
                  @iss_quantity, 
                  @iss_site_id, 
                  @iss_created_by, 
                  isnull(@iss_created_date, getdate()), 
                  @iss_updated_by, 
                  isnull(@iss_updated_date, getdate()))

         END
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         */

         INSERT dbo.inv_equipment_txn_history(
            dbo.inv_equipment_txn_history.txn_type_name, 
            dbo.inv_equipment_txn_history.equipment_id, 
            dbo.inv_equipment_txn_history.to_group_id, 
            dbo.inv_equipment_txn_history.to_loc_id, 
            dbo.inv_equipment_txn_history.txn_qty, 
            dbo.inv_equipment_txn_history.site_id, 
            dbo.inv_equipment_txn_history.created_by, 
            dbo.inv_equipment_txn_history.created_date, 
            dbo.inv_equipment_txn_history.updated_by, 
            dbo.inv_equipment_txn_history.updated_date)
            VALUES (
               N'EQUIPMENT_ISSUE - ERROR INSUFFICIENT QUANTITY', 
               @iss_equipment_id, 
               @iss_group_id, 
               @iss_location_id, 
               @iss_quantity, 
               @iss_site_id, 
               @iss_created_by, 
               isnull(@iss_created_date, getdate()), 
               @iss_updated_by, 
               isnull(@iss_updated_date, getdate()))

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_rcpt_serial_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_equipment_rcpt_serial_txn  
   @in_equipment nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_serial nvarchar(128),
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @rcv_equipment_id int

      DECLARE
         @rcv_group_id int

      DECLARE
         @rcv_location_id int

      DECLARE
         @rcv_serial nvarchar(128)

      DECLARE
         @rcv_site_id int

      DECLARE
         @rcv_created_date datetime2(0)

      DECLARE
         @rcv_updated_date datetime2(0)

      DECLARE
         @rcv_created_by int

      DECLARE
         @rcv_updated_by int

      SET @rcv_site_id = @in_site_id

      SET @rcv_equipment_id = 
         (
            SELECT inv_equipment_master.equipment_id
            FROM dbo.inv_equipment_master
            WHERE inv_equipment_master.equipment_name = @in_equipment AND inv_equipment_master.site_id = @rcv_site_id
         )

      SET @rcv_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @rcv_site_id
         )

      SET @rcv_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @rcv_group_id AND 
               inv_locations.site_id = @rcv_site_id
         )

      SET @rcv_serial = @in_serial

      SET @rcv_created_date = getdate()

      SET @rcv_updated_date = getdate()

      SET @rcv_created_by = @in_user_id

      SET @rcv_updated_by = @in_user_id

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_equipment_onhand_balances  AS iob1
            WHERE 
               iob1.equipment_id = @rcv_equipment_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id
         ) = 1
         BEGIN

            SELECT iob1.onhand_qty
            FROM dbo.inv_equipment_onhand_balances  AS iob1 
               WITH ( UPDLOCK )
            WHERE 
               iob1.equipment_id = @rcv_equipment_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_onhand_balances
               SET 
                  onhand_qty = (inv_equipment_onhand_balances.onhand_qty + 1), 
                  updated_by = @rcv_updated_by, 
                  updated_date = isnull(@rcv_updated_date, getdate())
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @rcv_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @rcv_group_id AND 
               inv_equipment_onhand_balances.location_id = @rcv_location_id AND 
               inv_equipment_onhand_balances.site_id = @rcv_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_serials(
               dbo.inv_equipment_serials.serial_number, 
               dbo.inv_equipment_serials.equipment_id, 
               dbo.inv_equipment_serials.site_id, 
               dbo.inv_equipment_serials.group_id, 
               dbo.inv_equipment_serials.location_id, 
               dbo.inv_equipment_serials.status, 
               dbo.inv_equipment_serials.created_by, 
               dbo.inv_equipment_serials.created_date, 
               dbo.inv_equipment_serials.updated_by, 
               dbo.inv_equipment_serials.updated_date)
               VALUES (
                  @rcv_serial, 
                  @rcv_equipment_id, 
                  @rcv_site_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  N'Active', 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.site_id, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date)
               VALUES (
                  N'EQUIPMENT_RECEIPT', 
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  1, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

         END
      ELSE 
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_onhand_balances(
               dbo.inv_equipment_onhand_balances.equipment_id, 
               dbo.inv_equipment_onhand_balances.group_id, 
               dbo.inv_equipment_onhand_balances.location_id, 
               dbo.inv_equipment_onhand_balances.site_id, 
               dbo.inv_equipment_onhand_balances.onhand_qty, 
               dbo.inv_equipment_onhand_balances.created_by, 
               dbo.inv_equipment_onhand_balances.created_date, 
               dbo.inv_equipment_onhand_balances.updated_by, 
               dbo.inv_equipment_onhand_balances.updated_date)
               VALUES (
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_site_id, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_serials(
               dbo.inv_equipment_serials.serial_number, 
               dbo.inv_equipment_serials.equipment_id, 
               dbo.inv_equipment_serials.site_id, 
               dbo.inv_equipment_serials.group_id, 
               dbo.inv_equipment_serials.location_id, 
               dbo.inv_equipment_serials.status, 
               dbo.inv_equipment_serials.created_by, 
               dbo.inv_equipment_serials.created_date, 
               dbo.inv_equipment_serials.updated_by, 
               dbo.inv_equipment_serials.updated_date)
               VALUES (
                  @rcv_serial, 
                  @rcv_equipment_id, 
                  @rcv_site_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  N'Active', 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.site_id, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date)
               VALUES (
                  N'EQUIPMENT_RECEIPT', 
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  1, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

         END

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_rcpt_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_equipment_rcpt_txn  
   @in_equipment nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_quantity int,
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @rcv_equipment_id int

      DECLARE
         @rcv_group_id int

      DECLARE
         @rcv_location_id int

      DECLARE
         @rcv_quantity int

      DECLARE
         @rcv_site_id int

      DECLARE
         @rcv_created_date datetime2(0)

      DECLARE
         @rcv_updated_date datetime2(0)

      DECLARE
         @rcv_created_by int

      DECLARE
         @rcv_updated_by int

      SET @rcv_site_id = @in_site_id

      SET @rcv_equipment_id = 
         (
            SELECT inv_equipment_master.equipment_id
            FROM dbo.inv_equipment_master
            WHERE inv_equipment_master.equipment_name = @in_equipment AND inv_equipment_master.site_id = @rcv_site_id
         )

      SET @rcv_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @rcv_site_id
         )

      SET @rcv_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @rcv_group_id AND 
               inv_locations.site_id = @rcv_site_id
         )

      SET @rcv_quantity = @in_quantity

      SET @rcv_created_date = getdate()

      SET @rcv_updated_date = getdate()

      SET @rcv_created_by = @in_user_id

      SET @rcv_updated_by = @in_user_id

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_equipment_onhand_balances  AS iob1
            WHERE 
               iob1.equipment_id = @rcv_equipment_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id
         ) = 1
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_onhand_balances
               SET 
                  onhand_qty = (inv_equipment_onhand_balances.onhand_qty + @rcv_quantity), 
                  updated_by = @rcv_updated_by, 
                  updated_date = isnull(@rcv_updated_date, getdate())
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @rcv_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @rcv_group_id AND 
               inv_equipment_onhand_balances.location_id = @rcv_location_id AND 
               inv_equipment_onhand_balances.site_id = @rcv_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.site_id, 
               dbo.inv_equipment_txn_history.created_by, 
  dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date)
               VALUES (
                  N'EQUIPMENT_RECEIPT', 
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_quantity, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

         END
      ELSE 
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_onhand_balances(
               dbo.inv_equipment_onhand_balances.equipment_id, 
               dbo.inv_equipment_onhand_balances.group_id, 
               dbo.inv_equipment_onhand_balances.location_id, 
               dbo.inv_equipment_onhand_balances.site_id, 
               dbo.inv_equipment_onhand_balances.onhand_qty, 
               dbo.inv_equipment_onhand_balances.created_by, 
               dbo.inv_equipment_onhand_balances.created_date, 
               dbo.inv_equipment_onhand_balances.updated_by, 
               dbo.inv_equipment_onhand_balances.updated_date)
               VALUES (
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_site_id, 
                  @rcv_quantity, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.site_id, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date)
               VALUES (
                  N'EQUIPMENT_RECEIPT', 
                  @rcv_equipment_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_quantity, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

         END

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_xfer_serial_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*   M2SS0134: Conversion of following Comment(s) is not supported :  Variable Declaration 
*
*/

CREATE PROCEDURE dbo.proc_inv_equipment_xfer_serial_txn  
   @in_equipment nvarchar(50),
   @in_from_group nvarchar(50),
   @in_from_location nvarchar(50),
   @in_to_group nvarchar(50),
   @in_to_location nvarchar(50),
   @in_quantity int,
   @in_site_id int,
   @in_user_id int,
   @in_serial nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @xfer_equipment_id int

      DECLARE
         @xfer_from_group_id int

      DECLARE
         @xfer_from_location_id int

      DECLARE
         @xfer_to_group_id int

      DECLARE
         @xfer_to_location_id int

      DECLARE
         @xfer_site_id int

      DECLARE
         @xfer_created_date datetime2(0)

      DECLARE
         @xfer_updated_date datetime2(0)

      DECLARE
         @xfer_created_by int

      DECLARE
         @xfer_updated_by int

      DECLARE
         @xfer_quantity int

      DECLARE
         @xfer_serial nvarchar(50)

      DECLARE
         @xfer_serial_id int

      DECLARE
         @current_from_onhand_qty int

      SET @xfer_site_id = @in_site_id

      SET @xfer_equipment_id = 
         (
            SELECT inv_equipment_master.equipment_id
            FROM dbo.inv_equipment_master
            WHERE inv_equipment_master.equipment_name = @in_equipment AND inv_equipment_master.site_id = @xfer_site_id
         )

      SET @xfer_from_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_from_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_from_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_from_location AND 
               inv_locations.group_id = @xfer_from_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_to_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_to_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_to_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_to_location AND 
               inv_locations.group_id = @xfer_to_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_created_date = getdate()

      SET @xfer_updated_date = getdate()

      SET @xfer_created_by = @in_user_id

      SET @xfer_updated_by = @in_user_id

      SET @xfer_quantity = @in_quantity

      SET @current_from_onhand_qty = 
         (
            SELECT inv_equipment_onhand_balances.onhand_qty
            FROM dbo.inv_equipment_onhand_balances
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_equipment_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_equipment_onhand_balances.site_id = @xfer_site_id
         )

      SET @xfer_serial = @in_serial

      SET @xfer_serial_id = 
         (
            SELECT inv_equipment_serials.serial_id
            FROM dbo.inv_equipment_serials
            WHERE 
               inv_equipment_serials.serial_number = @xfer_serial AND 
               inv_equipment_serials.equipment_id = @xfer_equipment_id AND 
        inv_equipment_serials.site_id = @xfer_site_id AND 
               inv_equipment_serials.location_id = @xfer_from_location_id
         )

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF @current_from_onhand_qty >= @xfer_quantity
         BEGIN

            SELECT iis.group_id, iis.location_id, iis.updated_by, iis.updated_date
            FROM dbo.inv_equipment_serials  AS iis 
               WITH ( UPDLOCK )
            WHERE iis.serial_id = @xfer_serial_id AND iis.site_id = @xfer_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_serials
               SET 
                  group_id = @xfer_to_group_id, 
                  location_id = @xfer_to_location_id, 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE inv_equipment_serials.serial_id = @xfer_serial_id AND inv_equipment_serials.site_id = @xfer_site_id

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_equipment_onhand_balances  AS iobc
                  WHERE 
                     iobc.equipment_id = @xfer_equipment_id AND 
                     iobc.group_id = @xfer_to_group_id AND 
                     iobc.location_id = @xfer_to_location_id
               ) = 1
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               UPDATE dbo.inv_equipment_onhand_balances
                  SET 
                     onhand_qty = (inv_equipment_onhand_balances.onhand_qty + @xfer_quantity), 
                     updated_date = isnull(@xfer_updated_date, getdate()), 
                     updated_by = @xfer_updated_by
               WHERE 
                  inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
                  inv_equipment_onhand_balances.group_id = @xfer_to_group_id AND 
                  inv_equipment_onhand_balances.location_id = @xfer_to_location_id AND 
                  inv_equipment_onhand_balances.site_id = @xfer_site_id
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_equipment_onhand_balances(
                  dbo.inv_equipment_onhand_balances.equipment_id, 
                  dbo.inv_equipment_onhand_balances.group_id, 
                  dbo.inv_equipment_onhand_balances.location_id, 
                  dbo.inv_equipment_onhand_balances.onhand_qty, 
                  dbo.inv_equipment_onhand_balances.created_by, 
                  dbo.inv_equipment_onhand_balances.created_date, 
                  dbo.inv_equipment_onhand_balances.updated_by, 
                  dbo.inv_equipment_onhand_balances.updated_date, 
                  dbo.inv_equipment_onhand_balances.site_id)
                  VALUES (
                     @xfer_equipment_id, 
                     @xfer_to_group_id, 
                     @xfer_to_location_id, 
                     @xfer_quantity, 
                     @xfer_created_by, 
                     isnull(@xfer_created_date, getdate()), 
                     @xfer_updated_by, 
                     isnull(@xfer_updated_date, getdate()), 
                     @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.serial_number, 
               dbo.inv_equipment_txn_history.from_group_id, 
               dbo.inv_equipment_txn_history.from_loc_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date, 
               dbo.inv_equipment_txn_history.site_id)
               VALUES (
                  N'EQUIPMENT_TRANSFER_SERIAL', 
                  @xfer_equipment_id, 
                  @xfer_serial, 
                  @xfer_from_group_id, 
                  @xfer_from_location_id, 
                  @xfer_to_group_id, 
                  @xfer_to_location_id, 
                  @xfer_quantity, 
                  @xfer_created_by, 
                  isnull(@xfer_created_date, getdate()), 
                  @xfer_updated_by, 
                  isnull(@xfer_updated_date, getdate()), 
                  @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_onhand_balances
               SET 
                  onhand_qty = (inv_equipment_onhand_balances.onhand_qty - @xfer_quantity), 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_equipment_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_equipment_onhand_balances.site_id = @xfer_site_id

         END
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         */

         INSERT dbo.inv_equipment_txn_history(
            dbo.inv_equipment_txn_history.txn_type_name, 
            dbo.inv_equipment_txn_history.equipment_id, 
            dbo.inv_equipment_txn_history.serial_number, 
            dbo.inv_equipment_txn_history.from_group_id, 
            dbo.inv_equipment_txn_history.from_loc_id, 
            dbo.inv_equipment_txn_history.to_group_id, 
            dbo.inv_equipment_txn_history.to_loc_id, 
            dbo.inv_equipment_txn_history.txn_qty, 
            dbo.inv_equipment_txn_history.created_by, 
            dbo.inv_equipment_txn_history.created_date, 
            dbo.inv_equipment_txn_history.updated_by, 
            dbo.inv_equipment_txn_history.updated_date, 
            dbo.inv_equipment_txn_history.site_id)
            VALUES (
               N'TRANSACTION_ERROR', 
               @xfer_equipment_id, 
               @xfer_serial, 
               @xfer_from_group_id, 
               @xfer_from_location_id, 
               @xfer_to_group_id, 
               @xfer_to_location_id, 
               @xfer_quantity, 
               @xfer_created_by, 
               isnull(@xfer_created_date, getdate()), 
               @xfer_updated_by, 
               isnull(@xfer_updated_date, getdate()), 
               @xfer_site_id)

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_equipment_xfer_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*   M2SS0134: Conversion of following Comment(s) is not supported :  Variable Declaration 
*
*/

CREATE PROCEDURE dbo.proc_inv_equipment_xfer_txn  
   @in_equipment nvarchar(50),
   @in_from_group nvarchar(50),
   @in_from_location nvarchar(50),
   @in_to_group nvarchar(50),
   @in_to_location nvarchar(50),
   @in_quantity int,
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @xfer_equipment_id int

      DECLARE
         @xfer_from_group_id int

      DECLARE
         @xfer_from_location_id int

      DECLARE
         @xfer_to_group_id int

      DECLARE
         @xfer_to_location_id int

      DECLARE
         @xfer_site_id int

      DECLARE
         @xfer_created_date datetime2(0)

      DECLARE
         @xfer_updated_date datetime2(0)

      DECLARE
         @xfer_created_by int

      DECLARE
         @xfer_updated_by int

      DECLARE
         @xfer_quantity int

      DECLARE
         @current_from_onhand_qty int

      SET @xfer_site_id = @in_site_id

      SET @xfer_equipment_id = 
         (
            SELECT inv_equipment_master.equipment_id
            FROM dbo.inv_equipment_master
            WHERE inv_equipment_master.equipment_name = @in_equipment AND inv_equipment_master.site_id = @xfer_site_id
         )

      SET @xfer_from_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_from_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_from_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_from_location AND 
               inv_locations.group_id = @xfer_from_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_to_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_to_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_to_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_to_location AND 
               inv_locations.group_id = @xfer_to_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_created_date = getdate()

      SET @xfer_updated_date = getdate()

      SET @xfer_created_by = @in_user_id

      SET @xfer_updated_by = @in_user_id

      SET @xfer_quantity = @in_quantity

      SET @current_from_onhand_qty = 
         (
            SELECT inv_equipment_onhand_balances.onhand_qty
            FROM dbo.inv_equipment_onhand_balances
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_equipment_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_equipment_onhand_balances.site_id = @xfer_site_id
         )

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF @current_from_onhand_qty >= @xfer_quantity
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_equipment_txn_history(
               dbo.inv_equipment_txn_history.txn_type_name, 
               dbo.inv_equipment_txn_history.equipment_id, 
               dbo.inv_equipment_txn_history.from_group_id, 
               dbo.inv_equipment_txn_history.from_loc_id, 
               dbo.inv_equipment_txn_history.to_group_id, 
               dbo.inv_equipment_txn_history.to_loc_id, 
               dbo.inv_equipment_txn_history.txn_qty, 
               dbo.inv_equipment_txn_history.created_by, 
               dbo.inv_equipment_txn_history.created_date, 
               dbo.inv_equipment_txn_history.updated_by, 
               dbo.inv_equipment_txn_history.updated_date, 
               dbo.inv_equipment_txn_history.site_id)
               VALUES (
                  N'EQUIPMENT_TRANSFER', 
                  @xfer_equipment_id, 
                  @xfer_from_group_id, 
                  @xfer_from_location_id, 
                  @xfer_to_group_id, 
                  @xfer_to_location_id, 
                  @xfer_quantity, 
                  @xfer_created_by, 
                  isnull(@xfer_created_date, getdate()), 
                  @xfer_updated_by, 
                  isnull(@xfer_updated_date, getdate()), 
                  @xfer_site_id)

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_equipment_onhand_balances  AS iobc
                  WHERE 
                     iobc.equipment_id = @xfer_equipment_id AND 
                     iobc.group_id = @xfer_to_group_id AND 
                     iobc.location_id = @xfer_to_location_id
               ) = 1
               /* 
               *   SSMA error messages:
               *   M2SS0283: A column cannot be assigned more than one value in the same SET clause. 
               *    Modify the SET clause to make sure that a column is updated only once.

               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date

               UPDATE dbo.inv_equipment_onhand_balances
                  SET 
                     onhand_qty = (inv_equipment_onhand_balances.onhand_qty + @xfer_quantity), 
                     updated_date = isnull(@xfer_updated_date, getdate()), 
                     updated_by = @xfer_updated_by, 
                     updated_date = isnull(@xfer_updated_date, getdate())
               WHERE 
                  inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
                  inv_equipment_onhand_balances.group_id = @xfer_to_group_id AND 
                  inv_equipment_onhand_balances.location_id = @xfer_to_location_id AND 
                  inv_equipment_onhand_balances.site_id = @xfer_site_id
               */


               DECLARE
                  @db_null_statement int
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_equipment_onhand_balances(
                  dbo.inv_equipment_onhand_balances.equipment_id, 
                  dbo.inv_equipment_onhand_balances.group_id, 
                  dbo.inv_equipment_onhand_balances.location_id, 
                  dbo.inv_equipment_onhand_balances.onhand_qty, 
                  dbo.inv_equipment_onhand_balances.created_by, 
                  dbo.inv_equipment_onhand_balances.created_date, 
                  dbo.inv_equipment_onhand_balances.updated_by, 
                  dbo.inv_equipment_onhand_balances.updated_date, 
                  dbo.inv_equipment_onhand_balances.site_id)
                  VALUES (
                     @xfer_equipment_id, 
                     @xfer_to_group_id, 
                     @xfer_to_location_id, 
                     @xfer_quantity, 
                     @xfer_created_by, 
                     isnull(@xfer_created_date, getdate()), 
                     @xfer_updated_by, 
                     isnull(@xfer_updated_date, getdate()), 
                     @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_equipment_onhand_balances
               SET 
                  onhand_qty = (inv_equipment_onhand_balances.onhand_qty - @xfer_quantity), 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE 
               inv_equipment_onhand_balances.equipment_id = @xfer_equipment_id AND 
               inv_equipment_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_equipment_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_equipment_onhand_balances.site_id = @xfer_site_id

         END
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         */

         INSERT dbo.inv_equipment_txn_history(
            dbo.inv_equipment_txn_history.txn_type_name, 
            dbo.inv_equipment_txn_history.equipment_id, 
            dbo.inv_equipment_txn_history.from_group_id, 
            dbo.inv_equipment_txn_history.from_loc_id, 
            dbo.inv_equipment_txn_history.to_group_id, 
            dbo.inv_equipment_txn_history.to_loc_id, 
            dbo.inv_equipment_txn_history.txn_qty, 
            dbo.inv_equipment_txn_history.created_by, 
            dbo.inv_equipment_txn_history.created_date, 
            dbo.inv_equipment_txn_history.updated_by, 
            dbo.inv_equipment_txn_history.updated_date, 
            dbo.inv_equipment_txn_history.site_id)
            VALUES (
               N'TRANSACTION ERROR', 
               @xfer_equipment_id, 
               @xfer_from_group_id, 
               @xfer_from_location_id, 
               @xfer_to_group_id, 
               @xfer_to_location_id, 
               @xfer_quantity, 
               @xfer_created_by, 
               isnull(@xfer_created_date, getdate()), 
               @xfer_updated_by, 
               isnull(@xfer_updated_date, getdate()), 
               @xfer_site_id)

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_group_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*/

CREATE PROCEDURE [dbo].[proc_inv_group_create]  
   @in_group nvarchar(20),
   @in_site_id int
   /*,
   @group_create_status nvarchar(30)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @group_create_status = NULL	*/

      DECLARE
         @crt_group_name nvarchar(20)

      DECLARE
         @crt_site_id int

      DECLARE
         @crt_created_date datetime2(0)

      DECLARE
         @crt_updated_date datetime2(0)

      DECLARE
         @crt_created_by int

      DECLARE
         @crt_updated_by int

	  SET @crt_group_name = @in_group

      SET @crt_site_id = @in_site_id

      SET @crt_created_date = getdate()

      SET @crt_updated_date = getdate()

      SET @crt_created_by = 1

      SET @crt_updated_by = 1

	  
      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.inv_groups(
         dbo.inv_groups.group_name, 
         dbo.inv_groups.site_id, 
         dbo.inv_groups.created_by, 
         dbo.inv_groups.created_date, 
         dbo.inv_groups.updated_by, 
         dbo.inv_groups.updated_date)
         VALUES (
            @crt_group_name, 
            @crt_site_id, 
            @crt_created_by, 
            isnull(@crt_created_date, getdate()), 
            @crt_updated_by, 
            isnull(@crt_updated_date, getdate()))

     /* SET @group_create_status = N'Success!'
	  SELECT @group_create_status;*/

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_issue_serial_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_issue_serial_txn  
   @in_item nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_comment nvarchar(100),
   @in_serial nvarchar(128),
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @iss_item_id int

      DECLARE
         @iss_group_id int

      DECLARE
         @iss_location_id int

      DECLARE
         @iss_serial nvarchar(128)

      DECLARE
         @iss_comment nvarchar(100)

      DECLARE
         @iss_serial_id int

      DECLARE
         @iss_site_id int

      DECLARE
         @iss_uom_id int

      DECLARE
         @iss_serial_current_status nvarchar(50)

      DECLARE
         @iss_created_date datetime2(0)

      DECLARE
         @iss_updated_date datetime2(0)

      DECLARE
         @iss_created_by int

      DECLARE
         @iss_updated_by int

      SET @iss_site_id = @in_site_id

      SET @iss_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @iss_site_id
         )

      SET @iss_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @iss_site_id
         )

      SET @iss_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @iss_group_id AND 
               inv_locations.site_id = @iss_site_id
         )

      SET @iss_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_id = @iss_item_id
         )

      SET @iss_comment = @in_comment

      SET @iss_serial = @in_serial

      SET @iss_serial_id = 
         (
            SELECT inv_item_serials.serial_id
            FROM dbo.inv_item_serials
            WHERE 
               inv_item_serials.serial_number = @iss_serial AND 
               inv_item_serials.item_id = @iss_item_id AND 
               inv_item_serials.site_id = @iss_site_id AND 
               inv_item_serials.location_id = @iss_location_id
         )

      SET @iss_serial_current_status = 
         (
            SELECT inv_item_serials.status
            FROM dbo.inv_item_serials
            WHERE inv_item_serials.serial_id = @iss_serial_id
         )

      SET @iss_created_date = getdate()

      SET @iss_updated_date = getdate()

      SET @iss_created_by = @in_user_id

      SET @iss_updated_by = @in_user_id

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF @iss_serial_current_status = 'Active'
         IF 
            (
               SELECT count_big(*)
               FROM dbo.inv_item_onhand_balances  AS iob1
               WHERE 
                  iob1.item_id = @iss_item_id AND 
                  iob1.group_id = @iss_group_id AND 
                  iob1.location_id = @iss_location_id AND 
                  iob1.site_id = @iss_site_id
            ) = 1
            BEGIN

               SELECT iis.status, iis.updated_by, iis.updated_date
               FROM dbo.inv_item_serials  AS iis 
                  WITH ( UPDLOCK )
               WHERE iis.serial_id = @iss_serial_id AND iis.site_id = @iss_site_id

               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

     UPDATE dbo.inv_item_serials
                  SET 
                     status = N'Issued', 
                     updated_by = @iss_updated_by, 
                     updated_date = isnull(@iss_updated_date, getdate())
               WHERE inv_item_serials.serial_id = @iss_serial_id AND inv_item_serials.site_id = @iss_site_id

               SELECT iob1.onhand_qty
               FROM dbo.inv_item_onhand_balances  AS iob1 
                  WITH ( UPDLOCK )
               WHERE 
                  iob1.item_id = @iss_item_id AND 
                  iob1.group_id = @iss_group_id AND 
                  iob1.location_id = @iss_location_id AND 
                  iob1.site_id = @iss_site_id

               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               UPDATE dbo.inv_item_onhand_balances
                  SET 
                     onhand_qty = (inv_item_onhand_balances.onhand_qty - 1), 
                     updated_by = @iss_updated_by, 
                     updated_date = isnull(@iss_updated_date, getdate())
               WHERE 
                  inv_item_onhand_balances.item_id = @iss_item_id AND 
                  inv_item_onhand_balances.group_id = @iss_group_id AND 
                  inv_item_onhand_balances.location_id = @iss_location_id AND 
                  inv_item_onhand_balances.site_id = @iss_site_id

               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_txn_history(
                  dbo.inv_txn_history.txn_type_name, 
                  dbo.inv_txn_history.item_id, 
                  dbo.inv_txn_history.serial_number, 
                  dbo.inv_txn_history.to_group_id, 
                  dbo.inv_txn_history.to_loc_id, 
                  dbo.inv_txn_history.txn_qty, 
                  dbo.inv_txn_history.txn_uom_id, 
                  dbo.inv_txn_history.site_id, 
                  dbo.inv_txn_history.created_by, 
                  dbo.inv_txn_history.created_date, 
                  dbo.inv_txn_history.updated_by, 
                  dbo.inv_txn_history.updated_date, 
                  dbo.inv_txn_history.comment)
                  VALUES (
                     N'ITEM_ISSUE', 
                     @iss_item_id, 
                     @iss_serial, 
                     @iss_group_id, 
                     @iss_location_id, 
                     @iss_uom_id, 
                     @iss_uom_id, 
                     @iss_site_id, 
                     @iss_created_by, 
                     isnull(@iss_created_date, getdate()), 
                     @iss_updated_by, 
                     isnull(@iss_updated_date, getdate()), 
                     @iss_comment)

            END
         ELSE 
            /* 
            *   SSMA error messages:
            *   M2SS0016: Identifier iss_quantity cannot be converted because it was not resolved.

            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.serial_number, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'ITEM_ISSUE - ERROR INSUFFICIENT QUANTITY', 
                  @iss_item_id, 
                  @iss_serial, 
                  @iss_group_id, 
                  @iss_location_id, 
                  iss_quantity, 
                  @iss_uom_id, 
                  @iss_site_id, 
                  @iss_created_by, 
                  isnull(@iss_created_date, getdate()), 
                  @iss_updated_by, 
                  isnull(@iss_updated_date, getdate()), 
                  @iss_comment)
            */


            DECLARE
               @db_null_statement int

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_issue_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_issue_txn  
   @in_item nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_quantity int,
   @in_comment nvarchar(100),
   @in_site_id int,
   @in_user_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @iss_item_id int

      DECLARE
         @iss_group_id int

      DECLARE
         @iss_location_id int

      DECLARE
         @iss_quantity int

      DECLARE
         @iss_uom_id int

      DECLARE
         @iss_site_id int

      DECLARE
         @iss_created_date datetime2(0)

      DECLARE
         @iss_updated_date datetime2(0)

      DECLARE
         @iss_created_by int

      DECLARE
         @iss_updated_by int

      DECLARE
         @iss_comment nvarchar(100)

      SET @iss_site_id = @in_site_id

      SET @iss_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @iss_site_id
         )

      SET @iss_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @iss_site_id
         )

      SET @iss_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @iss_group_id AND 
               inv_locations.site_id = @iss_site_id
         )

      SET @iss_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_id = @iss_item_id
         )

      SET @iss_quantity = @in_quantity

      SET @iss_created_date = getdate()

      SET @iss_updated_date = getdate()

      SET @iss_created_by = @in_user_id

      SET @iss_updated_by = @in_user_id

      SET @iss_comment = @in_comment

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_item_onhand_balances  AS iob1
            WHERE 
               iob1.item_id = @iss_item_id AND 
               iob1.group_id = @iss_group_id AND 
               iob1.location_id = @iss_location_id
         ) = 1
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_onhand_balances
               SET 
                  onhand_qty = (inv_item_onhand_balances.onhand_qty - @iss_quantity), 
                  updated_by = @iss_updated_by, 
                  updated_date = isnull(@iss_updated_date, getdate())
            WHERE 
               inv_item_onhand_balances.item_id = @iss_item_id AND 
               inv_item_onhand_balances.group_id = @iss_group_id AND 
               inv_item_onhand_balances.location_id = @iss_location_id AND 
               inv_item_onhand_balances.site_id = @iss_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
          dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'ITEM_ISSUE', 
                  @iss_item_id, 
                  @iss_group_id, 
                  @iss_location_id, 
                  @iss_quantity, 
                  @iss_uom_id, 
                  @iss_site_id, 
                  @iss_created_by, 
                  isnull(@iss_created_date, getdate()), 
                  @iss_updated_by, 
                  isnull(@iss_updated_date, getdate()), 
                  @iss_comment)

         END
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         */

         INSERT dbo.inv_txn_history(
            dbo.inv_txn_history.txn_type_name, 
            dbo.inv_txn_history.item_id, 
            dbo.inv_txn_history.to_group_id, 
            dbo.inv_txn_history.to_loc_id, 
            dbo.inv_txn_history.txn_qty, 
            dbo.inv_txn_history.txn_uom_id, 
            dbo.inv_txn_history.site_id, 
            dbo.inv_txn_history.created_by, 
            dbo.inv_txn_history.created_date, 
            dbo.inv_txn_history.updated_by, 
            dbo.inv_txn_history.updated_date, 
            dbo.inv_txn_history.comment)
            VALUES (
               N'ITEM_ISSUE - ERROR INSUFFICIENT QUANTITY', 
               @iss_item_id, 
               @iss_group_id, 
               @iss_location_id, 
               @iss_quantity, 
               @iss_uom_id, 
               @iss_site_id, 
               @iss_created_by, 
               isnull(@iss_created_date, getdate()), 
               @iss_updated_by, 
               isnull(@iss_updated_date, getdate()), 
               @iss_comment)

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_item_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE [dbo].[proc_inv_item_create]  
   @in_item varchar(20),
   @in_item_desc varchar(50),
   @in_uom varchar(10),
   @in_status varchar(20),
   @in_serial varchar(5),
   @in_reorder varchar(5),
   @in_consumable varchar(5),
   @in_user_id int,
   @in_site_id int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @crt_item_name nvarchar(20)

      DECLARE
         @crt_item_desc nvarchar(50)

      DECLARE
         @crt_uom_id int

      DECLARE
         @crt_site_id int

      DECLARE
         @crt_created_date datetime2(0)

      DECLARE
         @crt_updated_date datetime2(0)

      DECLARE
         @crt_created_by int

      DECLARE
         @crt_updated_by int

      DECLARE
         @crt_status nvarchar(20)

      SET @crt_item_name = @in_item

      SET @crt_item_desc = @in_item_desc

      SET @crt_uom_id = 
         (
            SELECT iuom.uom_id
            FROM dbo.inv_units_of_measure  AS iuom
            WHERE iuom.uom_name = @in_uom
         )

      SET @crt_status = @in_status

      SET @crt_site_id = @in_site_id

      SET @crt_created_date = getdate()

      SET @crt_updated_date = getdate()

      SET @crt_created_by = @in_user_id

      SET @crt_updated_by = @in_user_id

      /*
      *   SSMA informational messages:
      *   M2SS0214: The Default Values for the Columns have been added in the Insert Statement based on Project setttings.
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.inv_item_master(
         dbo.inv_item_master.item_name, 
         dbo.inv_item_master.item_description, 
         dbo.inv_item_master.base_uom_id, 
         dbo.inv_item_master.site_id, 
         dbo.inv_item_master.status, 
         dbo.inv_item_master.created_by, 
         dbo.inv_item_master.created_date, 
         dbo.inv_item_master.updated_by, 
         dbo.inv_item_master.updated_date, 
         dbo.inv_item_master.serial_enabled, 
         dbo.inv_item_master.consumable, 
         dbo.inv_item_master.planning_enabled, 
         dbo.inv_item_master.item_picture)
         VALUES (
            @crt_item_name, 
            @crt_item_desc, 
            @crt_uom_id, 
            @crt_site_id, 
            @crt_status, 
            @crt_created_by, 
            isnull(@crt_created_date, getdate()), 
            @crt_updated_by, 
            isnull(@crt_updated_date, getdate()), 
            @in_serial, 
            @in_consumable, 
            @in_reorder, 
            0x)

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_location_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE [dbo].[proc_inv_location_create]  
   @in_location nvarchar(20),
   @in_group_name nvarchar(20),
   @in_site_id int,
   @in_user_id int
   /*@location_create_status nvarchar(30)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @location_create_status = NULL*/

      DECLARE
         @crt_location_name nvarchar(20)

      DECLARE
         @crt_group_id int

      DECLARE
         @crt_site_id int

      DECLARE
         @crt_created_date datetime2(0)

      DECLARE
         @crt_updated_date datetime2(0)

      DECLARE
         @crt_created_by int

      DECLARE
         @crt_updated_by int
	  DECLARE
		 @location_create_status nvarchar(30)

      SET @crt_location_name = @in_location

      SET @crt_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group_name AND inv_groups.site_id = @in_site_id
         )

      SET @crt_site_id = @in_site_id

      SET @crt_created_date = getdate()

      SET @crt_updated_date = getdate()

      SET @crt_created_by = @in_user_id

      SET @crt_updated_by = @in_user_id

	  SET @location_create_status = NULL

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.inv_locations(
         dbo.inv_locations.location_name, 
         dbo.inv_locations.group_id, 
         dbo.inv_locations.site_id, 
         dbo.inv_locations.created_by, 
         dbo.inv_locations.created_date, 
         dbo.inv_locations.updated_by, 
         dbo.inv_locations.updated_date)
         VALUES (
            @crt_location_name, 
            @crt_group_id, 
            @crt_site_id, 
            @crt_created_by, 
            isnull(@crt_created_date, getdate()), 
            @crt_updated_by, 
            isnull(@crt_updated_date, getdate()))

     /* SET @location_create_status = N'Success!'
	  SELECT @location_create_status;*/

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_rcpt_serial_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_rcpt_serial_txn  
   @in_item nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_serial nvarchar(128),
   @in_site_id int,
   @in_user_id int,
   @in_comment nvarchar(100)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @rcv_item_id int

      DECLARE
         @rcv_group_id int

      DECLARE
         @rcv_location_id int

      DECLARE
         @rcv_serial nvarchar(128)

      DECLARE
         @rcv_site_id int

      DECLARE
         @rcv_uom_id int

      DECLARE
         @rcv_created_date datetime2(0)

      DECLARE
         @rcv_updated_date datetime2(0)

      DECLARE
         @rcv_created_by int

      DECLARE
         @rcv_updated_by int

      DECLARE
         @rcv_comment nvarchar(100)

      SET @rcv_site_id = @in_site_id

      SET @rcv_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @rcv_site_id
         )

      SET @rcv_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @rcv_site_id
         )

      SET @rcv_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @rcv_group_id AND 
               inv_locations.site_id = @rcv_site_id
         )

      SET @rcv_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_id = @rcv_item_id
         )

      SET @rcv_serial = @in_serial

      SET @rcv_created_date = getdate()

      SET @rcv_updated_date = getdate()

      SET @rcv_created_by = @in_user_id

      SET @rcv_updated_by = @in_user_id

      SET @rcv_comment = @in_comment

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_item_onhand_balances  AS iob1
            WHERE 
               iob1.item_id = @rcv_item_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id
         ) = 1
         BEGIN

            SELECT iob1.onhand_qty
            FROM dbo.inv_item_onhand_balances  AS iob1 
               WITH ( UPDLOCK )
            WHERE 
               iob1.item_id = @rcv_item_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_onhand_balances
               SET 
                  onhand_qty = (inv_item_onhand_balances.onhand_qty + 1), 
                  updated_by = @rcv_updated_by, 
                  updated_date = isnull(@rcv_updated_date, getdate())
            WHERE 
               inv_item_onhand_balances.item_id = @rcv_item_id AND 
               inv_item_onhand_balances.group_id = @rcv_group_id AND 
               inv_item_onhand_balances.location_id = @rcv_location_id AND 
               inv_item_onhand_balances.site_id = @rcv_site_id

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_item_serials
                  WHERE 
      inv_item_serials.item_id = @rcv_item_id AND 
                     inv_item_serials.site_id = @rcv_site_id AND 
                     inv_item_serials.serial_number = @rcv_serial AND 
                     inv_item_serials.status <> 'Active'
               ) = 1
               BEGIN

                  SELECT 
                     iis.status, 
                     iis.group_id, 
                     iis.location_id, 
                     iis.updated_by, 
                     iis.updated_date
                  FROM dbo.inv_item_serials  AS iis 
                     WITH ( UPDLOCK )
                  WHERE 
                     iis.item_id = @rcv_item_id AND 
                     iis.site_id = @rcv_item_id AND 
                     iis.serial_number = @rcv_serial

                  /*
                  *   SSMA informational messages:
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  */

                  UPDATE dbo.inv_item_serials
                     SET 
                        status = N'Active', 
                        group_id = @rcv_group_id, 
                        location_id = @rcv_location_id, 
                        updated_by = @rcv_updated_by, 
                        updated_date = isnull(@rcv_updated_date, getdate())
                  WHERE 
                     inv_item_serials.item_id = @rcv_item_id AND 
                     inv_item_serials.site_id = @rcv_site_id AND 
                     inv_item_serials.serial_number = @rcv_serial

               END
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_item_serials(
                  dbo.inv_item_serials.serial_number, 
                  dbo.inv_item_serials.item_id, 
                  dbo.inv_item_serials.site_id, 
                  dbo.inv_item_serials.group_id, 
                  dbo.inv_item_serials.location_id, 
                  dbo.inv_item_serials.status, 
                  dbo.inv_item_serials.created_by, 
                  dbo.inv_item_serials.created_date, 
                  dbo.inv_item_serials.updated_by, 
                  dbo.inv_item_serials.updated_date)
                  VALUES (
                     @rcv_serial, 
                     @rcv_item_id, 
                     @rcv_site_id, 
                     @rcv_group_id, 
                     @rcv_location_id, 
                     N'Active', 
                     @rcv_created_by, 
                     isnull(@rcv_created_date, getdate()), 
                     @rcv_updated_by, 
                     isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.serial_number, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
           VALUES (
                  N'ITEM_RECEIPT_SERIAL', 
                  @rcv_item_id, 
                  @rcv_serial, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  1, 
                  @rcv_uom_id, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()), 
                  @rcv_comment)

         END
      ELSE 
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_item_onhand_balances(
               dbo.inv_item_onhand_balances.item_id, 
               dbo.inv_item_onhand_balances.group_id, 
               dbo.inv_item_onhand_balances.location_id, 
               dbo.inv_item_onhand_balances.site_id, 
               dbo.inv_item_onhand_balances.onhand_qty, 
               dbo.inv_item_onhand_balances.base_uom_id, 
               dbo.inv_item_onhand_balances.created_by, 
               dbo.inv_item_onhand_balances.created_date, 
               dbo.inv_item_onhand_balances.updated_by, 
               dbo.inv_item_onhand_balances.updated_date)
               VALUES (
                  @rcv_item_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_site_id, 
                  1, 
                  @rcv_uom_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_item_serials
                  WHERE 
                     inv_item_serials.item_id = @rcv_item_id AND 
                     inv_item_serials.site_id = @rcv_site_id AND 
                     inv_item_serials.serial_number = @rcv_serial AND 
                     inv_item_serials.status <> 'Active'
               ) = 1
               BEGIN

                  SELECT 
                     iis.status, 
                     iis.group_id, 
                     iis.location_id, 
                     iis.updated_by, 
                     iis.updated_date
                  FROM dbo.inv_item_serials  AS iis 
                     WITH ( UPDLOCK )
                  WHERE 
                     iis.item_id = @rcv_item_id AND 
                     iis.site_id = @rcv_item_id AND 
                     iis.serial_number = @rcv_serial

                  /*
                  *   SSMA informational messages:
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  */

                  UPDATE dbo.inv_item_serials
                     SET 
                        status = N'Active', 
                        group_id = @rcv_group_id, 
                        location_id = @rcv_location_id, 
                        updated_by = @rcv_updated_by, 
                        updated_date = isnull(@rcv_updated_date, getdate())
                  WHERE 
                     inv_item_serials.item_id = @rcv_item_id AND 
                     inv_item_serials.site_id = @rcv_site_id AND 
                     inv_item_serials.serial_number = @rcv_serial

               END
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_item_serials(
                  dbo.inv_item_serials.serial_number, 
                  dbo.inv_item_serials.item_id, 
                  dbo.inv_item_serials.site_id, 
                  dbo.inv_item_serials.group_id, 
                  dbo.inv_item_serials.location_id, 
                  dbo.inv_item_serials.status, 
                  dbo.inv_item_serials.created_by, 
                  dbo.inv_item_serials.created_date, 
                  dbo.inv_item_serials.updated_by, 
                  dbo.inv_item_serials.updated_date)
                  VALUES (
                     @rcv_serial, 
                     @rcv_item_id, 
                     @rcv_site_id, 
                     @rcv_group_id, 
                     @rcv_location_id, 
                     N'Active', 
                     @rcv_created_by, 
                     isnull(@rcv_created_date, getdate()), 
                     @rcv_updated_by, 
                     isnull(@rcv_updated_date, getdate()))

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.serial_number, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'ITEM_RECEIPT_SERIAL', 
                  @rcv_item_id, 
                  @rcv_serial, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  1, 
                  @rcv_uom_id, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()), 
                  @rcv_comment)

         END

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_rcpt_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_rcpt_txn  
   @in_item nvarchar(20),
   @in_group nvarchar(50),
   @in_location nvarchar(10),
   @in_quantity int,
   @in_site_id int,
   @in_user_id int,
   @in_comment nvarchar(100)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @rcv_item_id int

      DECLARE
         @rcv_group_id int

      DECLARE
         @rcv_location_id int

      DECLARE
         @rcv_quantity int

      DECLARE
         @rcv_site_id int

      DECLARE
         @rcv_uom_id int

      DECLARE
         @rcv_created_date datetime2(0)

      DECLARE
         @rcv_updated_date datetime2(0)

      DECLARE
         @rcv_created_by int

      DECLARE
         @rcv_updated_by int

      DECLARE
         @rcv_comment nvarchar(100)

      SET @rcv_site_id = @in_site_id

      SET @rcv_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @rcv_site_id
         )

      SET @rcv_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_group AND inv_groups.site_id = @rcv_site_id
         )

      SET @rcv_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_location AND 
               inv_locations.group_id = @rcv_group_id AND 
               inv_locations.site_id = @rcv_site_id
         )

      SET @rcv_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_id = @rcv_item_id
         )

      SET @rcv_quantity = @in_quantity

      SET @rcv_created_date = getdate()

      SET @rcv_updated_date = getdate()

      SET @rcv_created_by = @in_user_id

      SET @rcv_updated_by = @in_user_id

      SET @rcv_comment = @in_comment

      IF 
         (
            SELECT count_big(*)
            FROM dbo.inv_item_onhand_balances  AS iob1
            WHERE 
               iob1.item_id = @rcv_item_id AND 
               iob1.group_id = @rcv_group_id AND 
               iob1.location_id = @rcv_location_id AND 
               iob1.site_id = @rcv_site_id
         ) = 1
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_onhand_balances
               SET 
                  onhand_qty = (inv_item_onhand_balances.onhand_qty + @rcv_quantity), 
                  updated_by = @rcv_updated_by, 
                  updated_date = isnull(@rcv_updated_date, getdate())
            WHERE 
               inv_item_onhand_balances.item_id = @rcv_item_id AND 
               inv_item_onhand_balances.group_id = @rcv_group_id AND 
               inv_item_onhand_balances.location_id = @rcv_location_id AND 
               inv_item_onhand_balances.site_id = @rcv_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
            dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'ITEM_RECEIPT', 
                  @rcv_item_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_quantity, 
                  @rcv_uom_id, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()), 
                  @rcv_comment)

         END
      ELSE 
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_item_onhand_balances(
               dbo.inv_item_onhand_balances.item_id, 
               dbo.inv_item_onhand_balances.group_id, 
               dbo.inv_item_onhand_balances.location_id, 
               dbo.inv_item_onhand_balances.site_id, 
               dbo.inv_item_onhand_balances.onhand_qty, 
               dbo.inv_item_onhand_balances.base_uom_id, 
               dbo.inv_item_onhand_balances.created_by, 
               dbo.inv_item_onhand_balances.created_date, 
               dbo.inv_item_onhand_balances.updated_by, 
               dbo.inv_item_onhand_balances.updated_date)
               VALUES (
                  @rcv_item_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_site_id, 
                  @rcv_quantity, 
                  @rcv_uom_id, 
                  @rcv_created_by, 
                  isnull(@rcv_created_date, getdate()), 
                  @rcv_updated_by, 
                  isnull(@rcv_updated_date, getdate()))

            /* 
            *   SSMA error messages:
            *   M2SS0165: The column referenced in VALUES clause must be previously assigned in the same INSERT statement.

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'ITEM_RECEIPT', 
                  @rcv_item_id, 
                  @rcv_group_id, 
                  @rcv_location_id, 
                  @rcv_quantity, 
                  @rcv_uom_id, 
                  @rcv_site_id, 
                  @rcv_created_by, 
                  @rcv_created_date, 
                  @rcv_updated_by, 
                  @rcv_updated_date, 
                  dbo.inv_txn_history.comment)
            */



         END

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_site_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_inv_site_create  
   @in_site nvarchar(20),
   @in_user int
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @crt_site_name nvarchar(20)

      DECLARE
         @crt_created_date datetime2(0)

      DECLARE
         @crt_updated_date datetime2(0)

      DECLARE
         @crt_created_by int

      DECLARE
         @crt_updated_by int

      SET @crt_site_name = @in_site

      SET @crt_created_date = getdate()

      SET @crt_updated_date = getdate()

      SET @crt_created_by = @in_user

      SET @crt_updated_by = @in_user

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.inv_sites(
         dbo.inv_sites.site_name, 
         dbo.inv_sites.enabled_flag, 
         dbo.inv_sites.created_by, 
         dbo.inv_sites.created_date, 
         dbo.inv_sites.updated_by, 
         dbo.inv_sites.updated_date)
         VALUES (
            @crt_site_name, 
            1, 
            @crt_created_by, 
            isnull(@crt_created_date, getdate()), 
            @crt_updated_by, 
            isnull(@crt_updated_date, getdate()))

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_xfer_serial_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*   M2SS0134: Conversion of following Comment(s) is not supported :  Variable Declaration 
*
*/

CREATE PROCEDURE dbo.proc_inv_xfer_serial_txn  
   @in_item nvarchar(50),
   @in_from_group nvarchar(50),
   @in_from_location nvarchar(50),
   @in_to_group nvarchar(50),
   @in_to_location nvarchar(50),
   @in_quantity int,
   @in_comment nvarchar(100),
   @in_site_id int,
   @in_user_id int,
   @in_serial nvarchar(50)
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      DECLARE
         @xfer_item_id int

      DECLARE
         @xfer_from_group_id int

      DECLARE
         @xfer_from_location_id int

      DECLARE
         @xfer_to_group_id int

      DECLARE
         @xfer_to_location_id int

      DECLARE
         @xfer_site_id int

      DECLARE
         @xfer_uom_id int

      DECLARE
         @xfer_comment nvarchar(100)

      DECLARE
         @xfer_created_date datetime2(0)

      DECLARE
         @xfer_updated_date datetime2(0)

      DECLARE
         @xfer_created_by int

      DECLARE
         @xfer_updated_by int

      DECLARE
         @xfer_quantity int

      DECLARE
         @xfer_serial nvarchar(50)

      DECLARE
         @xfer_serial_id int

      DECLARE
         @current_from_onhand_qty int

      SET @xfer_site_id = @in_site_id

      SET @xfer_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @xfer_site_id
         )

      SET @xfer_from_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_from_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_from_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_from_location AND 
               inv_locations.group_id = @xfer_from_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_to_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_to_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_to_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_to_location AND 
               inv_locations.group_id = @xfer_to_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_id = @xfer_item_id
         )

      SET @xfer_comment = @in_comment

      SET @xfer_created_date = getdate()

      SET @xfer_updated_date = getdate()

      SET @xfer_created_by = @in_user_id

      SET @xfer_updated_by = @in_user_id

      SET @xfer_quantity = @in_quantity

      SET @current_from_onhand_qty = 
         (
            SELECT inv_item_onhand_balances.onhand_qty
            FROM dbo.inv_item_onhand_balances
            WHERE 
               inv_item_onhand_balances.item_id = @xfer_item_id AND 
               inv_item_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_item_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_item_onhand_balances.site_id = @xfer_site_id
         )

      SET @xfer_serial = @in_serial

      SET @xfer_serial_id = 
         (
            SELECT inv_item_serials.serial_id
            FROM dbo.inv_item_serials
            WHERE 
               inv_item_serials.serial_number = @xfer_serial AND 
               inv_item_serials.item_id = @xfer_item_id AND 
               inv_item_serials.site_id = @xfer_site_id AND 
               inv_item_serials.location_id = @xfer_from_location_id
         )

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

      BEGIN TRANSACTION 

      IF @current_from_onhand_qty >= @xfer_quantity
         BEGIN

            SELECT iis.group_id, iis.location_id, iis.updated_by, iis.updated_date
            FROM dbo.inv_item_serials  AS iis 
               WITH ( UPDLOCK )
            WHERE iis.serial_id = @xfer_serial_id AND iis.site_id = @xfer_site_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_serials
               SET 
                  group_id = @xfer_to_group_id, 
                  location_id = @xfer_to_location_id, 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE inv_item_serials.serial_id = @xfer_serial_id AND inv_item_serials.site_id = @xfer_site_id

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_item_onhand_balances  AS iobc
                  WHERE 
                     iobc.item_id = @xfer_item_id AND 
                     iobc.group_id = @xfer_to_group_id AND 
                     iobc.location_id = @xfer_to_location_id
               ) = 1
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               UPDATE dbo.inv_item_onhand_balances
                  SET 
                     onhand_qty = (inv_item_onhand_balances.onhand_qty + @xfer_quantity), 
                     updated_date = isnull(@xfer_updated_date, getdate()), 
                     updated_by = @xfer_updated_by
               WHERE 
                  inv_item_onhand_balances.item_id = @xfer_item_id AND 
                  inv_item_onhand_balances.group_id = @xfer_to_group_id AND 
                  inv_item_onhand_balances.location_id = @xfer_to_location_id AND 
                  inv_item_onhand_balances.site_id = @xfer_site_id
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_item_onhand_balances(
                  dbo.inv_item_onhand_balances.item_id, 
                  dbo.inv_item_onhand_balances.group_id, 
                  dbo.inv_item_onhand_balances.location_id, 
                  dbo.inv_item_onhand_balances.onhand_qty, 
                  dbo.inv_item_onhand_balances.base_uom_id, 
                  dbo.inv_item_onhand_balances.created_by, 
                  dbo.inv_item_onhand_balances.created_date, 
                  dbo.inv_item_onhand_balances.updated_by, 
                  dbo.inv_item_onhand_balances.updated_date, 
                  dbo.inv_item_onhand_balances.site_id)
                  VALUES (
                     @xfer_item_id, 
                     @xfer_to_group_id, 
                     @xfer_to_location_id, 
                     @xfer_quantity, 
                     1, 
                     @xfer_created_by, 
                     isnull(@xfer_created_date, getdate()), 
                     @xfer_updated_by, 
                     isnull(@xfer_updated_date, getdate()), 
        @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.serial_number, 
               dbo.inv_txn_history.from_group_id, 
               dbo.inv_txn_history.from_loc_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.comment, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.site_id)
               VALUES (
                  N'INVENTORY_TRANSFER_SERIAL', 
                  @xfer_item_id, 
                  @xfer_serial, 
                  @xfer_from_group_id, 
                  @xfer_from_location_id, 
                  @xfer_to_group_id, 
                  @xfer_to_location_id, 
                  @xfer_comment, 
                  @xfer_quantity, 
                  1, 
                  @xfer_created_by, 
                  isnull(@xfer_created_date, getdate()), 
                  @xfer_updated_by, 
                  isnull(@xfer_updated_date, getdate()), 
                  @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_onhand_balances
               SET 
                  onhand_qty = (inv_item_onhand_balances.onhand_qty - @xfer_quantity), 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE 
               inv_item_onhand_balances.item_id = @xfer_item_id AND 
               inv_item_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_item_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_item_onhand_balances.site_id = @xfer_site_id

         END
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
         */

         INSERT dbo.inv_txn_history(
            dbo.inv_txn_history.txn_type_name, 
            dbo.inv_txn_history.item_id, 
            dbo.inv_txn_history.serial_number, 
            dbo.inv_txn_history.from_group_id, 
            dbo.inv_txn_history.from_loc_id, 
            dbo.inv_txn_history.to_group_id, 
            dbo.inv_txn_history.to_loc_id, 
            dbo.inv_txn_history.comment, 
            dbo.inv_txn_history.txn_qty, 
            dbo.inv_txn_history.txn_uom_id, 
            dbo.inv_txn_history.created_by, 
            dbo.inv_txn_history.created_date, 
            dbo.inv_txn_history.updated_by, 
            dbo.inv_txn_history.updated_date, 
            dbo.inv_txn_history.site_id)
            VALUES (
               N'TRANSACTION_ERROR', 
               @xfer_item_id, 
               @xfer_serial, 
               @xfer_from_group_id, 
               @xfer_from_location_id, 
               @xfer_to_group_id, 
               @xfer_to_location_id, 
               @xfer_comment, 
               @xfer_quantity, 
               1, 
               @xfer_created_by, 
               isnull(@xfer_created_date, getdate()), 
               @xfer_updated_by, 
               isnull(@xfer_updated_date, getdate()), 
               @xfer_site_id)

      WHILE @@TRANCOUNT > 0
      
         COMMIT 

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_inv_xfer_txn
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*   M2SS0134: Conversion of following Comment(s) is not supported :  Variable Declaration 
*
*/

CREATE PROCEDURE [dbo].[proc_inv_xfer_txn]  
   @in_item nvarchar(50),
   @in_from_group nvarchar(50),
   @in_from_location nvarchar(50),
   @in_to_group nvarchar(50),
   @in_to_location nvarchar(50),
   @in_quantity int,
   @in_comment nvarchar(100),
   @in_site_id int,
   @in_user_id int/*,
   @xfer_status nvarchar(10)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @xfer_status = NULL*/

      DECLARE
         @xfer_item_id int

      DECLARE
         @xfer_from_group_id int

      DECLARE
         @xfer_from_location_id int

      DECLARE
         @xfer_to_group_id int

      DECLARE
         @xfer_to_location_id int

      DECLARE
         @xfer_site_id int

      DECLARE
         @xfer_created_date datetime2(0)

      DECLARE
         @xfer_updated_date datetime2(0)

      DECLARE
         @xfer_created_by int

      DECLARE
         @xfer_updated_by int

      DECLARE
         @xfer_quantity int

      DECLARE
         @current_from_onhand_qty int

      DECLARE
         @xfer_comment nvarchar(100)

      SET @xfer_site_id = @in_site_id

      SET @xfer_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item AND inv_item_master.site_id = @xfer_site_id
         )

      SET @xfer_from_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_from_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_from_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_from_location AND 
               inv_locations.group_id = @xfer_from_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_to_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_to_group AND inv_groups.site_id = @xfer_site_id
         )

      SET @xfer_to_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.location_name = @in_to_location AND 
               inv_locations.group_id = @xfer_to_group_id AND 
               inv_locations.site_id = @xfer_site_id
         )

      SET @xfer_created_date = getdate()

      SET @xfer_updated_date = getdate()

      SET @xfer_comment = @in_comment

      SET @xfer_created_by = @in_user_id

      SET @xfer_updated_by = @in_user_id

      SET @xfer_quantity = @in_quantity

      SET @current_from_onhand_qty = 
         (
            SELECT inv_item_onhand_balances.onhand_qty
            FROM dbo.inv_item_onhand_balances
            WHERE 
               inv_item_onhand_balances.item_id = @xfer_item_id AND 
               inv_item_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_item_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_item_onhand_balances.site_id = @xfer_site_id
         )

      IF @current_from_onhand_qty >= @xfer_quantity
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.from_group_id, 
               dbo.inv_txn_history.from_loc_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'INVENTORY_TRANSFER', 
                  @xfer_item_id, 
                  @xfer_from_group_id, 
                  @xfer_from_location_id, 
                  @xfer_to_group_id, 
                  @xfer_to_location_id, 
                  @xfer_quantity, 
                  1, 
                  @xfer_created_by, 
                  isnull(@xfer_created_date, getdate()), 
                  @xfer_updated_by, 
                  isnull(@xfer_updated_date, getdate()), 
                  @xfer_site_id, 
                  @xfer_comment)

            IF 
               (
                  SELECT count_big(*)
                  FROM dbo.inv_item_onhand_balances  AS iobc
                  WHERE 
                     iobc.item_id = @xfer_item_id AND 
                     iobc.group_id = @xfer_to_group_id AND 
                     iobc.location_id = @xfer_to_location_id
               ) = 1
               /* 
               *   SSMA error messages:
               *   M2SS0283: A column cannot be assigned more than one value in the same SET clause. 
               *    Modify the SET clause to make sure that a column is updated only once.

               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date

               UPDATE dbo.inv_item_onhand_balances
                  SET 
                     onhand_qty = (inv_item_onhand_balances.onhand_qty + @xfer_quantity), 
                     updated_date = isnull(@xfer_updated_date, getdate()), 
                     updated_by = @xfer_updated_by, 
                     updated_date = isnull(@xfer_updated_date, getdate())
               WHERE 
                  inv_item_onhand_balances.item_id = @xfer_item_id AND 
                  inv_item_onhand_balances.group_id = @xfer_to_group_id AND 
                  inv_item_onhand_balances.location_id = @xfer_to_location_id AND 
                  inv_item_onhand_balances.site_id = @xfer_site_id
               */


               DECLARE
                  @db_null_statement int
            ELSE 
               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               INSERT dbo.inv_item_onhand_balances(
                  dbo.inv_item_onhand_balances.item_id, 
                  dbo.inv_item_onhand_balances.group_id, 
                  dbo.inv_item_onhand_balances.location_id, 
                  dbo.inv_item_onhand_balances.onhand_qty, 
                  dbo.inv_item_onhand_balances.base_uom_id, 
                  dbo.inv_item_onhand_balances.created_by, 
                  dbo.inv_item_onhand_balances.created_date, 
                  dbo.inv_item_onhand_balances.updated_by, 
                  dbo.inv_item_onhand_balances.updated_date, 
                  dbo.inv_item_onhand_balances.site_id)
                  VALUES (
                     @xfer_item_id, 
                     @xfer_to_group_id, 
                     @xfer_to_location_id, 
                     @xfer_quantity, 
                     1, 
                     @xfer_created_by, 
                     isnull(@xfer_created_date, getdate()), 
                     @xfer_updated_by, 
                     isnull(@xfer_updated_date, getdate()), 
                     @xfer_site_id)

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.inv_item_onhand_balances
               SET 
                  onhand_qty = (inv_item_onhand_balances.onhand_qty - @xfer_quantity), 
                  updated_by = @xfer_updated_by, 
                  updated_date = isnull(@xfer_updated_date, getdate())
            WHERE 
               inv_item_onhand_balances.item_id = @xfer_item_id AND 
               inv_item_onhand_balances.group_id = @xfer_from_group_id AND 
               inv_item_onhand_balances.location_id = @xfer_from_location_id AND 
               inv_item_onhand_balances.site_id = @xfer_site_id

            /*SET @xfer_status = N'SUCCESS'*/

         END
      ELSE 
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.inv_txn_history(
               dbo.inv_txn_history.txn_type_name, 
               dbo.inv_txn_history.item_id, 
               dbo.inv_txn_history.from_group_id, 
               dbo.inv_txn_history.from_loc_id, 
               dbo.inv_txn_history.to_group_id, 
               dbo.inv_txn_history.to_loc_id, 
               dbo.inv_txn_history.txn_qty, 
               dbo.inv_txn_history.txn_uom_id, 
               dbo.inv_txn_history.created_by, 
               dbo.inv_txn_history.created_date, 
               dbo.inv_txn_history.updated_by, 
               dbo.inv_txn_history.updated_date, 
               dbo.inv_txn_history.site_id, 
               dbo.inv_txn_history.comment)
               VALUES (
                  N'TRANSACTION ERROR', 
                  @xfer_item_id, 
                  @xfer_from_group_id, 
                  @xfer_from_location_id, 
                  @xfer_to_group_id, 
                  @xfer_to_location_id, 
                  @xfer_quantity, 
                  1, 
                  @xfer_created_by, 
                  isnull(@xfer_created_date, getdate()), 
                  @xfer_updated_by, 
                  isnull(@xfer_updated_date, getdate()), 
                  @xfer_site_id, 
                  @xfer_comment)

            /*SET @xfer_status = N'ERROR'*/

         END

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_srm_function_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*/

CREATE PROCEDURE [dbo].[proc_srm_function_create]  
   @in_function_name nvarchar(60),
   @in_function_shortcode nvarchar(60),
   @in_description nvarchar(100),
   @in_enabled_flag int,
   @in_filename nvarchar(40)/*,
   @func_create_status nvarchar(20)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @func_create_status = NULL*/

      INSERT dbo.srm_functions(
         dbo.srm_functions.function_name, 
         dbo.srm_functions.function_shortcode, 
         dbo.srm_functions.description, 
         dbo.srm_functions.enabled_flag, 
         dbo.srm_functions.filename)
         VALUES (
            @in_function_name, 
            @in_function_shortcode, 
            @in_description, 
            @in_enabled_flag, 
            @in_filename)

      /*SET @func_create_status = N'Success!'*/

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_srm_menu_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   MODIFIES SQL DATA.
*/

CREATE PROCEDURE [dbo].[proc_srm_menu_create]  
   @in_menu_name nvarchar(60),
   @in_menu_shortcode nvarchar(60),
   @in_description nvarchar(100),
   @in_menu_type nvarchar(2)/*,
   @menu_create_status nvarchar(20)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @menu_create_status = NULL*/

      INSERT dbo.srm_menu_header(dbo.srm_menu_header.menu_name, dbo.srm_menu_header.menu_shortcode, dbo.srm_menu_header.menu_description, dbo.srm_menu_header.menu_type)
         VALUES (@in_menu_name, @in_menu_shortcode, @in_description, @in_menu_type)

     /* SET @menu_create_status = N'Success!'
	  SELECT @menu_create_status;*/
   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_srm_menu_line_create
DELIMITER //

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_srm_menu_line_create  
   @in_menu_header_id int,
   @in_object_type int,
   @in_object_id int/*,
   @srm_add_status nvarchar(30)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @srm_add_status = NULL*/

      DECLARE
         @srm_current_max_sequence int

      DECLARE
         @srm_sequence_next_value int

DECLARE @srm_add_status nvarchar(30)

      SET @srm_current_max_sequence = 
         (
            /*
            *   SSMA warning messages:
            *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
            *   M2SS0284: Numeric value is converted to string value
            */

            SELECT ISNULL(
               (
                  /*
                  *   SSMA warning messages:
                  *   M2SS0104: Non aggregated column MENU_HEADER_ID is aggregated with Min(..) in Select,Orderby and Having clauses.
                  *   M2SS0104: Non aggregated column MENU_HEADER_ID is aggregated with Min(..) in Select,Orderby and Having clauses.
                  */

                  SELECT max(srm_menu_lines.menu_line_sequence)
                  FROM dbo.srm_menu_lines
                  WHERE srm_menu_lines.menu_header_id = @in_menu_header_id
               ), N'0')
         )

      SET @srm_sequence_next_value = (@srm_current_max_sequence + 10) 

      IF (@in_object_type = 1)
         BEGIN

            INSERT dbo.srm_menu_lines(
               dbo.srm_menu_lines.menu_header_id, 
               dbo.srm_menu_lines.menu_line_sequence, 
               dbo.srm_menu_lines.menu_line_type_id, 
               dbo.srm_menu_lines.menu_line_submenu_id, 
               dbo.srm_menu_lines.menu_line_enabled_flag)
               VALUES (
                  @in_menu_header_id, 
                  @srm_sequence_next_value, 
                  @in_object_type, 
                  @in_object_id, 
                  1)

            SET @srm_add_status = N'Success!'

         END
      ELSE 
         IF (@in_object_type = 2)
            BEGIN

               INSERT dbo.srm_menu_lines(
                  dbo.srm_menu_lines.menu_header_id, 
                  dbo.srm_menu_lines.menu_line_sequence, 
                  dbo.srm_menu_lines.menu_line_type_id, 
                  dbo.srm_menu_lines.menu_line_function_id, 
                  dbo.srm_menu_lines.menu_line_enabled_flag)
                  VALUES (
                     @in_menu_header_id, 
                     @srm_sequence_next_value, 
                     @in_object_type, 
                     @in_object_id, 
                     1)

               SET @srm_add_status = N'Success!'

            END
         ELSE 
            SET @srm_add_status = N'Error!'

   END
//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_srm_person_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE [dbo].[proc_srm_person_create]  
   @in_lastname nvarchar(50),
   @in_firstname nvarchar(50),
   @in_middlename nvarchar(50),
   @in_email nvarchar(50),
   @in_phone nvarchar(50),
   @in_address nvarchar(50),
   @in_city nvarchar(50),
   @in_state nvarchar(50),
   @in_postal nvarchar(50)/*,
   @create_status nvarchar(10)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @create_status = NULL*/

      DECLARE
         @reg_last_name nvarchar(50)

      DECLARE
         @reg_first_name nvarchar(50)

      DECLARE
         @reg_middle_name nvarchar(50)

      DECLARE
         @reg_email nvarchar(50)

      DECLARE
         @reg_phone nvarchar(20)

      DECLARE
         @reg_address nvarchar(50)

      DECLARE
         @reg_city nvarchar(20)

      DECLARE
         @reg_state nvarchar(20)

      DECLARE
         @reg_postal nvarchar(20)

      DECLARE
         @reg_created_date datetime2(0)

      DECLARE
         @reg_updated_date datetime2(0)

      DECLARE
         @reg_created_by int

      DECLARE
         @reg_updated_by int

      SET @reg_last_name = @in_lastname

      SET @reg_first_name = @in_firstname

      SET @reg_middle_name = @in_middlename

      SET @reg_email = @in_email

      SET @reg_phone = @in_phone

      SET @reg_address = @in_address

      SET @reg_city = @in_city

      SET @reg_state = @in_state

      SET @reg_postal = @in_postal

      SET @reg_created_date = getdate()

      SET @reg_updated_date = getdate()

      SET @reg_created_by = 1

      SET @reg_updated_by = 1

      IF 
         (
            SELECT count_big(*)
            FROM dbo.srm_persons
            WHERE srm_persons.last_name = @reg_last_name AND srm_persons.first_name = @reg_first_name
         ) = 0
         BEGIN

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            INSERT dbo.srm_persons(
               dbo.srm_persons.last_name, 
               dbo.srm_persons.first_name, 
               dbo.srm_persons.middle_name, 
               dbo.srm_persons.email_address, 
               dbo.srm_persons.telephone_number, 
               dbo.srm_persons.street_address, 
               dbo.srm_persons.city, 
               dbo.srm_persons.state, 
               dbo.srm_persons.postal_code, 
               dbo.srm_persons.created_by, 
               dbo.srm_persons.created_date, 
               dbo.srm_persons.updated_by, 
               dbo.srm_persons.updated_date)
               VALUES (
                  @reg_last_name, 
                  @reg_first_name, 
                  @reg_middle_name, 
                  @reg_email, 
                  @reg_phone, 
                  @reg_address, 
                  @reg_city, 
                  @reg_state, 
                  CAST(@reg_postal AS bigint), 
                  @reg_created_by, 
                  isnull(@reg_created_date, getdate()), 
                  @reg_updated_by, 
                  isnull(@reg_updated_date, getdate()))

            /*SET @create_status = N'Success!'*/
			
         END
      /*ELSE 
         SET @create_status = N'Person already exists'*/

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_srm_user_resp_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE [dbo].[proc_srm_user_resp_create]  
   @in_user_id int,
   @in_responsibility_id int/*,
   @srm_add_status nvarchar(30)  OUTPUT*/
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      /*SET @srm_add_status = NULL*/

      DECLARE
         @srm_updated_date datetime2(0)

      DECLARE
         @srm_created_date datetime2(0)

      DECLARE
         @srm_created_by int

      DECLARE
         @srm_updated_by int

      SET @srm_updated_date = getdate()

      SET @srm_created_date = getdate()

      SET @srm_created_by = 1

      SET @srm_updated_by = 1

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.srm_user_resp_assignments(
         dbo.srm_user_resp_assignments.user_id, 
         dbo.srm_user_resp_assignments.responsibility_id, 
         dbo.srm_user_resp_assignments.created_by, 
         dbo.srm_user_resp_assignments.created_date, 
         dbo.srm_user_resp_assignments.updated_by, 
         dbo.srm_user_resp_assignments.updated_date)
         VALUES (
            @in_user_id, 
            @in_responsibility_id, 
            @srm_created_by, 
            isnull(@srm_created_date, getdate()), 
            @srm_updated_by, 
            isnull(@srm_updated_date, getdate()))

      /*SET @srm_add_status = N'Success!'*/

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_header_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_header_create  
   @in_name nvarchar(200),
   @in_location nvarchar(200),
   @in_scheduled_date date,
   @in_priority int,
   @in_status int,
   @in_site_id int,
   @wo_create_status nvarchar(30)  OUTPUT,
   @wo_header_number int  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_header_number = NULL

      SET @wo_create_status = NULL

      DECLARE
         @woc_deliver_to_name nvarchar(200)

      DECLARE
         @woc_deliver_to_location nvarchar(200)

      DECLARE
         @woc_scheduled_date date

      DECLARE
         @woc_header_number int

      DECLARE
         @woc_site_id int

      DECLARE
         @woc_status int

      DECLARE
         @woc_priority int

      DECLARE
         @woc_created_date datetime2(0)

      DECLARE
         @woc_created_by int

      DECLARE
         @woc_updated_date datetime2(0)

      DECLARE
         @woc_updated_by int

      SET @woc_deliver_to_name = @in_name

      SET @woc_deliver_to_location = @in_location

      SET @woc_scheduled_date = @in_scheduled_date

      SET @woc_created_date = getdate()

      SET @woc_updated_date = getdate()

      SET @woc_created_by = 1

      SET @woc_updated_by = 1

      SET @woc_status = @in_status

      SET @woc_site_id = @in_site_id

      SET @woc_header_number = 
         (
            SELECT wo_header_number_values.next_order
            FROM dbo.wo_header_number_values 
               WITH ( UPDLOCK )
         )

      UPDATE dbo.wo_header_number_values
         SET 
            next_order = wo_header_number_values.next_order + 1

      SET @woc_priority = @in_priority

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.wo_work_order_header(
         dbo.wo_work_order_header.work_order_number, 
         dbo.wo_work_order_header.wo_site_id, 
         dbo.wo_work_order_header.deliver_to_name, 
         dbo.wo_work_order_header.deliver_to_location, 
         dbo.wo_work_order_header.scheduled_date, 
         dbo.wo_work_order_header.status, 
         dbo.wo_work_order_header.priority, 
         dbo.wo_work_order_header.created_date, 
         dbo.wo_work_order_header.created_by, 
         dbo.wo_work_order_header.updated_date, 
         dbo.wo_work_order_header.updated_by)
         VALUES (
            @woc_header_number, 
            @woc_site_id, 
            @woc_deliver_to_name, 
            @woc_deliver_to_location, 
            isnull(@woc_scheduled_date, getdate()), 
            CAST(@woc_status AS varchar(50)), 
            CAST(@woc_priority AS varchar(50)), 
            isnull(@woc_created_date, getdate()), 
            @woc_created_by, 
            isnull(@woc_updated_date, getdate()), 
            @woc_updated_by)

      SET @wo_header_number = @woc_header_number

      SET @wo_create_status = N'Success!'

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_line_backorder
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_line_backorder  
   @in_wo_line_id int,
   @in_user_id int,
   @in_site_id int,
   @wo_line_bo_status nvarchar(50)  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_line_bo_status = NULL

      DECLARE
         @wol_pick_line_id int

      DECLARE
         @wol_pick_header_id int

      DECLARE
         @wol_pick_header_number nvarchar(25)

      DECLARE
         @wol_pick_updated_by int

      DECLARE
         @wol_pick_updated_date datetime2(0)

      DECLARE
         @wol_pick_released_quantity int

      DECLARE
         @wol_pick_line_quantity int

      DECLARE
         @wol_pick_picked_quantity int

      DECLARE
         @wol_pick_remaining_quantity int

      DECLARE
         @wol_pick_current_onhand int

      DECLARE
         @wol_pick_order_complete int

      SET @wol_pick_line_id = @in_wo_line_id

      SET @wol_pick_header_id = 
         (
            SELECT wo_work_order_lines.wo_line_header_id
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_header_number = 
         (
            SELECT CAST(wo_work_order_header.work_order_number AS varchar(50))
            FROM dbo.wo_work_order_header
            WHERE wo_work_order_header.work_order_header_id = @wol_pick_header_id
         )

      SET @wol_pick_updated_date = getdate()

      SET @wol_pick_updated_by = @in_user_id

      SET @wol_pick_released_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_released_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_line_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_picked_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_picked_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_remaining_quantity = (@wol_pick_released_quantity - @wol_pick_picked_quantity)

      IF @wol_pick_line_quantity = @wol_pick_remaining_quantity
         BEGIN

            SELECT 
               wol.wo_line_status, 
               wol.wo_line_backordered_quantity, 
               wol.wo_line_released_quantity, 
               wol.updated_date, 
               wol.updated_by
            FROM dbo.wo_work_order_lines  AS wol 
               WITH ( UPDLOCK )
            WHERE wol.wo_line_id = @wol_pick_line_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.wo_work_order_lines
               SET 
                  wo_line_status = 
                     (
                        SELECT sys_code_meanings.meaning_code
                        FROM dbo.sys_code_meanings
                        WHERE sys_code_meanings.meaning_type = 'WOLINESTATUS' AND sys_code_meanings.meaning_name = 'Backordered'
                     ), 
                  wo_line_released_quantity = 0, 
                  wo_line_backordered_quantity = @wol_pick_line_quantity, 
                  updated_date = isnull(@wol_pick_updated_date, getdate()), 
                  updated_by = @wol_pick_updated_by
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id

            SET @wo_line_bo_status = N'Success!'

         END
      ELSE 
         IF @wol_pick_remaining_quantity < @wol_pick_line_quantity
            BEGIN

               SELECT 
                  wol.wo_line_status, 
                  wol.wo_line_backordered_quantity, 
                  wol.wo_line_released_quantity, 
                  wol.updated_date, 
                  wol.updated_by
               FROM dbo.wo_work_order_lines  AS wol 
                  WITH ( UPDLOCK )
               WHERE wol.wo_line_id = @wol_pick_line_id

               /*
               *   SSMA informational messages:
               *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
               */

               UPDATE dbo.wo_work_order_lines
                  SET 
                     wo_line_status = 
                        (
                           SELECT sys_code_meanings.meaning_code
                           FROM dbo.sys_code_meanings
                           WHERE sys_code_meanings.meaning_type = 'WOLINESTATUS' AND sys_code_meanings.meaning_name = 'Partially Picked'
                        ), 
                     wo_line_released_quantity = @wol_pick_picked_quantity, 
                     wo_line_backordered_quantity = @wol_pick_remaining_quantity, 
                     updated_date = isnull(@wol_pick_updated_date, getdate()), 
                     updated_by = @wol_pick_updated_by
               WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id

               SET @wo_line_bo_status = N'Success!'

            END
         ELSE 
            SET @wo_line_bo_status = N'Error!'

      SET @wol_pick_order_complete = 
         (
            SELECT count_big(*)
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_header_id = @wol_pick_header_id AND wo_work_order_lines.wo_line_status NOT IN ( 3, 6 )
         )

      IF @wol_pick_order_complete = 0
         BEGIN

            SELECT woh.status, woh.updated_date, woh.updated_date
            FROM dbo.wo_work_order_header  AS woh 
               WITH ( UPDLOCK )
            WHERE woh.work_order_header_id = @wol_pick_header_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.wo_work_order_header
               SET 
                  status = 
                     (
                        SELECT CAST(sys_code_meanings.meaning_code AS varchar(50))
                        FROM dbo.sys_code_meanings
                        WHERE sys_code_meanings.meaning_type = 'WOHEADERSTATUS' AND sys_code_meanings.meaning_name = 'Picked Full'
                     ), 
                  updated_date = isnull(@wol_pick_updated_date, getdate()), 
                  updated_by = @wol_pick_updated_by
            WHERE wo_work_order_header.work_order_header_id = @wol_pick_header_id

         END

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_line_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_line_create  
   @in_item nvarchar(200),
   @in_order_number int,
   @in_qty int,
   @in_site_id int,
   @wo_add_status nvarchar(30)  OUTPUT,
   @wo_header_number int  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_header_number = NULL

      SET @wo_add_status = NULL

      DECLARE
         @wolc_item_id nvarchar(200)

      DECLARE
         @wolc_order_id int

      DECLARE
         @wolc_header_number int

      DECLARE
         @wolc_site_id int

      DECLARE
         @wolc_qty int

      DECLARE
         @wolc_uom_id int

      DECLARE
         @wolc_status int

      DECLARE
         @wolc_created_date datetime2(0)

      DECLARE
         @wolc_created_by int

      DECLARE
         @wolc_updated_date datetime2(0)

      DECLARE
         @wolc_updated_by int

      SET @wolc_item_id = 
         (
            SELECT CAST(inv_item_master.item_id AS varchar(50))
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item
         )

      SET @wolc_order_id = 
         (
            SELECT wo_work_order_header.work_order_header_id
            FROM dbo.wo_work_order_header
            WHERE wo_work_order_header.work_order_number = @in_order_number
         )

      SET @wolc_header_number = @in_order_number

      SET @wolc_qty = @in_qty

      SET @wolc_uom_id = 
         (
            SELECT inv_item_master.base_uom_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item
         )

      SET @wolc_status = 1

      SET @wolc_created_date = getdate()

      SET @wolc_updated_date = getdate()

      SET @wolc_created_by = 1

      SET @wolc_updated_by = 1

      SET @wolc_site_id = @in_site_id

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      INSERT dbo.wo_work_order_lines(
         dbo.wo_work_order_lines.wo_line_header_id, 
         dbo.wo_work_order_lines.wo_line_item_id, 
         dbo.wo_work_order_lines.wo_line_quantity, 
         dbo.wo_work_order_lines.wo_line_uom_id, 
         dbo.wo_work_order_lines.wo_line_site_id, 
         dbo.wo_work_order_lines.wo_line_status, 
         dbo.wo_work_order_lines.created_date, 
         dbo.wo_work_order_lines.created_by, 
         dbo.wo_work_order_lines.updated_date, 
         dbo.wo_work_order_lines.updated_by)
         VALUES (
            @wolc_order_id, 
            CAST(@wolc_item_id AS bigint), 
            @wolc_qty, 
            @wolc_uom_id, 
            @wolc_site_id, 
            @wolc_status, 
            isnull(@wolc_created_date, getdate()), 
            @wolc_created_by, 
            isnull(@wolc_updated_date, getdate()), 
            @wolc_updated_by)

      SET @wo_header_number = @wolc_header_number

      SET @wo_add_status = N'Success!'

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_line_edit
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_line_edit  
   @in_wo_line_id int,
   @in_item nvarchar(50),
   @in_qty int,
   @in_user_id int,
   @in_status int,
   @wo_line_edit_status nvarchar(50)  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_line_edit_status = NULL

      DECLARE
         @wol_edit_line_id int

      DECLARE
         @wol_edit_item_id int

      DECLARE
         @wol_edit_quantity int

      DECLARE
         @wol_edit_updated_by int

      DECLARE
         @wol_edit_updated_date datetime2(0)

      DECLARE
         @wol_edit_status int

      SET @wol_edit_line_id = @in_wo_line_id

      SET @wol_edit_item_id = 
         (
            SELECT inv_item_master.item_id
            FROM dbo.inv_item_master
            WHERE inv_item_master.item_name = @in_item
         )

      SET @wol_edit_quantity = @in_qty

      SET @wol_edit_status = @in_status

      SET @wol_edit_updated_date = getdate()

      SET @wol_edit_updated_by = @in_user_id

      /*
      *   SSMA warning messages:
      *   M2SS0219: Converted operator may not work exactly the same as in MySQL
      */

      IF @wol_edit_status IN ( 1, 4 )
         BEGIN

            SELECT wol.wo_line_item_id, wol.wo_line_quantity, wol.updated_by, wol.updated_date
            FROM dbo.wo_work_order_lines  AS wol 
               WITH ( UPDLOCK )
            WHERE wol.wo_line_id = @wol_edit_line_id

            /*
            *   SSMA informational messages:
            *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
            */

            UPDATE dbo.wo_work_order_lines
               SET 
                  wo_line_item_id = @wol_edit_item_id, 
                  wo_line_quantity = @wol_edit_quantity, 
                  updated_by = @wol_edit_updated_by, 
                  updated_date = isnull(@wol_edit_updated_date, getdate())
            WHERE wo_work_order_lines.wo_line_id = @wol_edit_line_id

            SET @wo_line_edit_status = N'Success!'

         END
      ELSE 
         SET @wo_line_edit_status = N'Error - no updates allowed'

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_line_pick_confirm
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_line_pick_confirm  
   @in_wo_line_id int,
   @in_from_group nvarchar(50),
   @in_from_location nvarchar(50),
   @in_quantity int,
   @in_user_id int,
   @in_site_id int,
   @wo_line_pick_status nvarchar(50)  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_line_pick_status = NULL

      DECLARE
         @wol_pick_line_id int

      DECLARE
         @wol_pick_item_id int

      DECLARE
         @wol_pick_quantity int

      DECLARE
         @wol_pick_from_group_id int

      DECLARE
         @wol_pick_from_location_id int

      DECLARE
         @wol_pick_header_id int

      DECLARE
         @wol_pick_header_number nvarchar(25)

      DECLARE
         @wol_pick_updated_by int

      DECLARE
         @wol_pick_updated_date datetime2(0)

      DECLARE
         @wol_pick_released_quantity int

      DECLARE
         @wol_pick_line_quantity int

      DECLARE
         @wol_pick_picked_quantity int

      DECLARE
         @wol_pick_remaining_quantity int

      DECLARE
         @wol_pick_uom_id int

      DECLARE
         @wol_pick_current_onhand int

      DECLARE
         @wol_pick_order_complete int

      SET @wol_pick_line_id = @in_wo_line_id

      SET @wol_pick_item_id = 
         (
            SELECT wo_work_order_lines.wo_line_item_id
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_from_group_id = 
         (
            SELECT inv_groups.group_id
            FROM dbo.inv_groups
            WHERE inv_groups.group_name = @in_from_group AND inv_groups.site_id = @in_site_id
         )

      SET @wol_pick_from_location_id = 
         (
            SELECT inv_locations.location_id
            FROM dbo.inv_locations
            WHERE 
               inv_locations.group_id = @wol_pick_from_group_id AND 
               inv_locations.location_name = @in_from_location AND 
               inv_locations.site_id = @in_site_id
         )

      SET @wol_pick_quantity = @in_quantity

      SET @wol_pick_header_id = 
         (
            SELECT wo_work_order_lines.wo_line_header_id
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_header_number = 
         (
            SELECT CAST(wo_work_order_header.work_order_number AS varchar(50))
            FROM dbo.wo_work_order_header
            WHERE wo_work_order_header.work_order_header_id = @wol_pick_header_id
         )

      SET @wol_pick_uom_id = 
         (
            SELECT wo_work_order_lines.wo_line_uom_id
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_updated_date = getdate()

      SET @wol_pick_updated_by = @in_user_id

      SET @wol_pick_released_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_released_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_line_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_picked_quantity = 
         (
            SELECT wo_work_order_lines.wo_line_picked_quantity
            FROM dbo.wo_work_order_lines
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id
         )

      SET @wol_pick_remaining_quantity = (@wol_pick_released_quantity - @wol_pick_picked_quantity)

      SET @wol_pick_current_onhand = 
         (
            SELECT iob.onhand_qty
            FROM dbo.inv_item_onhand_balances  AS iob
            WHERE 
               iob.item_id = @wol_pick_item_id AND 
               iob.group_id = @wol_pick_from_group_id AND 
               iob.location_id = @wol_pick_from_location_id AND 
               iob.site_id = @in_site_id
         )

      IF @wol_pick_quantity <= @wol_pick_remaining_quantity
         BEGIN

            SELECT wol.wo_line_picked_quantity
            FROM dbo.wo_work_order_lines  AS wol 
               WITH ( UPDLOCK )
            WHERE wol.wo_line_id = @wol_pick_line_id

            UPDATE dbo.wo_work_order_lines
               SET 
                  wo_line_picked_quantity = wo_work_order_lines.wo_line_picked_quantity + @wol_pick_quantity
            WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id

            IF @wol_pick_current_onhand >= @wol_pick_quantity
               BEGIN

                  SELECT iob.onhand_qty, iob.updated_by, iob.updated_date
                  FROM dbo.inv_item_onhand_balances  AS iob 
                     WITH ( UPDLOCK )
                  WHERE 
                     iob.item_id = @wol_pick_item_id AND 
                     iob.group_id = @wol_pick_from_group_id AND 
                     iob.location_id = @wol_pick_from_location_id AND 
                     iob.site_id = @in_site_id

                  /*
                  *   SSMA informational messages:
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  */

                  UPDATE dbo.inv_item_onhand_balances
                     SET 
                        onhand_qty = (inv_item_onhand_balances.onhand_qty - @wol_pick_quantity), 
                        updated_by = @wol_pick_updated_by, 
                        updated_date = isnull(@wol_pick_updated_date, getdate())
                  WHERE 
                     inv_item_onhand_balances.item_id = @wol_pick_item_id AND 
                     inv_item_onhand_balances.group_id = @wol_pick_from_group_id AND 
                     inv_item_onhand_balances.location_id = @wol_pick_from_location_id AND 
                     inv_item_onhand_balances.site_id = @in_site_id

                  /*
                  *   SSMA informational messages:
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  */

                  INSERT dbo.inv_txn_history(
                     dbo.inv_txn_history.txn_type_name, 
                     dbo.inv_txn_history.item_id, 
                     dbo.inv_txn_history.from_group_id, 
                     dbo.inv_txn_history.from_loc_id, 
                     dbo.inv_txn_history.source_wo_header_id, 
                     dbo.inv_txn_history.source_wo_header_number, 
                     dbo.inv_txn_history.source_wo_line_id, 
                     dbo.inv_txn_history.txn_qty, 
                     dbo.inv_txn_history.txn_uom_id, 
                     dbo.inv_txn_history.site_id, 
                     dbo.inv_txn_history.created_by, 
                     dbo.inv_txn_history.created_date, 
                     dbo.inv_txn_history.updated_by, 
                     dbo.inv_txn_history.updated_date)
                     VALUES (
                        N'WO_PICK_TXN', 
                        @wol_pick_item_id, 
                        @wol_pick_from_group_id, 
                        @wol_pick_from_location_id, 
                        @wol_pick_header_id, 
                        @wol_pick_header_number, 
                        @wol_pick_line_id, 
                        @wol_pick_quantity, 
                        @wol_pick_uom_id, 
                        @in_site_id, 
                        @wol_pick_updated_by, 
           isnull(@wol_pick_updated_date, getdate()), 
                        @wol_pick_updated_by, 
                        isnull(@wol_pick_updated_date, getdate()))

               END

            IF (@wol_pick_quantity + @wol_pick_picked_quantity) = @wol_pick_line_quantity
               BEGIN

                  SELECT wol.wo_line_status
                  FROM dbo.wo_work_order_lines  AS wol 
                     WITH ( UPDLOCK )
                  WHERE wol.wo_line_id = @wol_pick_line_id

                  UPDATE dbo.wo_work_order_lines
                     SET 
                        wo_line_status = 
                           (
                              SELECT sys_code_meanings.meaning_code
                              FROM dbo.sys_code_meanings
                              WHERE sys_code_meanings.meaning_type = 'WOLINESTATUS' AND sys_code_meanings.meaning_name = 'Picked'
                           )
                  WHERE wo_work_order_lines.wo_line_id = @wol_pick_line_id

               END

            SET @wol_pick_order_complete = 
               (
                  SELECT count_big(*)
                  FROM dbo.wo_work_order_lines
                  WHERE wo_work_order_lines.wo_line_header_id = @wol_pick_header_id AND wo_work_order_lines.wo_line_status <> 3
               )

            IF @wol_pick_order_complete = 0
               BEGIN

                  SELECT woh.status, woh.updated_date, woh.updated_date
                  FROM dbo.wo_work_order_header  AS woh 
                     WITH ( UPDLOCK )
                  WHERE woh.work_order_header_id = @wol_pick_header_id

                  /*
                  *   SSMA informational messages:
                  *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
                  */

                  UPDATE dbo.wo_work_order_header
                     SET 
                        status = 
                           (
                              SELECT CAST(sys_code_meanings.meaning_code AS varchar(50))
                              FROM dbo.sys_code_meanings
                              WHERE sys_code_meanings.meaning_type = 'WOHEADERSTATUS' AND sys_code_meanings.meaning_name = 'Picked Full'
                           ), 
                        updated_date = isnull(@wol_pick_updated_date, getdate()), 
                        updated_by = @wol_pick_updated_by
                  WHERE wo_work_order_header.work_order_header_id = @wol_pick_header_id

               END

            SET @wo_line_pick_status = N'Success!'

         END
      ELSE 
         SET @wo_line_pick_status = N'Error!'

   END//
DELIMITER ;


-- Dumping structure for procedure mod43fordpoc.proc_wo_release_create
DELIMITER //
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*/

CREATE PROCEDURE dbo.proc_wo_release_create  
   @in_order_number int,
   @in_site_id int,
   @wo_release_status nvarchar(30)  OUTPUT,
   @wo_header_number int  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @wo_header_number = NULL

      SET @wo_release_status = NULL

      DECLARE
         @wohr_order_id int

      DECLARE
         @wohr_status int

      DECLARE
         @wol_status int

      DECLARE
         @wohr_header_number int

      DECLARE
         @wohr_updated_date datetime2(0)

      DECLARE
         @wohr_updated_by int

      SET @wohr_order_id = 
         (
            SELECT woh.work_order_header_id
            FROM dbo.wo_work_order_header  AS woh
            WHERE woh.work_order_number = @in_order_number AND woh.wo_site_id = @in_site_id
         )

      SET @wohr_header_number = @in_order_number

      SET @wohr_status = 2

      SET @wol_status = 2

      SET @wohr_updated_date = getdate()

      SET @wohr_updated_by = 1

      SELECT woh.status, woh.updated_date, woh.updated_by
      FROM dbo.wo_work_order_header  AS woh 
         WITH ( UPDLOCK )
      WHERE woh.work_order_header_id = @wohr_order_id

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      UPDATE dbo.wo_work_order_header
         SET 
            status = CAST(@wohr_status AS varchar(50)), 
            updated_by = @wohr_updated_by, 
            updated_date = isnull(@wohr_updated_date, getdate())
      WHERE wo_work_order_header.work_order_header_id = @wohr_order_id

      SELECT wol.wo_line_status, wol.wo_line_released_quantity, wol.updated_date, wol.updated_by
      FROM dbo.wo_work_order_lines  AS wol 
         WITH ( UPDLOCK )
      WHERE wol.wo_line_header_id = @wohr_order_id

      /*
      *   SSMA informational messages:
      *   M2SS0231: Zero-date, zero-in-date and invalid dates to not null columns has been replaced with GetDate()/Constant date
      */

      UPDATE dbo.wo_work_order_lines
         SET 
            wo_line_status = @wol_status, 
            wo_line_released_quantity = wo_work_order_lines.wo_line_quantity, 
            updated_by = @wohr_updated_by, 
            updated_date = isnull(@wohr_updated_date, getdate())
      WHERE wo_work_order_lines.wo_line_header_id = @wohr_order_id

      SET @wo_release_status = N'Success!'

      SET @wo_header_number = @wohr_header_number

   END//
DELIMITER ;


-- Dumping structure for table mod43fordpoc.srm_functions
CREATE TABLE IF NOT EXISTS "srm_functions" (
	"function_id" INT(10,0) NOT NULL,
	"function_name" NVARCHAR(50) NOT NULL,
	"function_shortcode" NVARCHAR(30) NOT NULL,
	"enabled_flag" INT(10,0) NOT NULL,
	"description" NVARCHAR(150) NOT NULL,
	"filename" NVARCHAR(60) NOT NULL,
	PRIMARY KEY ("function_id")
);

-- Dumping data for table mod43fordpoc.srm_functions: 31 rows
DELETE FROM "srm_functions";
/*!40000 ALTER TABLE "srm_functions" DISABLE KEYS */;
INSERT INTO "srm_functions" ("function_id", "function_name", "function_shortcode", "enabled_flag", "description", "filename") VALUES
	(66, 'Create Functions', 'SRMFUNCTIONCREATE', 1, 'Used to create functions', 'UP_SRM_FUNCTION_CREATE.php'),
	(67, 'Create User', 'SRMUSERS', 1, 'User Account Creation', 'UP_SRM_USERS.php'),
	(68, 'Create Person', 'SRMPEOPLE', 1, 'Used to register people', 'UP_SRM_PERSONS.php'),
	(69, 'User Responsibility Assignment', 'SRMUSERRESPCREATE', 1, 'Assign Responsibiiltes to Users', 'UP_SRM_USER_RESP_CREATE.php'),
	(70, 'Create Responsibilities', 'SRMRESPCREATE', 1, 'Create Responsibilities', 'UP_SRM_RESP_CREATE.php'),
	(71, 'Responsibilities Listing', 'SRMRESPSVIEW', 1, 'Listing of all Responsibilities', 'UP_SRM_RESPS_VIEW.php'),
	(72, 'User Person Assignment', 'SRMUSERPERSONCREATE', 1, 'Assign Person to Username', 'UP_SRM_USER_PERSON_CREATE.php'),
	(73, 'Create Menu Lines', 'SRMMENULINESCREATE', 1, 'Create/Add lines to a menu', 'UP_SRM_MENU_LINES_CREATE.php'),
	(74, 'Create Menu', 'SRMMENUCREATE', 1, 'Create Menu Structures', 'UP_SRM_MENU_HEADER_CREATE.php'),
	(75, 'Menu Listing', 'SRMMENUSVIEW', 1, 'Listing of all menus', 'UP_SRM_MENUS_VIEW.php'),
	(76, 'View Functions', 'SRMFUNCTIONVIEW', 1, 'View Function Listing', 'UP_SRM_FUNCTION_VIEW.php'),
	(77, 'Edit Function', 'SRMFUNCTIONEDIT', 1, 'Edit Functions', 'UP_SRM_FUNCTION_EDIT.php'),
	(78, 'Create Locations', 'INVLOCATIONCREATE', 1, 'Create Inventory Locations', 'UP_INV_LOC_CREATE.php'),
	(79, 'Create Group', 'INVGROUPCREATE', 1, 'Used to create inventory groups', 'UP_INV_GROUP_CREATE.php'),
	(80, 'Create Site', 'INVSITECREATE', 1, 'Create Inventory Site', 'UP_INV_SITE_CREATE.php'),
	(81, 'Persons Listing', 'SRMPERSONSVIEW', 1, 'Persons Listing', 'UP_SRM_PERSONS_VIEW.php'),
	(82, 'User Listing', 'SRMUSERSVIEW', 1, 'Listing of Users', 'UP_SRM_USERS_VIEW.php'),
	(83, 'Item On-hand Balances', 'INVONHANDVIEW', 1, 'View onhand blances for the site', 'UP_INV_ONHAND_VIEW.php'),
	(84, 'User Password Reset', 'SRMPASSWORDRESET', 1, 'User Password Reset', 'UP_SRM_USER_PASSWORD_RESET.php'),
	(85, 'Item Master List', 'INVITEMSVIEW', 1, 'Item Master List', 'UP_INV_ITEMS_VIEW.php'),
	(86, 'Create Equipment', 'INVEQUIPMENTCREATE', 1, 'Create Equipment', 'UP_INV_EQUIPMENT_CREATE.php'),
	(87, 'View Equipment', 'INVEQUIPMENTVIEW', 1, 'View Equipment', 'UP_INV_EQUIPMENT_VIEW.php'),
	(88, 'Item Receipt', 'INVITEMRECEIPTSERIAL', 1, 'Inventory Receipt with Serial Support', 'UP_INV_ITEM_RCPT_SERIAL.php'),
	(89, 'View Inventory Transaction History', 'INVTXNHISTORYVIEW', 1, 'View Inventory Transaction History', 'UP_INV_TXN_HISTORY_VIEW.php'),
	(90, 'Item Issue', 'INVITEMISSUESERIAL', 1, 'Item Issue with Serial', 'UP_INV_ITEM_ISSUE_SERIAL.php'),
	(91, 'Item Transfer', 'INVXFERSERIAL', 1, 'Inventory Transfer Serial', 'UP_INV_XFER_TXN_SERIAL.php'),
	(92, 'Create Item', 'INVITEMCREATE', 1, 'Create Inventory Items', 'UP_INV_ITEM_CREATE.php'),
	(93, 'Equipment Receipt Serial', 'INVEQPRECEIPTSERIAL', 1, 'Equipment Receipt Serial', 'UP_INV_EQUIPMENT_RCPT_SERIAL.php'),
	(94, 'Equipment Calibration Transaction', 'INVEQPCALTXN', 1, 'Equipment Calibration Transaction', 'UP_INV_EQUIPMENT_CALIBRATION_TXN.php'),
	(95, 'Equipment Issue Serial', 'INVEQPISSUESERIAL', 1, 'Equipment Issue Serial', 'UP_INV_EQUIPMENT_ISSUE_SERIAL.php'),
	(96, 'Equipment Transfer Serial', 'INVEQPXFERSERIAL', 1, 'Equipment Transfer Serial', 'UP_INV_EQUIPMENT_XFER_TXN_SERIAL.php');
/*!40000 ALTER TABLE "srm_functions" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_menu_header
CREATE TABLE IF NOT EXISTS "srm_menu_header" (
	"menu_header_id" INT(10,0) NOT NULL,
	"menu_name" NVARCHAR(50) NOT NULL,
	"menu_description" NVARCHAR(80) NOT NULL,
	"menu_shortcode" NVARCHAR(20) NOT NULL,
	"menu_type" NVARCHAR(1) NOT NULL,
	PRIMARY KEY ("menu_header_id")
);

-- Dumping data for table mod43fordpoc.srm_menu_header: 6 rows
DELETE FROM "srm_menu_header";
/*!40000 ALTER TABLE "srm_menu_header" DISABLE KEYS */;
INSERT INTO "srm_menu_header" ("menu_header_id", "menu_name", "menu_description", "menu_shortcode", "menu_type") VALUES
	(28, 'System Administration', 'All Functions', 'SYSADMIN', 'M'),
	(1028, 'Personnel Setup', 'Create Users, Persons, Responsibilities', 'STAFFSETUP', 'S'),
	(1029, 'System Design', 'Edit Menus and Functions', 'ADMIN', 'S'),
	(1033, 'Site Setup', 'Create sites, groups, locations, equipment and inventory', 'SITESETUP', 'S'),
	(1034, 'Item Transactions', 'Inventory Transactions for issuing and receiving materials', 'INV_ADJ', 'S'),
	(1035, 'Equipment Menu', 'Menu for Equipment Setups and Transactions', 'EQUIPMENTMENU', 'S');
/*!40000 ALTER TABLE "srm_menu_header" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_menu_lines
CREATE TABLE IF NOT EXISTS "srm_menu_lines" (
	"menu_line_id" INT(10,0) NOT NULL,
	"menu_header_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"menu_line_sequence" INT(10,0) NOT NULL DEFAULT ((-999)),
	"menu_line_type_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"menu_line_function_id" INT(10,0) NULL DEFAULT (NULL),
	"menu_line_submenu_id" INT(10,0) NULL DEFAULT (NULL),
	"menu_line_enabled_flag" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("menu_line_id")
);

-- Dumping data for table mod43fordpoc.srm_menu_lines: 38 rows
DELETE FROM "srm_menu_lines";
/*!40000 ALTER TABLE "srm_menu_lines" DISABLE KEYS */;
INSERT INTO mod43fordpoc.dbo.srm_menu_lines ("menu_line_id", "menu_header_id", "menu_line_sequence", "menu_line_type_id", "menu_line_function_id", "menu_line_submenu_id", "menu_line_enabled_flag") VALUES
	(180, 27, 10, 2, 1, NULL, 1),
	(185, 1028, 10, 2, 67, NULL, 1),
	(186, 28, 10, 1, NULL, 1028, 1),
	(188, 28, 20, 1, NULL, 1029, 1),
	(189, 1029, 10, 2, 74, NULL, 1),
	(192, 1029, 30, 2, 66, NULL, 1),
	(193, 1029, 40, 2, 75, NULL, 1),
	(194, 1028, 20, 2, 68, NULL, 1),
	(196, 1029, 50, 2, 76, NULL, 1),
	(197, 1029, 60, 2, 77, NULL, 1),
	(198, 28, 30, 1, NULL, 1033, 1),
	(199, 1033, 10, 2, 78, NULL, 1),
	(200, 1033, 20, 2, 79, NULL, 1),
	(201, 1033, 30, 2, 80, NULL, 1),
	(202, 1028, 30, 2, 69, NULL, 1),
	(203, 1028, 40, 2, 70, NULL, 1),
	(204, 1028, 50, 2, 71, NULL, 1),
	(205, 1028, 60, 2, 72, NULL, 1),
	(206, 1029, 20, 2, 73, NULL, 1),
	(207, 1028, 70, 2, 82, NULL, 1),
	(208, 1028, 80, 2, 81, NULL, 1),
	(209, 1028, 90, 2, 84, NULL, 1),
	(210, 1034, 10, 2, 83, NULL, 1),
	(211, 1034, 20, 2, 85, NULL, 1),
	(212, 1034, 30, 2, 87, NULL, 1),
	(213, 1034, 40, 2, 88, NULL, 1),
	(214, 1034, 50, 2, 89, NULL, 1),
	(215, 1034, 60, 2, 90, NULL, 1),
	(216, 1034, 70, 2, 91, NULL, 1),
	(217, 28, 40, 1, NULL, 1034, 1),
	(218, 1033, 40, 2, 92, NULL, 1),
	(220, 28, 50, 1, NULL, 1035, 1),
	(221, 1035, 10, 2, 86, NULL, 1),
	(222, 1035, 20, 2, 87, NULL, 1),
	(223, 1035, 30, 2, 93, NULL, 1),
	(224, 1035, 40, 2, 95, NULL, 1),
	(225, 1035, 50, 2, 96, NULL, 1),
	(226, 1035, 60, 2, 94, NULL, 1);
/*!40000 ALTER TABLE "srm_menu_lines" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_persons
CREATE TABLE IF NOT EXISTS "srm_persons" (
	"person_id" INT idenity(1,1) PRIMARY KEY,
	"first_name" NVARCHAR(20) NOT NULL DEFAULT (N'DEFAULT'),
	"last_name" NVARCHAR(20) NOT NULL DEFAULT (N'DEFAULT'),
	"middle_name" NVARCHAR(20) NULL DEFAULT (NULL),
	"email_address" NVARCHAR(20) NULL DEFAULT (NULL),
	"telephone_number" NVARCHAR(20) NULL DEFAULT (NULL),
	"street_address" NVARCHAR(20) NULL DEFAULT (NULL),
	"city" NVARCHAR(20) NULL DEFAULT (NULL),
	"state" NVARCHAR(20) NULL DEFAULT (NULL),
	"postal_code" INT(10,0) NULL DEFAULT ((0)),
	"kiosk_id" NVARCHAR(128) NULL DEFAULT (N'-999'),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT ('1977-07-07 00:00:00'),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT ('1977-07-07 00:00:00'),
	PRIMARY KEY ("person_id")
);

-- Dumping data for table mod43fordpoc.srm_persons: 3 rows
DELETE FROM "srm_persons";
/*!40000 ALTER TABLE "srm_persons" DISABLE KEYS */;
INSERT INTO "dbo.srm_persons" ("first_name", "last_name", "middle_name", "email_address", "telephone_number", "street_address", "city", "state", "postal_code", "kiosk_id", "created_by", "created_date", "updated_by", "updated_date") VALUES
	('JOSHUA', 'BRISH', 'M', 'JOSH@MOD43.COM', '(999) 999-9988', '3233 WOCHA WAY', 'BEESKNEES', 'CA', 88888, '-999', 1, SYSDATETIME, 1, SYSDATETIME),
	('BRAD', 'CORNETT', 'J', 'BRAD@MOD43.COM', '(789) 456-7894', '546 WINNERS LANE', 'HOOPTOWN', 'MI', 48187, '-999', 1, SYSDATETIME, 1, SYSDATETIME)



-- Dumping structure for table mod43fordpoc.srm_responsibility
CREATE TABLE IF NOT EXISTS "srm_responsibility" (
	"responsibility_id" INT(10,0) NOT NULL,
	"responsibility_name" NVARCHAR(40) NOT NULL,
	"responsibility_shortcode" NVARCHAR(20) NOT NULL,
	"responsibility_root_menu_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("responsibility_id")
);

-- Dumping data for table mod43fordpoc.srm_responsibility: 1 rows
DELETE FROM "srm_responsibility";
/*!40000 ALTER TABLE "srm_responsibility" DISABLE KEYS */;
INSERT INTO "srm_responsibility" ("responsibility_id", "responsibility_name", "responsibility_shortcode", "responsibility_root_menu_id") VALUES
	(43, 'System Administrator', 'SYSADMIN', 28);
/*!40000 ALTER TABLE "srm_responsibility" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_resp_site_assignments
CREATE TABLE IF NOT EXISTS "srm_resp_site_assignments" (
	"resp_site_assignment_id" INT(10,0) NOT NULL,
	"responsibility_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"site_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"enabled_flag" INT(10,0) NOT NULL DEFAULT ((1)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	PRIMARY KEY ("resp_site_assignment_id")
);

-- Dumping data for table mod43fordpoc.srm_resp_site_assignments: 1 rows
DELETE FROM "srm_resp_site_assignments";
/*!40000 ALTER TABLE "srm_resp_site_assignments" DISABLE KEYS */;
INSERT INTO "srm_resp_site_assignments" ("resp_site_assignment_id", "responsibility_id", "site_id", "enabled_flag", "created_by", "created_date", "updated_by", "updated_date") VALUES
	(28, 43, 9, 1, 29, '2017-02-13 17:27:22', 29, '2017-02-13 17:27:22');
/*!40000 ALTER TABLE "srm_resp_site_assignments" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_users
CREATE TABLE IF NOT EXISTS "srm_users" (
	"user_id" INT(10,0) NOT NULL,
	"user_name" NVARCHAR(20) NOT NULL,
	"person_id" INT(10,0) NULL DEFAULT (NULL),
	"enabled_flag" INT(10,0) NOT NULL DEFAULT ((1)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"password" NVARCHAR(255) NOT NULL,
	PRIMARY KEY ("user_id")
);

-- Dumping data for table mod43fordpoc.srm_users: 3 rows
DELETE FROM "srm_users";
/*!40000 ALTER TABLE "srm_users" DISABLE KEYS */;
INSERT INTO "srm_users" ("user_name", "person_id", "enabled_flag", "created_by", "created_date", "updated_by", "updated_date", "password") VALUES
	('TEST', 14, 1, 1, '2017-01-26 03:58:00', 1, '2017-01-26 03:58:00', '$1$EJ0.7B5.$kxC4pSXLyHI7gV9BX2ml0/'),
	(30, 'TEST2', 17, 1, 1, '2017-02-03 02:57:23', 1, '2017-02-03 02:57:23', '$1$/S5.a.4.$pFjSJh8bnWoVdoXFGWICr0'),
	(1027, 'DVA', 18, 1, 1, '2016-12-20 10:34:09', 1, '2016-12-20 10:34:09', '$1$.paDPWiV$YM6Zd/ZwPW6WYgA1XYiy./');
/*!40000 ALTER TABLE "srm_users" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.srm_user_resp_assignments
CREATE TABLE IF NOT EXISTS "srm_user_resp_assignments" (
	"assignment_id" INT(10,0) NOT NULL,
	"user_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"responsibility_id" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	PRIMARY KEY ("assignment_id")
);

-- Dumping data for table mod43fordpoc.srm_user_resp_assignments: 3 rows
DELETE FROM "srm_user_resp_assignments";
/*!40000 ALTER TABLE "srm_user_resp_assignments" DISABLE KEYS */;
INSERT INTO "srm_user_resp_assignments" ("assignment_id", "user_id", "responsibility_id", "created_by", "created_date", "updated_by", "updated_date") VALUES
	(54, 29, 43, 1, '2017-02-03 11:04:53', 1, '2017-02-03 11:04:53'),
	(56, 30, 43, 1, '2017-02-15 18:05:08', 1, '2017-02-15 18:05:08'),
	(57, 1027, 43, 1, '2017-02-16 10:43:36', 1, '2017-02-16 10:43:36');
/*!40000 ALTER TABLE "srm_user_resp_assignments" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.sys_code_meanings
CREATE TABLE IF NOT EXISTS "sys_code_meanings" (
	"meaning_id" INT(10,0) NOT NULL,
	"meaning_type" NVARCHAR(24) NOT NULL,
	"meaning_code" INT(10,0) NOT NULL DEFAULT ((0)),
	"meaning_name" NVARCHAR(128) NOT NULL DEFAULT (N'Unknown Meaning'),
	PRIMARY KEY ("meaning_id")
);

-- Dumping data for table mod43fordpoc.sys_code_meanings: 2 rows
DELETE FROM "sys_code_meanings";
/*!40000 ALTER TABLE "sys_code_meanings" DISABLE KEYS */;
INSERT INTO mod43fordpoc.dbo.sys_code_meanings ("meaning_type", "meaning_code", "meaning_name") VALUES
	('SRMLINETYPE', 1, 'Submenu'),
	('SRMLINETYPE', 2, 'Function');
/*!40000 ALTER TABLE "sys_code_meanings" ENABLE KEYS */;


-- Dumping structure for view mod43fordpoc.up_equipment_calibration_history_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_equipment_calibration_history_view" (
	"equipment_name" VARCHAR(30) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"equipment_description" VARCHAR(200) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"serial_number" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"calibration_notes" NVARCHAR(255) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"calibration_date" DATETIME2 NOT NULL,
	"calibration_user" NVARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"site_id" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_equipment_onhand_balances_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_equipment_onhand_balances_view" (
	"equipment_name" VARCHAR(30) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"equipment_description" VARCHAR(200) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"group_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"location_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"serial_number" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"onhand_qty" INT NOT NULL,
	"site_id" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_inv_item_master_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_inv_item_master_view" (
	"item_name" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"item_description" NVARCHAR(150) NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"uom_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"status" VARCHAR(24) NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"serial_enabled" NVARCHAR(1) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"consumable" NVARCHAR(1) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"planning_enabled" NVARCHAR(1) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"site_id" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_inv_kiosk_item_serial_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_inv_kiosk_item_serial_view" (
	"TableName" NVARCHAR(6) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"ID" BIGINT NOT NULL,
	"SEARCH_TERM" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS'
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_inv_onhand_balances_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_inv_onhand_balances_view" (
	"item_name" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"item_description" NVARCHAR(150) NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"group_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"location_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"uom_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"onhand_qty" INT NOT NULL,
	"site_id" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_inv_serial_onhand_balances_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_inv_serial_onhand_balances_view" (
	"item_name" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"item_description" NVARCHAR(150) NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"group_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"location_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"serial_number" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"uom_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"site_id" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_inv_txn_history_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_inv_txn_history_view" (
	"transaction_type" VARCHAR(64) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"comment" NVARCHAR(100) NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"item" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"from_group" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"from_location" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"to_group" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"to_location" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"uom" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"quantity" INT NOT NULL,
	"created_by_user" NVARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"updated_by_user" NVARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"site_id" BIGINT NOT NULL,
	"site_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS'
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_srm_menu_lines_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_srm_menu_lines_view" (
	"srm_menu_line_id" INT NOT NULL,
	"srm_menu_header_id" INT NOT NULL,
	"srm_menu_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"srm_menu_shortcode" NVARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"srm_menu_line_sequence" INT NOT NULL,
	"srm_menu_line_type_id" INT NOT NULL,
	"line_type" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"line_type_id" INT NULL,
	"line_type_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"line_filename" NVARCHAR(80) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS'
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_srm_user_resp_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_srm_user_resp_view" (
	"user_id" INT NOT NULL,
	"responsibility_id" INT NOT NULL,
	"responsibility_name" NVARCHAR(40) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"responsibility_shortcode" NVARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS'
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_wo_best_location
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_wo_best_location" (
	"item_id" INT NOT NULL,
	"item_name" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"location_id" INT NOT NULL,
	"location_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"onhand_qty" INT NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view mod43fordpoc.up_wo_lines_view
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE "up_wo_lines_view" (
	"work_order_header_id" INT NOT NULL,
	"work_order_number" INT NOT NULL,
	"deliver_to_name" NVARCHAR(128) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"deliver_to_location" NVARCHAR(200) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"scheduled_date" DATE NOT NULL,
	"wo_line_id" INT NOT NULL,
	"wo_line_item_id" INT NOT NULL,
	"item_name" VARCHAR(20) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"wo_line_uom_id" INT NOT NULL,
	"uom_name" VARCHAR(24) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"wo_line_quantity" INT NOT NULL,
	"wo_line_released_quantity" INT NOT NULL,
	"wo_line_picked_quantity" INT NOT NULL,
	"wo_line_cancelled_quantity" INT NOT NULL,
	"wo_line_backordered_quantity" INT NOT NULL,
	"wo_line_site_id" INT NOT NULL,
	"site_name" NVARCHAR(50) NOT NULL COLLATE 'SQL_Latin1_General_CP1_CI_AS',
	"wo_line_status" INT NOT NULL,
	"created_by" INT NOT NULL,
	"created_date" DATETIME2 NOT NULL,
	"updated_by" INT NOT NULL,
	"updated_date" DATETIME2 NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for table mod43fordpoc.wo_header_number_values
CREATE TABLE IF NOT EXISTS "wo_header_number_values" (
	"id" INT(10,0) NULL DEFAULT (NULL),
	"next_order" INT(10,0) NOT NULL DEFAULT ((100001))
);

-- Dumping data for table mod43fordpoc.wo_header_number_values: 0 rows
DELETE FROM "wo_header_number_values";
/*!40000 ALTER TABLE "wo_header_number_values" DISABLE KEYS */;
/*!40000 ALTER TABLE "wo_header_number_values" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.wo_work_order_header
CREATE TABLE IF NOT EXISTS "wo_work_order_header" (
	"work_order_header_id" INT(10,0) NOT NULL,
	"work_order_number" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_site_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"deliver_to_name" NVARCHAR(128) NOT NULL,
	"deliver_to_location" NVARCHAR(200) NOT NULL,
	"scheduled_date" DATE(0) NOT NULL DEFAULT (getdate()),
	"close_date" DATE(0) NOT NULL DEFAULT (getdate()),
	"priority" NVARCHAR(1) NULL DEFAULT (NULL),
	"status" NVARCHAR(1) NULL DEFAULT (NULL),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"created_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((-999)),
	PRIMARY KEY ("work_order_header_id")
);

-- Dumping data for table mod43fordpoc.wo_work_order_header: 0 rows
DELETE FROM "wo_work_order_header";
/*!40000 ALTER TABLE "wo_work_order_header" DISABLE KEYS */;
/*!40000 ALTER TABLE "wo_work_order_header" ENABLE KEYS */;


-- Dumping structure for table mod43fordpoc.wo_work_order_lines
CREATE TABLE IF NOT EXISTS "wo_work_order_lines" (
	"wo_line_id" INT(10,0) NOT NULL,
	"wo_line_header_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_item_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_uom_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_quantity" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_released_quantity" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_picked_quantity" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_cancelled_quantity" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_backordered_quantity" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_site_id" INT(10,0) NOT NULL DEFAULT ((0)),
	"wo_line_status" INT(10,0) NOT NULL DEFAULT ((1)),
	"created_by" INT(10,0) NOT NULL DEFAULT ((0)),
	"created_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	"updated_by" INT(10,0) NOT NULL DEFAULT ((0)),
	"updated_date" DATETIME2(0) NOT NULL DEFAULT (getdate()),
	PRIMARY KEY ("wo_line_id")
);

-- Dumping data for table mod43fordpoc.wo_work_order_lines: 0 rows
DELETE FROM "wo_work_order_lines";
/*!40000 ALTER TABLE "wo_work_order_lines" DISABLE KEYS */;
/*!40000 ALTER TABLE "wo_work_order_lines" ENABLE KEYS */;


-- Dumping structure for function mod43fordpoc.num_to_datetime
DELIMITER //
CREATE FUNCTION m2ss.num_to_datetime(@val NUMERIC(38,0)) RETURNS DATETIME2(0)
AS
  BEGIN
    IF(@val IS NULL)
        RETURN NULL

    DECLARE @InputLen INT
    DECLARE @StrVal VARCHAR(50)

    SET @StrVal = @val
    SET @InputLen = LEN(@StrVal)

    IF(@InputLen < 3 OR @InputLen > 14 OR @InputLen = 7 OR @val <= 0)
        RETURN NULL

    IF(@InputLen >= 3 and @InputLen < 6)
        SET @StrVal = REPLICATE('0', 6 - @InputLen) + @StrVal
    ELSE IF(@InputLen > 6  and @InputLen < 8)
        SET @StrVal = REPLICATE('0', 8 - @InputLen) + @StrVal
    ELSE IF(@InputLen > 8  and @InputLen < 12)
        SET @StrVal = REPLICATE('0', 12 - @InputLen) + @StrVal 
    ELSE IF(@InputLen > 12  and @InputLen < 14)
        SET @StrVal = REPLICATE('0', 14 - @InputLen) + @StrVal  

    IF LEN(@StrVal) = 14
        SET @StrVal = LEFT(@StrVal, 4) + '-' + SUBSTRING(@StrVal, 5, 2) + 
                      '-'+ SUBSTRING(@StrVal, 7, 2) + ' ' + SUBSTRING(@StrVal, 9, 2) + 
                      ':' + SUBSTRING(@StrVal, 11, 2) + ':' + SUBSTRING(@StrVal, 13, 2)
    ELSE IF(LEN(@StrVal) = 12)
        SET @StrVal = CASE WHEN LEFT(@StrVal, 2) > '69' THEN '19' ELSE '20' END +  
                      LEFT(@StrVal, 2) + '-' + SUBSTRING(@StrVal, 3, 2) + '-' + 
                      SUBSTRING(@StrVal, 5, 2) + ' ' + SUBSTRING(@StrVal, 7, 2) + ':' + 
                      SUBSTRING(@StrVal, 9, 2) + ':' + SUBSTRING(@StrVal, 11, 2) 
    ELSE IF(LEN(@StrVal) = 8)
        SET @StrVal = LEFT(@StrVal, 4) + '-' + SUBSTRING(@StrVal, 5, 2) + '-'+ 
                      SUBSTRING(@StrVal, 7, 2) + ' 00:00:00' 
    ELSE IF(LEN(@StrVal) = 6)
        SET @StrVal = CASE WHEN LEFT(@StrVal, 2) > '69' THEN '19' ELSE '20' END +  
                      LEFT(@StrVal, 2) + '-' + SUBSTRING(@StrVal, 3, 2) + '-' + 
                      SUBSTRING(@StrVal, 5, 2) + ' 00:00:00'  

        DECLARE @Year INT
        SET @Year = CAST(LEFT(@StrVal, 4) AS INT) % 2000 + 2000 

    IF(ISDATE(CAST(@year + 2000 AS VARCHAR(4)) + RIGHT(@StrVal, LEN(@StrVal) - 4)) = 1)
          RETURN CONVERT(DATETIME2(0), @StrVal, 120)
   RETURN NULL
 END//
DELIMITER ;


-- Dumping structure for view mod43fordpoc.up_equipment_calibration_history_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_equipment_calibration_history_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_equipment_calibration_history_view (
   [equipment_name], 
   [equipment_description], 
   [serial_number], 
   [calibration_notes], 
   [calibration_date], 
   [calibration_user], 
   [site_id])
AS 
   SELECT 
      iem.equipment_name AS equipment_name, 
      iem.equipment_description AS equipment_description, 
      ies.serial_number AS serial_number, 
      iech.calibration_notes AS calibration_notes, 
      iech.calibration_date AS calibration_date, 
      su.user_name AS calibration_user, 
      iech.site_id AS site_id
   FROM (((dbo.inv_equipment_master  AS iem 
      CROSS JOIN dbo.inv_equipment_calibration_history  AS iech) 
      CROSS JOIN dbo.inv_equipment_serials  AS ies) 
      CROSS JOIN dbo.srm_users  AS su)
   WHERE (
      (iem.equipment_id = iech.equipment_id) AND 
      (iech.serial_id = ies.serial_id) AND 
      (iech.calibration_user = su.user_id));


-- Dumping structure for view mod43fordpoc.up_equipment_onhand_balances_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_equipment_onhand_balances_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_equipment_onhand_balances_view (
   [equipment_name], 
   [equipment_description], 
   [group_name], 
   [location_name], 
   [serial_number], 
   [onhand_qty], 
   [site_id])
AS 
   SELECT 
      iem.equipment_name AS equipment_name, 
      iem.equipment_description AS equipment_description, 
      ig.group_name AS group_name, 
      il.location_name AS location_name, 
      ies.serial_number AS serial_number, 
      ieob.onhand_qty AS onhand_qty, 
      ieob.site_id AS site_id
   FROM ((((dbo.inv_equipment_master  AS iem 
      CROSS JOIN dbo.inv_equipment_onhand_balances  AS ieob) 
      CROSS JOIN dbo.inv_equipment_serials  AS ies) 
      CROSS JOIN dbo.inv_groups  AS ig) 
      CROSS JOIN dbo.inv_locations  AS il)
   WHERE (
      (iem.equipment_id = ieob.equipment_id) AND 
      (ieob.location_id = il.location_id) AND 
      (ieob.group_id = ig.group_id) AND 
      (iem.serial_enabled = 'Y') AND 
      (ies.location_id = ieob.location_id) AND 
      (ies.equipment_id = ieob.equipment_id) AND 
      (ieob.onhand_qty <> 0));


-- Dumping structure for view mod43fordpoc.up_inv_item_master_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_inv_item_master_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_inv_item_master_view (
   [item_name], 
   [item_description], 
   [uom_name], 
   [status], 
   [serial_enabled], 
   [consumable], 
   [planning_enabled], 
   [site_id])
AS 
   SELECT 
      iim.item_name AS item_name, 
      iim.item_description AS item_description, 
      iuom.uom_name AS uom_name, 
      iim.status AS status, 
      iim.serial_enabled AS serial_enabled, 
      iim.consumable AS consumable, 
      iim.planning_enabled AS planning_enabled, 
      iim.site_id AS site_id
   FROM (dbo.inv_item_master  AS iim 
      CROSS JOIN dbo.inv_units_of_measure  AS iuom)
   WHERE (iuom.uom_id = iim.base_uom_id);


-- Dumping structure for view mod43fordpoc.up_inv_kiosk_item_serial_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_inv_kiosk_item_serial_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_inv_kiosk_item_serial_view ([TableName], [ID], [SEARCH_TERM])
AS  
   SELECT N'Item' AS TableName, iim.item_id AS ID, iim.item_name AS SEARCH_TERM
   FROM dbo.inv_item_master  AS iim 
    UNION
    (
      SELECT N'Serial' AS TableName, iis.serial_id AS ID, iis.serial_number AS SEARCH_TERM
      FROM dbo.inv_item_serials  AS iis
    );


-- Dumping structure for view mod43fordpoc.up_inv_onhand_balances_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_inv_onhand_balances_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_inv_onhand_balances_view (
   [item_name], 
   [item_description], 
   [group_name], 
   [location_name], 
   [uom_name], 
   [onhand_qty], 
   [site_id])
AS 
   SELECT TOP (9223372036854775807) 
      iim.item_name AS item_name, 
      iim.item_description AS item_description, 
      ig.group_name AS group_name, 
      il.location_name AS location_name, 
      iuom.uom_name AS uom_name, 
      iiob.onhand_qty AS onhand_qty, 
      iiob.site_id AS site_id
   FROM ((((dbo.inv_item_onhand_balances  AS iiob 
      CROSS JOIN dbo.inv_item_master  AS iim) 
      CROSS JOIN dbo.inv_groups  AS ig) 
      CROSS JOIN dbo.inv_locations  AS il) 
      CROSS JOIN dbo.inv_units_of_measure  AS iuom)
   WHERE (
      (iiob.item_id = iim.item_id) AND 
      (iiob.group_id = ig.group_id) AND 
      (iiob.location_id = il.location_id) AND 
      (iiob.base_uom_id = iuom.uom_id) AND 
      (ig.site_id = iiob.site_id) AND 
      (il.site_id = iiob.site_id))
      ORDER BY ig.group_name, il.location_name, iim.item_name;


-- Dumping structure for view mod43fordpoc.up_inv_serial_onhand_balances_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_inv_serial_onhand_balances_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_inv_serial_onhand_balances_view (
   [item_name], 
   [item_description], 
   [group_name], 
   [location_name], 
   [serial_number], 
   [uom_name], 
   [site_id])
AS 
   SELECT TOP (9223372036854775807) 
      iim.item_name AS item_name, 
      iim.item_description AS item_description, 
      ig.group_name AS group_name, 
      il.location_name AS location_name, 
      iis.serial_number AS serial_number, 
      iuom.uom_name AS uom_name, 
      iiob.site_id AS site_id
   FROM (((((dbo.inv_item_onhand_balances  AS iiob 
      CROSS JOIN dbo.inv_item_master  AS iim) 
      CROSS JOIN dbo.inv_groups  AS ig) 
      CROSS JOIN dbo.inv_locations  AS il) 
      CROSS JOIN dbo.inv_units_of_measure  AS iuom) 
      CROSS JOIN dbo.inv_item_serials  AS iis)
   WHERE (
      (iiob.item_id = iim.item_id) AND 
      (iim.item_id = iis.item_id) AND 
      (iiob.group_id = ig.group_id) AND 
      (iiob.location_id = il.location_id) AND 
      (iiob.base_uom_id = iuom.uom_id) AND 
      (ig.site_id = iiob.site_id) AND 
      (il.site_id = iiob.site_id) AND 
      (iis.site_id = iiob.site_id) AND 
      (iis.group_id = iiob.group_id) AND 
      (iis.location_id = iiob.location_id))
      ORDER BY ig.group_name, il.location_name, iim.item_name;


-- Dumping structure for view mod43fordpoc.up_inv_txn_history_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_inv_txn_history_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_inv_txn_history_view (
   [transaction_type], 
   [comment], 
   [item], 
   [from_group], 
   [from_location], 
   [to_group], 
   [to_location], 
   [uom], 
   [quantity], 
   [created_by_user], 
   [updated_by_user], 
   [site_id], 
   [site_name])
AS 
   /*
   *   SSMA warning messages:
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   *   M2SS0250: Parameters used in N'ISNULL' function/operator are not of same data type so they are casted to Character datatype.
   */

   SELECT 
      ith.txn_type_name AS transaction_type, 
      ith.comment AS comment, 
      iim.item_name AS item, 
      ISNULL(
         (
            SELECT ig1.group_name AS group_name
            FROM dbo.inv_groups  AS ig1
            WHERE (ig1.group_id = ith.from_group_id)
         ), N'NA') AS from_group, 
      ISNULL(
         (
            SELECT il1.location_name AS location_name
            FROM dbo.inv_locations  AS il1
            WHERE (il1.location_id = ith.from_loc_id)
         ), 'NA') AS from_location, 
      ISNULL(
         (
            SELECT ig2.group_name AS group_name
            FROM dbo.inv_groups  AS ig2
            WHERE (ig2.group_id = ith.to_group_id)
         ), N'NA') AS to_group, 
      ISNULL(
         (
            SELECT il2.location_name AS location_name
            FROM dbo.inv_locations  AS il2
            WHERE (il2.location_id = ith.to_loc_id)
         ), 'NA') AS to_location, 
      iuom.uom_name AS uom, 
      ith.txn_qty AS quantity, 
      ISNULL(
         (
            SELECT su1.user_name AS user_name
            FROM dbo.srm_users  AS su1
            WHERE (su1.user_id = ith.created_by)
         ), N'UNKNOWN') AS created_by_user, 
      ISNULL(
         (
            SELECT su2.user_name AS user_name
            FROM dbo.srm_users  AS su2
            WHERE (su2.user_id = ith.updated_by)
         ), N'UNKNOWN') AS updated_by_user, 
      ith.site_id AS site_id, 
      invs.site_name AS site_name
   FROM (((dbo.inv_txn_history  AS ith 
      CROSS JOIN dbo.inv_item_master  AS iim) 
      CROSS JOIN dbo.inv_units_of_measure  AS iuom) 
      CROSS JOIN dbo.inv_sites  AS invs)
   WHERE (
      (ith.item_id = iim.item_id) AND 
      (ith.txn_uom_id = iuom.uom_id) AND 
      (ith.site_id = invs.site_id));


-- Dumping structure for view mod43fordpoc.up_srm_menu_lines_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_srm_menu_lines_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_srm_menu_lines_view (
   [srm_menu_line_id], 
   [srm_menu_header_id], 
   [srm_menu_name], 
   [srm_menu_shortcode], 
   [srm_menu_line_sequence], 
   [srm_menu_line_type_id], 
   [line_type], 
   [line_type_id], 
   [line_type_name], 
   [line_filename])
AS 
   SELECT 
      srml.menu_line_id AS srm_menu_line_id, 
      srml.menu_header_id AS srm_menu_header_id, 
      srmh.menu_name AS srm_menu_name, 
      srmh.menu_shortcode AS srm_menu_shortcode, 
      srml.menu_line_sequence AS srm_menu_line_sequence, 
      srml.menu_line_type_id AS srm_menu_line_type_id, 
      scm.meaning_name AS line_type, 
      srml.menu_line_function_id AS line_type_id, 
      srmf.function_name AS line_type_name, 
      srmf.filename AS line_filename
   FROM (((dbo.srm_menu_lines  AS srml 
      CROSS JOIN dbo.srm_menu_header  AS srmh) 
      CROSS JOIN dbo.srm_functions  AS srmf) 
      CROSS JOIN dbo.sys_code_meanings  AS scm)
   WHERE (
      (srml.menu_line_function_id = srmf.function_id) AND 
      (srml.menu_header_id = srmh.menu_header_id) AND 
      (srml.menu_line_type_id = 2) AND 
      (scm.meaning_code = 2) AND 
      (scm.meaning_type = 'srmlinetype'))
    UNION
   SELECT TOP (9223372036854775807) 
      srml.menu_line_id AS srm_menu_line_id, 
      srml.menu_header_id AS srm_menu_header_id, 
      srmh.menu_name AS srm_menu_name, 
      srmh.menu_shortcode AS srm_menu_shortcode, 
      srml.menu_line_sequence AS srm_menu_line_sequence, 
      srml.menu_line_type_id AS srm_menu_line_type_id, 
      scm.meaning_name AS line_type, 
      srml.menu_line_submenu_id AS line_type_id, 
      srmh2.menu_name AS line_type_name, 
      srmh2.menu_description AS line_filename
   FROM (((dbo.srm_menu_lines  AS srml 
      CROSS JOIN dbo.srm_menu_header  AS srmh) 
      CROSS JOIN dbo.srm_menu_header  AS srmh2) 
      CROSS JOIN dbo.sys_code_meanings  AS scm)
   WHERE (
      (srml.menu_line_submenu_id = srmh2.menu_header_id) AND 
      (srml.menu_header_id = srmh.menu_header_id) AND 
      (srml.menu_line_type_id = 1) AND 
      (scm.meaning_code = 1) AND 
      (scm.meaning_type = 'srmlinetype'))
      ORDER BY srm_menu_header_id, srm_menu_line_sequence;


-- Dumping structure for view mod43fordpoc.up_srm_user_resp_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_srm_user_resp_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_srm_user_resp_view (
   [user_id], 
   [responsibility_id], 
   [responsibility_name], 
   [responsibility_shortcode])
AS 
   SELECT sura.user_id AS user_id, sura.responsibility_id AS responsibility_id, sr.responsibility_name AS responsibility_name, sr.responsibility_shortcode AS responsibility_shortcode
   FROM (dbo.srm_user_resp_assignments  AS sura 
      CROSS JOIN dbo.srm_responsibility  AS sr)
   WHERE (sura.responsibility_id = sr.responsibility_id);


-- Dumping structure for view mod43fordpoc.up_wo_best_location
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_wo_best_location";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_wo_best_location (
   [item_id], 
   [item_name], 
   [location_id], 
   [location_name], 
   [onhand_qty])
AS 
   SELECT TOP (9223372036854775807) 
      iiob.item_id AS item_id, 
      iim.item_name AS item_name, 
      iiob.location_id AS location_id, 
      il.location_name AS location_name, 
      iiob.onhand_qty AS onhand_qty
   FROM ((dbo.inv_item_onhand_balances  AS iiob 
      CROSS JOIN dbo.inv_item_master  AS iim) 
      CROSS JOIN dbo.inv_locations  AS il)
   WHERE ((iiob.item_id = iim.item_id) AND (iiob.location_id = il.location_id))
      ORDER BY iiob.item_id, iiob.onhand_qty DESC;


-- Dumping structure for view mod43fordpoc.up_wo_lines_view
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS "up_wo_lines_view";
/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `mod43fordpoc`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW dbo.up_wo_lines_view (
   [work_order_header_id], 
   [work_order_number], 
   [deliver_to_name], 
   [deliver_to_location], 
   [scheduled_date], 
   [wo_line_id], 
   [wo_line_item_id], 
   [item_name], 
   [wo_line_uom_id], 
   [uom_name], 
   [wo_line_quantity], 
   [wo_line_released_quantity], 
   [wo_line_picked_quantity], 
   [wo_line_cancelled_quantity], 
   [wo_line_backordered_quantity], 
   [wo_line_site_id], 
   [site_name], 
   [wo_line_status], 
   [created_by], 
   [created_date], 
   [updated_by], 
   [updated_date])
AS 
   SELECT TOP (9223372036854775807) 
      woh.work_order_header_id AS work_order_header_id, 
      woh.work_order_number AS work_order_number, 
      woh.deliver_to_name AS deliver_to_name, 
      woh.deliver_to_location AS deliver_to_location, 
      woh.scheduled_date AS scheduled_date, 
      wol.wo_line_id AS wo_line_id, 
      wol.wo_line_item_id AS wo_line_item_id, 
      iim.item_name AS item_name, 
      wol.wo_line_uom_id AS wo_line_uom_id, 
      iuom.uom_name AS uom_name, 
      wol.wo_line_quantity AS wo_line_quantity, 
      wol.wo_line_released_quantity AS wo_line_released_quantity, 
      wol.wo_line_picked_quantity AS wo_line_picked_quantity, 
      wol.wo_line_cancelled_quantity AS wo_line_cancelled_quantity, 
      wol.wo_line_backordered_quantity AS wo_line_backordered_quantity, 
      wol.wo_line_site_id AS wo_line_site_id, 
      invs.site_name AS site_name, 
      wol.wo_line_status AS wo_line_status, 
      wol.created_by AS created_by, 
      wol.created_date AS created_date, 
      wol.updated_by AS updated_by, 
      wol.updated_date AS updated_date
   FROM ((((dbo.wo_work_order_header  AS woh 
      CROSS JOIN dbo.wo_work_order_lines  AS wol) 
      CROSS JOIN dbo.inv_item_master  AS iim) 
      CROSS JOIN dbo.inv_units_of_measure  AS iuom) 
      CROSS JOIN dbo.inv_sites  AS invs)
   WHERE (
      (woh.work_order_header_id = wol.wo_line_header_id) AND 
      (wol.wo_line_item_id = iim.item_id) AND 
      (wol.wo_line_uom_id = iuom.uom_id) AND 
      (wol.wo_line_site_id = invs.site_id))
      ORDER BY wol.wo_line_id;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
