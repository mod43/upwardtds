<?php
 //Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'WOPICKORDERVIEW';


require_once 'app/init.php';
// Include app init file
    

// Ensure that both a user has logged in and selected a responsibility.  
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in

        header("Location: index.php");
        die("Redirecting to index.php"); 
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.    



if (!$function_access)
   {
      // die if not logged in
      header("Location: index.php");
      die("You do not have access to this function.");
    }


include 'header.php'; //includes the navigation header


$wo_header_number = $wo_header_number = $_GET['wo_header_number'];

if (!$wo_header_number)
    {
      echo '<div class="col-md-12">';
      echo 'Work Order Number Not Found!';
      echo '<br>';
      echo '<a href="UP_WO_PICKABLE_ORDERS.php"><button type="button" class="btn btn-primary">Find Valid Work Order</button></a>';
      echo '</div>';
      echo '<br><br>';
      die();
    } 


$wo_header_info = $database->table('wo_work_order_header')->where('work_order_number','=',$wo_header_number)->first();

$wo_header_id = $wo_header_info->work_order_header_id;

$order_lines = $database->table('up_wo_lines_view')->where('work_order_header_id','=', $wo_header_id)->get();

if ($wo_header_info->status == 1){
  $status = 'Open';
} elseif ($wo_header_info->status == 2) {
  $status = 'Pick Released';
} elseif ($wo_header_info->status == 3) {
  $status = 'Picked Full';
} elseif ($wo_header_info->status == 4) {
  $status = 'Cancelled';
} else {
  $status = 'Undefined';
}

if ($wo_header_info->priority == 1){
  $priority = 'High Priority';
} elseif ($wo_header_info->status == 2) {
  $priority = 'Normal Priority';
} elseif ($wo_header_info->status == 3) {
  $priority = 'Restock/Low Priority';
} else {
  $priority = 'Undefined';
}

?>

<div class="col-md-8">

<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">

              <center><h1 class="panel-title">Work Order Pick List - Order <?php echo $wo_header_number; ?>  -  Status:  <?php echo $status; ?></h1></center>

</div>
<div class="panel-body">
  <div class="table-responsive">
          <table class="table">
          <tr>
            <th>Pick</th>
            <th>Item</th>
            <th>UOM</th>
            <th>Order Quantity</th>
            <th>Picked Quantity</th>
            <th>Remaining Quantity</th>
            <th>Backordered Quantity</th>
            <th>Status</th>
            <th>Actions</th>
         </tr>
          <?php



            $count = count($order_lines);
            $ln = 0;

            while($ln < $count){

                  echo '<tr>';
                  if ($order_lines[$ln]->wo_line_status == 2){
                  echo '<td><a class="btn btn-success btn-xs" href="UP_WO_LINE_PICK_TXN.php?wo_line_id='.$order_lines[$ln]->wo_line_id.'" role="button"> <span class="glyphicon glyphicon-check" aria-hidden="true"></span> PICK</a></td>';
                  } else { echo '<td></td>';}
                  echo '<td>'.$order_lines[$ln]->item_name.'</td>';
                  echo '<td>'.$order_lines[$ln]->uom_name.'</td>';
                  echo '<td>'.$order_lines[$ln]->wo_line_quantity.'</td>';
                  echo '<td>'.$order_lines[$ln]->wo_line_picked_quantity.'</td>';
                  echo '<td>'.($order_lines[$ln]->wo_line_released_quantity-$order_lines[$ln]->wo_line_picked_quantity).'</td>';
                  echo '<td>'.$order_lines[$ln]->wo_line_backordered_quantity.'</td>';
                  if ($order_lines[$ln]->wo_line_status == 1){
                    $line_status = 'Open';
                  } elseif ($order_lines[$ln]->wo_line_status == 2) {
                    $line_status = 'Pick Released';
                  } elseif ($order_lines[$ln]->wo_line_status == 3) {
                    $line_status = 'Picked';
                  } elseif ($order_lines[$ln]->wo_line_status == 4) {
                    $line_status = 'Backordered';
                  } elseif ($order_lines[$ln]->wo_line_status == 5) {
                    $line_status = 'Cancelled';
                  } elseif ($order_lines[$ln]->wo_line_status == 6) {
                    $line_status = 'Partially Picked';
                  } else {
                    $line_status = 'Unknown Status';
                  }
                  echo '<td>'.$line_status.'</td>';
                  if ($order_lines[$ln]->wo_line_released_quantity > $order_lines[$ln]->wo_line_picked_quantity){
                  echo '<td><a class="btn btn-warning btn-xs" href="UP_WO_LINE_BO_TXN.php?wo_line_id='.$order_lines[$ln]->wo_line_id.'" role="button"> <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> BACKORDER</a></td>';
                  }
                  echo '</tr>';
                  $ln++;
                }
            
          ?>
          </table>
        </div>

  </div>

</body>
</html>