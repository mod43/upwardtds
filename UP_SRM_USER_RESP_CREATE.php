<?php

//Function Information Variables
//------------------------------
//All created functions should must include the following shortcode variable to check for authorization.

$function_shortcode = 'SRMUSERRESPCREATE';

require_once 'app/init.php';
// Include app init file

// Ensure that both a user has logged in and selected a responsibility.
// Selecting a responsibility opens menu which pushes available functions into session stack.
if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
      // die if not logged in
      header("Location: index.php");
      die("Redirecting to index.php");
    }

$function_access = $auth->checkFunctionAccess($function_shortcode);
//Check if user has access to function, return true or false.

if (!$function_access)
   {
      // die if not logged in
    header("Location: index.php");
    die("You do not have access to this function.");

    }

include 'header.php'; //includes the navigation header

if(isset($_GET['username'])){

$username = $_GET['username'];

} else {
      ("Location: UP_SRM_USERS_VIEW.php");
      die();
    }

$user_info = $database->table('mod43fordpoc.dbo.srm_users')->where('user_name','=',$username)->first();

if ($user_info[0]['person_id'] != null){

$person_info = $database->table('mod43fordpoc.dbo.srm_persons')->where('person_id','=',$user_info[0]['person_id'])->first();

}

$assigned_responsibilities = $database->table('mod43fordpoc.dbo.up_srm_user_resp_view')->where('user_id','=', $user_info[0]['user_id'])->get();



$count = count($assigned_responsibilities);
$ln = 0;

?>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">User Setup - <?php echo $username; ?></h1></center>
    </div>
    <div class="panel-body">
      <div class="col-sm-6">
        <div class="row">
          <div class="col-sm-3 text-left"><h5><strong>Username: </strong></h5></div>
          <div class="col-sm-9 text-left"><h5><?php echo $username; ?></h5></div>
        </div>
        <?php
        
        if (isset($person_info)){
          echo  '<div class="row">';
          echo  '<div class="col-sm-3 text-left"><h5><strong>Firstname: </strong></h5></div>';
          echo  '<div class="col-sm-9 text-left"><h5>'.$person_info[0]['first_name'].'</h5></div>';
          echo  '</div>';
          echo  '<div class="row">';
          echo  '<div class="col-sm-3 text-left"><h5><strong>Lastname: </strong></h5></div>';
          echo  '<div class="col-sm-9 text-left"><h5>'.$person_info[0]['last_name'].'</h5></div>';
          echo  '</div>';
        } else {
          echo  '<div class="row">';
          echo  '<div class="col-sm-3 text-left"><h5><strong>No Person Record - </strong></h5></div>';
          echo  '<div class="col-sm-9 text-left"><h5><a href="UP_SRM_USER_PERSON_CREATE.php?username='.$username.'">Link a person to this user</h5></div>';
          echo  '</div>';
        }
        ?>
        <div class="row">
          <div class="col-sm-3 text-left"></div>
          <div class="col-sm-9 text-left"><h5><a href="UP_SRM_USER_PASSWORD_RESET.php?username=<?php echo $username; ?>">Reset Password</a></h5></div>
        </div>
        <div class="row">
          <div class="col-sm-3 text-left"></div>
          <div class="col-sm-9 text-left"><h5><a href="UP_SRM_USERS_VIEW.php">Return To Users List</a></h5></div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
          <div class="panel-heading">
            <center><h1 class="panel-title">Add Responsibilities</h1></center>
          </div>
          <div class="panel-body">
            <form class="form-horizontal" action='ajax/UP_SRM_USER_ADD_RESP_PROC.php' method='post'>
              <div class="form-group">

                <div class="row">
                  <div class="col-xs-offset-1 col-xs-2">
                    <label for="item" class="control-label">Responsibility Name</label>
                  </div>
                  <div class="col-xs-7">
                    <select class="form-control" name="resps" id="resps">
                      <option>Choose a Responsibility...</option>
                    </select>
                  <div class="col-xs-2">
                    <span id='quantityvalid'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-1">
                    <input type="hidden" name="user_id" id="user_id" value=<?php echo '"'.$user_info[0]['user_id'].'"'; ?> >
                  </div>
                  <div class="col-xs-1">
                    <input type="hidden" name="user_name" value=<?php echo '"'.$username.'"'; ?> >
                  </div>
                  <div class="col-xs-offset-2 col-xs-10">
                    <span id='message'></span>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-offset-4 col-xs-4">
                    <input type="submit" value="Add" id='item_submit' class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <div class="panel panel-default" style="box-shadow: 1px 1px 1px #787878;">
    <div class="panel-heading">
      <center><h1 class="panel-title">Responsibilites Assigned</h1></center>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Responsibility Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php

                while($ln < $count){

                  echo '<tr>';
                  echo '<td>'.$assigned_responsibilities[$ln]['responsibility_name'].'</td>';
                  echo '<td><a href="#" data-href="ajax/UP_SRM_USER_RESP_DELETE_PROC.php?shortcode='.$assigned_responsibilities[$ln]['responsibility_shortcode'].'&delete_user='.$username.'" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>';
                  echo '</tr>';
                  $ln++;
                }
              ?>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>

                <div class="modal-body">
                    <p>You are about to delete this responsibility for this user, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="debug-url"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">

   $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });


$(document).ready(function (){
      var user_id = $('#user_id').val();
      $.getJSON('ajax/UP_SRM_GET_RESPS_QUERY.php',
                {user_id: user_id},
                function(data){
        $.each( data, function( key, value ){
          console.log( key + ": " + value );
          $('#resps').append($('<option></option>').val(key).html(value));
        });
      });

});




//$.each(data, function(key, value) {
//              console.log(data[key], data[value]);
//              });



</script>


</body>
</html>
