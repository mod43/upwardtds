<?php
    require_once 'app/init.php';
    // Check if logged in
    if (!(isset($_SESSION['user_id']) && isset($_SESSION['responsibility'])))
    {
    	// die if not logged in
		die("Redirecting to index.php"); 
        header("Location: index.php");
    }
    unset($_SESSION['available_functions']); // remove any functions availble from previous resp choices
	$resp_details = $auth->getResponsibilityDetails($_SESSION['responsibility']); //grab resp details based on choice
	include 'header.php';
?>
<div class="col-sm-8">
<div class="panel panel-default" style="box-shadow: 2px 2px 2px #787878;">
<div class="panel-heading">
	<center><h1 class="panel-title"><?php echo $resp_details->responsibility_name; ?></h1></center></div>
<div class="panel-body">
<?php
		$root_menu = $resp_details->responsibility_root_menu_id;
		$menu_header = $auth->getMenuDetails($root_menu);
		$menu_lines = $auth->getMenuLines($root_menu);
		$count = count($menu_lines);
		$l = 0;

		while($l < $count)
		{
			if($menu_lines[$l]->menu_line_type_id == 1){
				$submenu_header = $auth->getMenuDetails($menu_lines[$l]->menu_line_submenu_id);
				echo '<div class="panel-group" id="accordion">
        			<div class="panel panel-default">
           				 <div class="panel-heading">
                			<h4 class="panel-title">
                    			<a data-toggle="collapse" data-parent="#accordion" href="#'.$submenu_header->menu_shortcode.'"><span class="glyphicon glyphicon-plus"> </span>';
									
									echo "  ".$submenu_header->menu_name."<br>";
				echo ' 	        </a>
                			</h4>
            			</div>';
				$submenu_lines = $auth->getMenuLines($menu_lines[$l]->menu_line_submenu_id);
				$smcount = count($submenu_lines);
				$sm = 0;
				echo '	<div id="'.$submenu_header->menu_shortcode.'" class="panel-collapse collapse">
                			<div class="panel-body">
                			    <table class="table table-striped">';
					while($sm < $smcount)
					{
						
						$function_details = $auth->getFunctionDetails($submenu_lines[$sm]->menu_line_function_id);
						$_SESSION['available_functions'][] = $function_details->function_id;

						echo '			<tr>
					  						<td><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>
					  						<a href="'.$function_details->filename.'">'.$function_details->function_name.'</td>
					  					</tr>';

						$sm++;
					}

				echo '			</table>
                			</div>
            			</div>
            		</div>';

			} else {

					$function_details = $auth->getFunctionDetails($menu_lines[$l]->menu_line_function_id);
					$_SESSION['available_functions'][] = $function_details->function_id;

					echo '<div class="panel-heading">
                			<h4 class="panel-title">
                    			<a data-parent="#accordion" href="'.$function_details->filename.'">  <span class="glyphicon glyphicon-transfer"> </span>';
                    echo '  '.$function_details->function_name.'</a>
                    		</h4>
                    	</div>';

			}
			$l++;
		}
?>
</div>
</body>
</html>
