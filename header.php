<?php

if (!isset($_SESSION['responsibility']))
{
echo '<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Upward Inventory Management</title>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="js/bootstrap.min.css">
      <link rel="stylesheet" href="js/jquery-ui.min.css" type="text/css" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
      <script src="js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui.min.js"></script>
      <style type="text/css">
       body { background: #336699 !important;}
       .panel-default > .panel-heading { background-color: #CCCCCC !important;}
        </style>';

      echo '<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php" style="padding: 3px 3px;"><img alt="Brand" height="43px" width="43px" src="img/nav_bar_logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="navbar-text">Upward Inventory</li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span><span> </span>'.$_SESSION['username'].'<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="UP_SRM_RESPONSIBILITIES_VIEW.php">Change Responsibility</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>';
} else {

$current_responsibility = $_SESSION['responsibility'];

  echo '<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Upward Inventory Management</title>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="js/bootstrap.min.css">
      <link rel="stylesheet" href="js/jquery-ui.min.css" type="text/css" />
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
      <script src="js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/jquery-ui.min.js"></script>
      <style type="text/css">
       body { background: #336699 !important;}
       .panel-default > .panel-heading { background-color: #CCCCCC !important;}
        </style>';

      echo '<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="UP_SRM_MENU_VIEW.php" style="padding: 3px 3px;"><img alt="Brand" height="43px" width="43px" src="img/nav_bar_logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="navbar-text">Upward Inventory</li>
        <li class="navbar-text"><strong>Session Info:</strong></li>
        <li class="navbar-text">Site: '.$_SESSION['site_name'].'</li>
        <li class="navbar-text">Responsibility: '.$_SESSION['responsibility_name'].'</li>
      </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span><span> </span>'.$_SESSION['username'].'<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="UP_SRM_MENU_VIEW.php">Menu</a></li>
            <li><a href="UP_SRM_RESPONSIBILITIES_VIEW.php">Change Responsibility</a></li>
            <li class="divider"></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>';
}

?>
